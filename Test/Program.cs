﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbEntity;


namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            string md = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            Directory.CreateDirectory(md + "\\Maprom");
            if (!File.Exists(md + "\\Maprom\\database.db"))
            {
                File.Copy("database\\database.db", md + "\\Maprom\\database.db");
            }

            AppDomain.CurrentDomain.SetData("DataDirectory", md + "\\Maprom\\");

            MyContext context = new MyContext();
            context.Settingses.Load();
            Settings Settings = context.Settingses.Local.FirstOrDefault();

            if (Settings == null)
            {
                Settings = new Settings(context);
            }
            else
            {
                Settings._context = context;
                Settings.Deserialize();
            }

            

        }
    }
}
