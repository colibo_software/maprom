﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using jsonDeutschePost;
using LabelPrint.DeutschePostService;
using LabelPrint.Services;
using Newtonsoft.Json;
using TemplateEngine.Docx;

namespace LabelPrint
{
    public class HandlerDeutschePost
    {
        private Response _response;
        private OneClickForAppPortTypeV3Client client;
        private Settings _settings;
        private AuthenticateUserResponseType authInfo;
        public void Parse(string json)
        {
            //todo try -> catch
            _response = JsonConvert.DeserializeObject<Response>(json);
            int a = 0;

            //todo _response.data - > check for null
            foreach (var order in _response.data){

                //todo try -> catch
                if (order.delivery_id == 1136) //ToDO хардкод
                {
                    if(a++>=3)return;
                    var stamp = client
                        .checkoutShoppingCartPNG(new ShoppingCartPNGRequestType()
                        {
                            userToken = authInfo.userToken,
                            //createManifest = true,
                            //createShippingList = ShippingList.Item0,
                            positions = new ShoppingCartPosition[1] {new ShoppingCartPosition() {productCode = 9861}},
                            //ppl = 22,
                            shopOrderId =
                                client.createShopOrderId(
                                    new CreateShopOrderIdRequest() {userToken = authInfo.userToken}).shopOrderId,
                            total = 45,

                        });

                    Image im = null;

                    using (WebClient webClient = new WebClient())
                    {
                        var data = webClient.DownloadData(stamp.link);
                        using (MemoryStream memoryStream = new MemoryStream(data))
                        {
                            //todo move to using
                            ZipArchive zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Read, false);
                            ZipArchiveEntry zipArchiveEntry = zipArchive.GetEntry(zipArchive.Entries[0].FullName);
                            im = Image.FromStream(zipArchiveEntry.Open());
                        }
                    }

                    File.Copy(_settings.SettingsPrinter.PathTemplateLabelDeutschePost, $"testFolder\\{order.voucher_id}.docx");

                    var valuesToFill = new Content(
                        new FieldContent("text1", "333-222-111"),
                        new FieldContent("name", $"{order.delivery.name}"),
                        new FieldContent("street", $"{order.delivery.street} {order.delivery.street_nr}"),
                        new FieldContent("town", $"{order.delivery.postal_code} {order.delivery.city}"),
                        new FieldContent("Referenznummer", $"{order.voucher_nr}"),
                        new FieldContent("Gewicht", "--- kg"),
                        new ImageContent("image",ImageToByteArray(im)) // todo stream test
                    );
                    using (var outputDocument = new TemplateProcessor($"testFolder\\{order.voucher_id}.docx")
                        .SetRemoveContentControls(true))
                    {
                        outputDocument.FillContent(valuesToFill);
                        outputDocument.SaveChanges();
                    }
                    /*ZipArchive z;
                    Image.FromStream((z = new ZipArchive(new MemoryStream(new WebClient().DownloadData(client
                        .checkoutShoppingCartPNG(new ShoppingCartPNGRequestType()
                        {
                            userToken = authInfo.userToken,
                            //createManifest = true,
                            //createShippingList = ShippingList.Item0,
                            positions = new ShoppingCartPosition[1] {new ShoppingCartPosition() {productCode = 9861}},
                            //ppl = 22,
                            shopOrderId =
                                client.createShopOrderId(
                                    new CreateShopOrderIdRequest() {userToken = authInfo.userToken}).shopOrderId,
                            total = 45,

                        }).link)), ZipArchiveMode.Read, false)).GetEntry(z.Entries[0].FullName).Open());*/ //ToDo =)
                }
            }
            
        }
        byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                //TODO check imageIn for null
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }
        public HandlerDeutschePost(Settings settings)
        {
            _settings = settings;
            CustomHeaderMessageInspector._partnerId = this._settings.SettingsAccount.DeutschePostAccount.PARTNER_ID;
            CustomHeaderMessageInspector._keyPhase = this._settings.SettingsAccount.DeutschePostAccount.KEY_PHASE;
            CustomHeaderMessageInspector._signatureKey = this._settings.SettingsAccount.DeutschePostAccount.SCHLUESSEL_DPWN_MARKTPLATZ;

            client = new OneClickForAppPortTypeV3Client();
            authInfo = client.authenticateUser(new AuthenticateUserRequestType()
            {
                password = settings.SettingsAccount.DeutschePostAccount.Password,
                username = settings.SettingsAccount.DeutschePostAccount.EMail
            });
        }
    }
}
