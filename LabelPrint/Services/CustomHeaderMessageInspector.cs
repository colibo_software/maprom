﻿using System;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace LabelPrint.Services
{
    public class CustomHeaderMessageInspector : IClientMessageInspector, IDispatchMessageInspector
    {
        const int HASH_STRING_LENGTH = 8;
        const string SEPARATOR = "::";

        public static string _partnerId { get; set; }
        public static string _keyPhase { get; set; }
        public static string _signatureKey { get; set; }

        public CustomHeaderMessageInspector()
        {

        }
        static string sign(string partnerId, string keyPhase, string signatureKey, string dateTime)
        {
            // Day - Month - Year - Hour - Min - Sec
            // For debugging, set a fixed date time to verify hash
            //requestTimestamp = "28012014-142729";
            string temp = partnerId + SEPARATOR +
                          dateTime + SEPARATOR +
                          keyPhase + SEPARATOR +
                          signatureKey;
            // A bit of debugging, to see what is going on...
            //Console.WriteLine("Input string for MD5 hash: " + temp);
            try
            {
                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] bsig = Encoding.ASCII.GetBytes(temp);
                // Do the MD5 hashing using a built-in lib
                byte[] md5ByteHash = md5.ComputeHash(bsig);
                // Now convert to string, as 4 Base16 encoded bytes (8 char)
                StringBuilder md5HashString = new StringBuilder(32);
                foreach (byte md5Byte in md5ByteHash)
                {
                    md5HashString.Append(md5Byte.ToString("x2"));
                }
                return md5HashString.ToString().Substring(0,
                    HASH_STRING_LENGTH);
            }
            catch (ArgumentException ex)
            {
                // Add logging or error handling here...
                Console.WriteLine("Can't generate signature! " + ex);
            }
            // Exit if hash could not be calculated.
            return null;
        }
        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel)
        {
            //string s,s2 = DateTime.Now.ToString("ddMMyyyy-HHmmss");
            string s, s2 = (DateTime.UtcNow.AddHours(1)).ToString("ddMMyyyy-HHmmss"); //Todo время !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            MessageBuffer buffer = request.CreateBufferedCopy(Int32.MaxValue);
            request = buffer.CreateMessage();

            //MessageHeader partner = MessageHeadecontext.CreateHeader("PARTNER", string.Empty, "APHMV");
            MessageHeader partnerId = MessageHeader.CreateHeader("PARTNER_ID", string.Empty, _partnerId);
            MessageHeader requestTimestamp = MessageHeader.CreateHeader("REQUEST_TIMESTAMP", string.Empty, s2);
            MessageHeader keyPhase = MessageHeader.CreateHeader("KEY_PHASE", string.Empty, _keyPhase);
            MessageHeader partnerSignature = MessageHeader.CreateHeader("PARTNER_SIGNATURE", string.Empty, s = sign(_partnerId, _keyPhase, _signatureKey, s2));
            //MessageHeader someShit = MessageHeadecontext.CreateHeader("SCHLUESSEL_DPWN_MARKTPLATZ", string.Empty, "= 28PwuxGYAYeHqImJciYI5QtuQUcjuIme");

            //request.Headers.Add(partner);
            request.Headers.Add(partnerId);
            request.Headers.Add(requestTimestamp);
            request.Headers.Add(keyPhase);
            request.Headers.Add(partnerSignature);
            //request.Headers.Add(someShit);

            HttpRequestMessageProperty httpRequestMessage;
            object httpRequestMessageObject;

            if (request.Properties.TryGetValue(HttpRequestMessageProperty.Name, out httpRequestMessageObject))
            {
                httpRequestMessage = httpRequestMessageObject as HttpRequestMessageProperty;
                if (string.IsNullOrEmpty(httpRequestMessage.Headers["PARTNER_ID"]))
                {
                    httpRequestMessage.Headers["PARTNER_ID"] = _partnerId;
                }
                //if (string.IsNullOrEmpty(httpRequestMessage.Headers["PARTNER"]))
                //{
                //    httpRequestMessage.Headers["PARTNER"] = "APHMV";
                //}
                if (string.IsNullOrEmpty(httpRequestMessage.Headers["KEY_PHASE"]))
                {
                    httpRequestMessage.Headers["KEY_PHASE"] = _keyPhase;
                }
                if (string.IsNullOrEmpty(httpRequestMessage.Headers["PARTNER_SIGNATURE"]))
                {
                    httpRequestMessage.Headers["PARTNER_SIGNATURE"] = s;
                }
                if (string.IsNullOrEmpty(httpRequestMessage.Headers["REQUEST_TIMESTAMP"]))
                {
                    httpRequestMessage.Headers["REQUEST_TIMESTAMP"] = s2;
                }
                //if (string.IsNullOrEmpty(httpRequestMessage.Headers["SCHLUESSEL_DPWN_MARKTPLATZ"]))
                //{
                //    httpRequestMessage.Headers["SCHLUESSEL_DPWN_MARKTPLATZ"] = "= 28PwuxGYAYeHqImJciYI5QtuQUcjuIme";
                //}
            }
            else
            {
                httpRequestMessage = new HttpRequestMessageProperty();
                httpRequestMessage.Headers.Add("PARTNER_ID", _partnerId);
                httpRequestMessage.Headers.Add("REQUEST_TIMESTAMP", s2);
                httpRequestMessage.Headers.Add("KEY_PHASE", _keyPhase);
                httpRequestMessage.Headers.Add("PARTNER_SIGNATURE", s);
                //httpRequestMessage.Headers.Add("SCHLUESSEL_DPWN_MARKTPLATZ", "= 28PwuxGYAYeHqImJciYI5QtuQUcjuIme");
                request.Properties.Add(HttpRequestMessageProperty.Name, httpRequestMessage);
            }

            return null;
        }

        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            //throw new NotImplementedException();
        }

        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            return null;
            //throw new NotImplementedException();
        }

        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            //throw new NotImplementedException();
        }
    }
}