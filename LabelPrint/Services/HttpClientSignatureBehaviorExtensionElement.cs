﻿using System;
using System.ServiceModel.Configuration;

namespace LabelPrint.Services
{
    public class HttpClientSignatureBehaviorExtensionElement : BehaviorExtensionElement
    {
        public override Type BehaviorType => typeof(HttpClientSignatureEndpointBehavior);

        protected override object CreateBehavior()
        {
            return new HttpClientSignatureEndpointBehavior();
        }
    }
}