﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabelPrint
{
    public partial class AddPrinter : Form
    {
        private UserPrinter UserPrinter;
        public AddPrinter(UserPrinter userPrinter)
        {
            InitializeComponent();
            UserPrinter = userPrinter;

            if (UserPrinter.User!=null&&UserPrinter.Printer!=null)
            {
                Text = "Change";
                tbUserName.Text = UserPrinter.User;
                lbPrintName.Text = UserPrinter.Printer;
            }
        }

        private void btnSetPrinter_Click(object sender, EventArgs e)
        {
            if (printDialog.ShowDialog()==DialogResult.OK)
            {
                lbPrintName.Text = printDialog.PrinterSettings.PrinterName;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(lbPrintName.Text)||string.IsNullOrWhiteSpace(tbUserName.Text))
            {
                MessageBox.Show("Fill in all the fields", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning); 
            }
            else
            {
                UserPrinter.User = tbUserName.Text;
                UserPrinter.Printer = lbPrintName.Text;
                Close();
            }
        }
    }
}
