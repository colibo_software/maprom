﻿using CsvHelper.Configuration.Attributes;

namespace LabelPrint
{
    class CsvData
    {
        [Name("Sendungsreferenz")]
        public string order_id { get; set; }
        [Name("Abrechnungsnummer")]
        public string dhl_accNum_codeProduct { get; set; }
        [Name("Sendungsdatum")]
        public string date { get; set; }
        [Name("Absendername 1")]
        public string senderCompanyOrName { get; set; }
        [Name("Absender Straße und Hausnummer")]
        public string senderStreet { get; set; }
        [Name("Absender PLZ")]
        public string senderPostalCode { get; set; }
        [Name("Absender Ort")]
        public string senderCity { get; set; }
        [Name("Absender LKZ")]
        public string senderCountryId { get; set; }
        [Name("Absender eMailadresse")]
        public string senderMail { get; set; }
        [Name("Absender Telefonnummer")]
        public string senderPhone { get; set; }
        [Name("Empfängername 1")]
        public string deliveryCompanyOrName { get; set; }
        [Name("Empfängername 2 / PostNr")]
        public string deliveryPostNr { get; set; }
        [Name("Empfänger Straße und Hausnummer")]
        public string deliveryStreet { get; set; }
        [Name("Empfänger PLZ")]
        public string deliveryPostalCode { get; set; }
        [Name("Empfänger Ort")]
        public string deliveryCity { get; set; }
        [Name("Empfänger LKZ")]
        public string deliveryCountryId { get; set; }
        [Name("Empfängerreferenz")]
        public string deliveryCustomerNr { get; set; }
        [Name("Empfänger eMailadresse")]
        public string deliveryMail { get; set; }
        [Name("Empfänger Telefonnummer")]
        public string deliveryPhone { get; set; }
        [Name("Gewicht")]
        public string weight { get; set; }
        [Name("Produkt- und Servicedetails")]
        public string DHL_Product { get; set; }
        [Name("Nachnahme-Kontoreferenz")]
        public string accountReference { get; set; }
        [Name("Nachnahme-Betrag")]
        public string cashOnDeliveryPrice { get; set; }
        [Name("Nachnahme-IBAN")]
        public string iban { get; set; }
        [Name("Nachnahme-BIC")]
        public string bic { get; set; }
        [Name("Nachnahme-Zahlungsempfänger")]
        public string accountOwner { get; set; }
        [Name("Nachnahme-Bankname")]
        public string bankName { get; set; }
        [Name("dhl_application_id")]
        public string dhlAppId { get; set; }
        [Name("dhl_token")]
        public string dhlToken { get; set; }
        [Name("dhl_user")]
        public string dhlUser { get; set; }
        [Name("dhl_signature")]
        public string dhlSignature { get; set; }
        [Name("dhl_product")]
        public string dhlProduct { get; set; }
        [Name("mail_smtp_host")]
        public string smtpHost { get; set; }
        [Name("mail_smtp_port")]
        public string smtpPort { get; set; }
        [Name("mail_smtp_user")]
        public string smtpUser { get; set; }
        [Name("mail_smtp_password")]
        public string smtpPassword { get; set; }
        [Name("mail_smtp_from_name")]
        public string smtpFromName { get; set; }
        [Name("mail_smtp_from_email")]
        public string smtpFromMail { get; set; }


        public CsvData()
        {

        }
    }
}