﻿namespace jsonDeutschePost
{
    public class Sender
    {
        public string company { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string name { get; set; }
        public string country { get; set; }
        public string postal_code { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string street_nr { get; set; }
        public string additional { get; set; }

        public Sender()
        {
            
        }
    }
}