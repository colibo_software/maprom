﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace jsonDeutschePost
{
    public class Response
    {
        public bool status { get; set; }

        public List<Order> data { get; set; }

        public Response()
        {
            
        }
    }
}