﻿namespace jsonDeutschePost
{
    public class Order
    {
        public string user { get; set; }
        public string delivery_name { get; set; }
        public int delivery_id { get; set; }
        public string voucher_nr { get; set; }
        public int voucher_id { get; set; }

        public Delivery delivery { get; set; }
        public Invoice invoice { get; set; }
        public Sender sender { get; set; }

        public Order()
        {
            
        }
    }
}