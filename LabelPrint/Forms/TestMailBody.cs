﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DbEntity;
using DeutschePost;

namespace LabelPrint.Forms
{
    public partial class TestMailBody : Form
    {
        private Settings _setting;

        public TestMailBody(Settings setting)
        {
            _setting = setting;
            InitializeComponent();
        }


        private void btnTemplate_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                tbTemplateName.Text = openFileDialog.FileName;
            }
            webBrowser.Navigate(new Uri("file:///" + tbTemplateName.Text));
            ;

            //webBrowser.DocumentText = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(webBrowser.DocumentText));

            //webBrowser.Navigate("pack://siteoforigin:,,,/" + "test.html");
        }

        private void Form1_Load(object sender, EventArgs e)
        {


            propertyGrid1.SelectedObject = _setting.MailSettings.etikett;

            tbTemplateName.Text = _setting.MailSettings.PathTemplateMail;

            foreach (var propertyInfo in typeof(Etikett).GetProperties())
            {
                TreeNode tmp = new TreeNode(propertyInfo.Name);

                if (propertyInfo.GetCustomAttribute<DescriptionAttribute>()?.Description.Equals("MyClass") != null)
                {
                    foreach (var property in propertyInfo.PropertyType.GetProperties())
                    {
                        tmp.Nodes.Add(new TreeNode(property.Name));
                    }
                    //{Checked = _setting.MailSettings.Collection.Contains(property.Name) }
                }
                treeView1.Nodes.Add(tmp);
                //Debug.Print(tmp.FullPath);
                //for (int i = 0; i < tmp.Nodes.Count; i++)
                //{
                //    Debug.Print(tmp.Nodes[i].FullPath);
                //}
            }
            foreach (TreeNode treeViewNode in treeView1.Nodes)
            {
                if (_setting.MailSettings.Collection.Contains(treeViewNode.FullPath))
                {
                    treeViewNode.Checked = true;
                    foreach (TreeNode node in treeViewNode.Nodes)
                    {
                        if (_setting.MailSettings.Collection.Contains(node.FullPath))
                        {
                            node.Checked = true;
                            treeViewNode.Expand();
                        }
                    }
                }
            }
            this.treeView1.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterCheck);
        }

        private void treeView1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            this.treeView1.AfterCheck -= new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterCheck);
            if (e.Node.Checked == true)
            {
                if (e.Node.Nodes.Count != 0)
                {
                    foreach (TreeNode node in e.Node.Nodes)
                    {
                        node.Checked = true;
                        _setting.MailSettings.Collection.Add(node.FullPath);
                    }
                }
                _setting.MailSettings.Collection.Add(e.Node.FullPath);
                e.Node.Expand();
            }
            else
            {
                if (e.Node.Nodes.Count != 0)
                {
                    foreach (TreeNode node in e.Node.Nodes)
                    {
                        node.Checked = false;
                        _setting.MailSettings.Collection.Remove(node.FullPath);
                    }
                }
                _setting.MailSettings.Collection.Remove(e.Node.FullPath);
            }
            this.treeView1.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterCheck);
        }

        private void btnGenerateHtml_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbTemplateName.Text)) return;
            string mess = String.Empty;

            using (StreamReader reader = new StreamReader(tbTemplateName.Text, Encoding.Unicode))
            {
                mess = reader.ReadToEnd();
            }

            foreach (var VARIABLE in _setting.MailSettings.Collection)
            {
                if (VARIABLE.Contains("\\"))
                {
                    var words = VARIABLE.Split('\\');
                    //Type type= typeof(Etikett);
                    //PropertyInfo info = typeof(Etikett).GetProperty();
                    //for (int i = 0; i < words.Length; i++)
                    //{
                    //    info = type.GetProperty(words[i]);
                    //}
                    //ToDo !!!!!!!!!!!!!!!!!!!!!!!!!
                    //mess = Regex.Replace(mess, $@"\\w*@{VARIABLE}@\\w*",typeof(Etikett).GetProperty(words[0]).PropertyType.GetProperty(words[1]).GetValue(typeof(Etikett).GetProperty(words[0]).GetValue(etikett)).ToString());
                    mess = mess.Replace($"@{VARIABLE}@", typeof(Etikett).GetProperty(words[0]).PropertyType.GetProperty(words[1]).GetValue(typeof(Etikett).GetProperty(words[0]).GetValue(_setting.MailSettings.etikett)).ToString());
                }
                else
                {
                    //mess = Regex.Replace(mess, $"\\w*@{VARIABLE}@\\w*",typeof(Etikett).GetProperty(VARIABLE).GetValue(etikett).ToString());
                    mess = mess.Replace($"@{VARIABLE}@", typeof(Etikett).GetProperty(VARIABLE).GetValue(_setting.MailSettings.etikett).ToString());
                }

            }
            using (StreamWriter writer = new StreamWriter("tmp.html", false, Encoding.Unicode) { })
            {
                writer.Write(mess);
            }

            string applicationDirectory = Path.GetDirectoryName(Application.ExecutablePath);
            string myFile = Path.Combine(applicationDirectory, "tmp.html");
            webBrowser.Navigate(new Uri("file:///" + myFile));
            //ToDO временный файл
        }

        private void TestMailBody_FormClosing(object sender, FormClosingEventArgs e)
        {
            _setting.Serialize();
        }

        private void treeView1_NodeMouseHover(object sender, TreeNodeMouseHoverEventArgs e)
        {
            toolTip.SetToolTip(treeView1,e.Node.FullPath);
        }
    }
}
