﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Forms;
using DbEntity;
using DeutschePost;
using myADGV;

namespace LabelPrint
{
    public partial class LogForm : System.Windows.Forms.Form
    {
        private MyContext _context;
        private myADGV.AdvancedDataGridView _advancedDataGridView;

        private MyBindingSource _bindingSource;

        private Etikett _selectedEtikett;
        
        public LogForm(MyContext context)
        {
            InitializeComponent();
            _context = context;
            _bindingSource = new MyBindingSource();

        }

        private void LogForm_Load(object sender, EventArgs e)
        {
            _context.Etiketten.Load();
            _context.Deliveries.Load();
            _context.Senders.Load();
            _context.Invoices.Load();
            _context.Analyzers.Load();
            _context.Logs.Load();

            _advancedDataGridView = new AdvancedDataGridView()
            {
                Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top 
                                                                | System.Windows.Forms.AnchorStyles.Bottom)
                                                                | System.Windows.Forms.AnchorStyles.Left)
                                                                | System.Windows.Forms.AnchorStyles.Right))), 
                Dock = DockStyle.Fill,
                DataSource = _bindingSource.GetBindingSource(),
                ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Sunken,
                AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill,
                ReadOnly = true,
                SelectionMode = DataGridViewSelectionMode.FullRowSelect
            };

            _advancedDataGridView.CellMouseDoubleClick += _advancedDataGridView_CellMouseDoubleClick;
            _advancedDataGridView.CellEndEdit += _advancedDataGridView_CellEndEdit;
            _advancedDataGridView.CellClick += _advancedDataGridView_CellClick;

            splitContainer1.Panel1.Controls.Add(_advancedDataGridView);
            splitContainer1.Panel2Collapsed = true;
            //tabPageOrders.Controls.Add(_advancedDataGridViewOrders);

            _bindingSource.AddTable("Logs",_context.Logs.Local.ToList(),new List<string>()/*{"Callsite"}*/);
            _bindingSource.AddTable("Etiketten",_context.Etiketten.Local.ToList(),new List<string>(){  "delivery", "invoice", "sender", "Analyzer", "api","apiDhl","smtp" });
            //_bindingSourceOrders.AddTable("Etiketten",_context.Etiketten.Local.ToList(),new List<string>(){"Id","delivery_id","Delivery","Invoice","Sender","Analyzer"});

            comboBox.SelectedItem = "Etiketten";
        }

        private void _advancedDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!checkBoxInfoMode.Checked|| e.RowIndex >= _bindingSource.GetCountCurrentDataMember() || comboBox.SelectedItem.Equals("Logs")) return;
            string id = _advancedDataGridView.CurrentRow.Cells[_advancedDataGridView.CurrentRow.Cells.Count-1].Value.ToString();

            _selectedEtikett = _context.Etiketten.Local.FirstOrDefault(x=>x.Id.ToString().Equals(id)); // ToDo

            propertyGrid1.SelectedObject = _selectedEtikett;
        }

        private void _advancedDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //string tmp = _advancedDataGridView.CurrentCell.Value.ToString();
            //var type = _advancedDataGridView.CurrentCell.ValueType;
            //string id = _advancedDataGridView.CurrentRow.Cells[8].Value.ToString();
            //var rowObj = _context.Etiketten.FirstOrDefault(x => x.Id.ToString().Equals(id));
        }

        private void _advancedDataGridView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //if (e.RowIndex >= _bindingSource.GetCountCurrentDataMember())
            //    return;
            //string id = _advancedDataGridView.CurrentRow.Cells[8].Value.ToString();
            //var rowObj = _context.Etiketten.FirstOrDefault(x=>x.Id.ToString().Equals(id));
        }



        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox.SelectedItem.ToString().Equals("Etiketten"))
            {
                splitContainer1.Panel2Collapsed = !checkBoxInfoMode.Checked;
                _bindingSource.SetDataMember(comboBox.SelectedItem.ToString());
            }
            else
            {
                _bindingSource.SetDataMember(comboBox.SelectedItem.ToString());
                splitContainer1.Panel2Collapsed = true;
            }
        }

        private void checkBoxInfoMode_CheckedChanged(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = !checkBoxInfoMode.Checked;
        }
    }
}
