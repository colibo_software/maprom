﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using DbEntity;
using DeutschePost;
using DocumentFormat.OpenXml.Drawing.Charts;
using LabelPrint.handlers;
using Microsoft.VisualBasic.FileIO;
using NLog;

namespace LabelPrint.Forms
{
    public partial class Form2 : Form
    {
        private Settings Settings;
        private MyContext context;
        private HandlerDHL HandlerDhl;
        private Logger _logger;
        private List<Data> datas;

        private Etikett etikett;

        public Form2()
        {
            InitializeComponent();

            string md = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            Directory.CreateDirectory(md + "\\Maprom");
            if (!File.Exists(md + "\\Maprom\\database.db"))
            {
                File.Copy("database\\database.db", md + "\\Maprom\\database.db");
            }

            AppDomain.CurrentDomain.SetData("DataDirectory", md + "\\Maprom\\");

            context = new MyContext();
            context.Settingses.Load();
            Settings = context.Settingses.Local.FirstOrDefault();

            if (Settings == null)
            {
                Settings = new Settings(context);
            }
            else
            {
                Settings._context = context;
                Settings.Deserialize();
            }

            _logger = LogManager.GetCurrentClassLogger();

            HandlerDhl = new HandlerDHL(Settings, context, _logger);


            datas = new List<Data>();
            using (TextFieldParser parser = new TextFieldParser(@"test.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(";");
                while (!parser.EndOfData)
                {
                    //Process row
                    string[] fields = parser.ReadFields();

                    datas.Add(new Data() {country = fields[0],city = fields[1],street = fields[2]});

                }
            }


            var files = Directory.GetFiles(Settings.GeneralSettings.InputFolderDHL)
                .Where(s => Path.GetExtension(s).ToLower() == ".txt").ToList();
            if (files.Count() != 0)
            {
                try
                {

                    foreach (var file in files)
                    {
                        try
                        {
                            using (TextFieldParser parser = new TextFieldParser(file))
                            {
                                parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                                parser.SetDelimiters(";");

                                //Process row
                                string[] fields = parser.ReadFields();
                                fields = parser.ReadFields();

                                etikett = new Etikett()
                                {
                                    order_id = Int32.Parse(fields[0]),
                                    order_nr = fields[0],
                                    Date = DateTime.Parse(fields[2]),
                                    sender = new Sender()
                                    {
                                        company = fields[4],
                                        street = fields[7],
                                        postal_code = fields[8],
                                        city = fields[9],
                                        country = fields[10],
                                        //email = fields[11],
                                        //phone = fields[12],
                                    },
                                    delivery = new Delivery()
                                    {
                                        company = fields[13],
                                        street = fields[16],
                                        postal_code = fields[17],
                                        city = fields[18],
                                        country = fields[19],
                                        //customer_nr = fields[20],
                                        //email = fields[21],
                                        //phone = fields[22],
                                    },
                                    invoice = new Invoice(),
                                    weight = Decimal.Parse(fields[23]),
                                    delivery_name = "DHL",
                                    user = ""

                                };

                            }
                        }
                        catch (Exception) { }
                    }
                }
                catch (Exception) { }
            }
            etikett.delivery.street = "Forstamtsweg";
            etikett.delivery.street_nr = "2";

        }




        private void button1_Click(object sender, EventArgs e)
        {
            var listDE = datas.Where(data => data.country.Equals("DE")).ToList();
            var listDE50 = listDE.Take(50).ToList();

            foreach (var data in listDE50)
            {
                data.street_nr = Regex
                    .Match(data.street, "[.,:\\\\/|;\\s]*(\\d[-,\\\\|/]*)+.*", RegexOptions.IgnoreCase).Value
                    .Trim('.', ' ', ',');
                data.street = Regex.Replace(data.street, "[.,:\\\\/|;\\s]*(\\d[-,\\\\|/]*)+.*", "", RegexOptions.IgnoreCase);

                etikett.sender.street = data.street;
                etikett.sender.street_nr = data.street_nr;
                etikett.sender.city = data.city;

                HandlerDhl.Job(etikett,false);

            }
        }
    }

    class Data
    {
        public string country { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string street_nr { get; set; }

        public Data()
        {
            country = String.Empty;
            street = String.Empty;
            street_nr = String.Empty;
        }
    }
}
