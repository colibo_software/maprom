﻿namespace LabelPrint
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogMail = new System.Windows.Forms.OpenFileDialog();
            this.tabPageDeutschePost = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tbInputFolderDP = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSetInputFolderDP = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.cbFileDP = new System.Windows.Forms.CheckBox();
            this.panelFileOutputDP = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.tbExportFolderDP = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_ExportFileNameDP = new System.Windows.Forms.TextBox();
            this.btnSetExportFolderDP = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.cbWebhookDP = new System.Windows.Forms.CheckBox();
            this.panelWebhookOutputDP = new System.Windows.Forms.Panel();
            this.dgvPostDP = new System.Windows.Forms.DataGridView();
            this.label35 = new System.Windows.Forms.Label();
            this.dgvGetDP = new System.Windows.Forms.DataGridView();
            this.label26 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.tbDP_webhookURL = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.btnTemplateLabelDP = new System.Windows.Forms.Button();
            this.tbPathLabelTemplateDP = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lbPrintNameDP = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSetPrinterDP = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.testDPAccount = new System.Windows.Forms.Button();
            this.tbPasswordDeutschePost = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbSandboxDP_Price = new System.Windows.Forms.TextBox();
            this.tbSandboxDP_ProductCode = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tbSandboxDP_Password = new System.Windows.Forms.TextBox();
            this.tbSandboxDP_Mail = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tbMail = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tbSCHLUESSEL_DPWN_MARKTPLATZ = new System.Windows.Forms.TextBox();
            this.tbKEY_PHASE = new System.Windows.Forms.TextBox();
            this.tbPARTNER_ID = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbPriceForCode = new System.Windows.Forms.TextBox();
            this.tbProductCode = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tabPageDHL = new System.Windows.Forms.TabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.cbFileDHL = new System.Windows.Forms.CheckBox();
            this.panelFileOutputDHL = new System.Windows.Forms.Panel();
            this.btnSetExportFolderDHL = new System.Windows.Forms.Button();
            this.tbExportFolderDHL = new System.Windows.Forms.TextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.tb_ExportFileNameDHL = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.cbWebhookDHL = new System.Windows.Forms.CheckBox();
            this.panelWebhookOutputDHL = new System.Windows.Forms.Panel();
            this.dgvPostDHL = new System.Windows.Forms.DataGridView();
            this.label42 = new System.Windows.Forms.Label();
            this.dgvGetDHL = new System.Windows.Forms.DataGridView();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.tbDHL_webhookURL = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.tbInputFolderDHL = new System.Windows.Forms.TextBox();
            this.btnSetInputFolderDHL = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.btnTemplateLabelDHL = new System.Windows.Forms.Button();
            this.tbPathLabelTemplateDHL = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.lbPrintNameDHL = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.btnSetPrinterDHL = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbAccNumber = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tbProduct = new System.Windows.Forms.TextBox();
            this.tbCode = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.btnTestDhlSettings = new System.Windows.Forms.Button();
            this.tbSignature = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tbUser = new System.Windows.Forms.TextBox();
            this.tbToken = new System.Windows.Forms.TextBox();
            this.tbApplicationID = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tabPageSMTP = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.btnTuningMailBody = new System.Windows.Forms.Button();
            this.btnTemplateMail = new System.Windows.Forms.Button();
            this.tbPathMailTemplate = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbFromMail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbPasswordSMTP = new System.Windows.Forms.TextBox();
            this.btnTestSMTP = new System.Windows.Forms.Button();
            this.tbUserName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbFrom = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.tbHost = new System.Windows.Forms.TextBox();
            this.tabPageGeneral = new System.Windows.Forms.TabPage();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageDatabase = new System.Windows.Forms.TabPage();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabPageDeutschePost.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.panelFileOutputDP.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.panelWebhookOutputDP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPostDP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGetDP)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPageDHL.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.panelFileOutputDHL.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.panelWebhookOutputDHL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPostDHL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGetDHL)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPageSMTP.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageDatabase.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "docx files (*.docx)|*.docx";
            // 
            // openFileDialogMail
            // 
            this.openFileDialogMail.Filter = "html files (*.html)|*.html";
            // 
            // tabPageDeutschePost
            // 
            this.tabPageDeutschePost.AutoScroll = true;
            this.tabPageDeutschePost.Controls.Add(this.groupBox16);
            this.tabPageDeutschePost.Controls.Add(this.groupBox12);
            this.tabPageDeutschePost.Controls.Add(this.groupBox10);
            this.tabPageDeutschePost.Controls.Add(this.groupBox3);
            this.tabPageDeutschePost.Location = new System.Drawing.Point(4, 22);
            this.tabPageDeutschePost.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageDeutschePost.Name = "tabPageDeutschePost";
            this.tabPageDeutschePost.Size = new System.Drawing.Size(923, 750);
            this.tabPageDeutschePost.TabIndex = 6;
            this.tabPageDeutschePost.Text = "Deutsche Post";
            this.tabPageDeutschePost.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox16.Controls.Add(this.textBox2);
            this.groupBox16.Controls.Add(this.label30);
            this.groupBox16.Controls.Add(this.button1);
            this.groupBox16.Controls.Add(this.tbInputFolderDP);
            this.groupBox16.Controls.Add(this.label9);
            this.groupBox16.Controls.Add(this.btnSetInputFolderDP);
            this.groupBox16.Location = new System.Drawing.Point(3, 97);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(857, 76);
            this.groupBox16.TabIndex = 28;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Input";
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Location = new System.Drawing.Point(200, 46);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(561, 20);
            this.textBox2.TabIndex = 18;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(10, 53);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(66, 13);
            this.label30.TabIndex = 17;
            this.label30.Text = "File mapping";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(770, 45);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 22);
            this.button1.TabIndex = 19;
            this.button1.Text = "Set";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // tbInputFolderDP
            // 
            this.tbInputFolderDP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbInputFolderDP.Location = new System.Drawing.Point(200, 15);
            this.tbInputFolderDP.Name = "tbInputFolderDP";
            this.tbInputFolderDP.Size = new System.Drawing.Size(561, 20);
            this.tbInputFolderDP.TabIndex = 15;
            this.tbInputFolderDP.TextChanged += new System.EventHandler(this.tbInputFolderDP_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Input folder";
            // 
            // btnSetInputFolderDP
            // 
            this.btnSetInputFolderDP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetInputFolderDP.Location = new System.Drawing.Point(770, 14);
            this.btnSetInputFolderDP.Name = "btnSetInputFolderDP";
            this.btnSetInputFolderDP.Size = new System.Drawing.Size(75, 22);
            this.btnSetInputFolderDP.TabIndex = 16;
            this.btnSetInputFolderDP.Text = "Set";
            this.btnSetInputFolderDP.UseVisualStyleBackColor = true;
            this.btnSetInputFolderDP.Click += new System.EventHandler(this.btnSetInputFolderDP_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox12.Controls.Add(this.groupBox15);
            this.groupBox12.Controls.Add(this.groupBox14);
            this.groupBox12.Location = new System.Drawing.Point(3, 529);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(857, 627);
            this.groupBox12.TabIndex = 27;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Output";
            // 
            // groupBox15
            // 
            this.groupBox15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox15.Controls.Add(this.cbFileDP);
            this.groupBox15.Controls.Add(this.panelFileOutputDP);
            this.groupBox15.Location = new System.Drawing.Point(6, 379);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(841, 242);
            this.groupBox15.TabIndex = 1;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "File";
            // 
            // cbFileDP
            // 
            this.cbFileDP.AutoSize = true;
            this.cbFileDP.Location = new System.Drawing.Point(9, 19);
            this.cbFileDP.Name = "cbFileDP";
            this.cbFileDP.Size = new System.Drawing.Size(59, 17);
            this.cbFileDP.TabIndex = 7;
            this.cbFileDP.Text = "Enable";
            this.cbFileDP.UseVisualStyleBackColor = true;
            this.cbFileDP.CheckedChanged += new System.EventHandler(this.CbFileDP_CheckedChanged);
            // 
            // panelFileOutputDP
            // 
            this.panelFileOutputDP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelFileOutputDP.Controls.Add(this.richTextBox1);
            this.panelFileOutputDP.Controls.Add(this.tbExportFolderDP);
            this.panelFileOutputDP.Controls.Add(this.label38);
            this.panelFileOutputDP.Controls.Add(this.label2);
            this.panelFileOutputDP.Controls.Add(this.tb_ExportFileNameDP);
            this.panelFileOutputDP.Controls.Add(this.btnSetExportFolderDP);
            this.panelFileOutputDP.Controls.Add(this.label37);
            this.panelFileOutputDP.Location = new System.Drawing.Point(4, 42);
            this.panelFileOutputDP.Name = "panelFileOutputDP";
            this.panelFileOutputDP.Size = new System.Drawing.Size(830, 196);
            this.panelFileOutputDP.TabIndex = 22;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Location = new System.Drawing.Point(190, 66);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(629, 124);
            this.richTextBox1.TabIndex = 21;
            this.richTextBox1.Text = "";
            // 
            // tbExportFolderDP
            // 
            this.tbExportFolderDP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbExportFolderDP.Location = new System.Drawing.Point(190, 3);
            this.tbExportFolderDP.Name = "tbExportFolderDP";
            this.tbExportFolderDP.Size = new System.Drawing.Size(551, 20);
            this.tbExportFolderDP.TabIndex = 16;
            this.tbExportFolderDP.TextChanged += new System.EventHandler(this.tbExportFolderDP_TextChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(3, 54);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(44, 13);
            this.label38.TabIndex = 20;
            this.label38.Text = "Content";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Export folder";
            // 
            // tb_ExportFileNameDP
            // 
            this.tb_ExportFileNameDP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_ExportFileNameDP.Location = new System.Drawing.Point(190, 30);
            this.tb_ExportFileNameDP.Name = "tb_ExportFileNameDP";
            this.tb_ExportFileNameDP.Size = new System.Drawing.Size(629, 20);
            this.tb_ExportFileNameDP.TabIndex = 19;
            // 
            // btnSetExportFolderDP
            // 
            this.btnSetExportFolderDP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetExportFolderDP.Location = new System.Drawing.Point(748, 2);
            this.btnSetExportFolderDP.Name = "btnSetExportFolderDP";
            this.btnSetExportFolderDP.Size = new System.Drawing.Size(72, 22);
            this.btnSetExportFolderDP.TabIndex = 17;
            this.btnSetExportFolderDP.Text = "Set";
            this.btnSetExportFolderDP.UseVisualStyleBackColor = true;
            this.btnSetExportFolderDP.Click += new System.EventHandler(this.btnSetExportFolderDP_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(3, 30);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(52, 13);
            this.label37.TabIndex = 18;
            this.label37.Text = "File name";
            // 
            // groupBox14
            // 
            this.groupBox14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox14.Controls.Add(this.cbWebhookDP);
            this.groupBox14.Controls.Add(this.panelWebhookOutputDP);
            this.groupBox14.Location = new System.Drawing.Point(6, 19);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(841, 358);
            this.groupBox14.TabIndex = 0;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Webhook";
            // 
            // cbWebhookDP
            // 
            this.cbWebhookDP.AutoSize = true;
            this.cbWebhookDP.Location = new System.Drawing.Point(9, 19);
            this.cbWebhookDP.Name = "cbWebhookDP";
            this.cbWebhookDP.Size = new System.Drawing.Size(59, 17);
            this.cbWebhookDP.TabIndex = 0;
            this.cbWebhookDP.Text = "Enable";
            this.cbWebhookDP.UseVisualStyleBackColor = true;
            this.cbWebhookDP.CheckedChanged += new System.EventHandler(this.CbWebhookDP_CheckedChanged);
            // 
            // panelWebhookOutputDP
            // 
            this.panelWebhookOutputDP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelWebhookOutputDP.Controls.Add(this.dgvPostDP);
            this.panelWebhookOutputDP.Controls.Add(this.label35);
            this.panelWebhookOutputDP.Controls.Add(this.dgvGetDP);
            this.panelWebhookOutputDP.Controls.Add(this.label26);
            this.panelWebhookOutputDP.Controls.Add(this.label36);
            this.panelWebhookOutputDP.Controls.Add(this.tbDP_webhookURL);
            this.panelWebhookOutputDP.Location = new System.Drawing.Point(4, 38);
            this.panelWebhookOutputDP.Name = "panelWebhookOutputDP";
            this.panelWebhookOutputDP.Size = new System.Drawing.Size(830, 316);
            this.panelWebhookOutputDP.TabIndex = 7;
            // 
            // dgvPostDP
            // 
            this.dgvPostDP.AllowUserToOrderColumns = true;
            this.dgvPostDP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPostDP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPostDP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPostDP.Location = new System.Drawing.Point(20, 203);
            this.dgvPostDP.Name = "dgvPostDP";
            this.dgvPostDP.Size = new System.Drawing.Size(796, 107);
            this.dgvPostDP.TabIndex = 6;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.Location = new System.Drawing.Point(19, 47);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(30, 20);
            this.label35.TabIndex = 3;
            this.label35.Text = "Get";
            // 
            // dgvGetDP
            // 
            this.dgvGetDP.AllowUserToOrderColumns = true;
            this.dgvGetDP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvGetDP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGetDP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGetDP.Location = new System.Drawing.Point(20, 70);
            this.dgvGetDP.Name = "dgvGetDP";
            this.dgvGetDP.Size = new System.Drawing.Size(796, 107);
            this.dgvGetDP.TabIndex = 5;
            this.dgvGetDP.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvGetDP_CellContentClick);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(13, 13);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 13);
            this.label26.TabIndex = 1;
            this.label26.Text = "URL";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(19, 180);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(37, 20);
            this.label36.TabIndex = 4;
            this.label36.Text = "Post";
            // 
            // tbDP_webhookURL
            // 
            this.tbDP_webhookURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDP_webhookURL.Location = new System.Drawing.Point(190, 5);
            this.tbDP_webhookURL.Multiline = true;
            this.tbDP_webhookURL.Name = "tbDP_webhookURL";
            this.tbDP_webhookURL.Size = new System.Drawing.Size(636, 21);
            this.tbDP_webhookURL.TabIndex = 2;
            this.tbDP_webhookURL.TextChanged += new System.EventHandler(this.TbDP_webhookURL_TextChanged);
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.btnTemplateLabelDP);
            this.groupBox10.Controls.Add(this.tbPathLabelTemplateDP);
            this.groupBox10.Controls.Add(this.label12);
            this.groupBox10.Controls.Add(this.lbPrintNameDP);
            this.groupBox10.Controls.Add(this.label1);
            this.groupBox10.Controls.Add(this.btnSetPrinterDP);
            this.groupBox10.Location = new System.Drawing.Point(3, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(857, 88);
            this.groupBox10.TabIndex = 26;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Printer";
            // 
            // btnTemplateLabelDP
            // 
            this.btnTemplateLabelDP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTemplateLabelDP.Location = new System.Drawing.Point(770, 50);
            this.btnTemplateLabelDP.Name = "btnTemplateLabelDP";
            this.btnTemplateLabelDP.Size = new System.Drawing.Size(75, 22);
            this.btnTemplateLabelDP.TabIndex = 32;
            this.btnTemplateLabelDP.Text = "Change";
            this.btnTemplateLabelDP.UseVisualStyleBackColor = true;
            this.btnTemplateLabelDP.Click += new System.EventHandler(this.btnTemplateLabelDP_Click);
            // 
            // tbPathLabelTemplateDP
            // 
            this.tbPathLabelTemplateDP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPathLabelTemplateDP.Enabled = false;
            this.tbPathLabelTemplateDP.Location = new System.Drawing.Point(200, 51);
            this.tbPathLabelTemplateDP.Multiline = true;
            this.tbPathLabelTemplateDP.Name = "tbPathLabelTemplateDP";
            this.tbPathLabelTemplateDP.Size = new System.Drawing.Size(564, 20);
            this.tbPathLabelTemplateDP.TabIndex = 31;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 55);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "Template label";
            // 
            // lbPrintNameDP
            // 
            this.lbPrintNameDP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPrintNameDP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbPrintNameDP.Location = new System.Drawing.Point(200, 16);
            this.lbPrintNameDP.Name = "lbPrintNameDP";
            this.lbPrintNameDP.Size = new System.Drawing.Size(563, 21);
            this.lbPrintNameDP.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Printer name";
            // 
            // btnSetPrinterDP
            // 
            this.btnSetPrinterDP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetPrinterDP.Image = ((System.Drawing.Image)(resources.GetObject("btnSetPrinterDP.Image")));
            this.btnSetPrinterDP.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSetPrinterDP.Location = new System.Drawing.Point(815, 12);
            this.btnSetPrinterDP.Name = "btnSetPrinterDP";
            this.btnSetPrinterDP.Size = new System.Drawing.Size(30, 31);
            this.btnSetPrinterDP.TabIndex = 27;
            this.btnSetPrinterDP.UseVisualStyleBackColor = true;
            this.btnSetPrinterDP.Click += new System.EventHandler(this.btnSetPrinterDP_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.testDPAccount);
            this.groupBox3.Controls.Add(this.tbPasswordDeutschePost);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.tbMail);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.tbSCHLUESSEL_DPWN_MARKTPLATZ);
            this.groupBox3.Controls.Add(this.tbKEY_PHASE);
            this.groupBox3.Controls.Add(this.tbPARTNER_ID);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.tbPriceForCode);
            this.groupBox3.Controls.Add(this.tbProductCode);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Location = new System.Drawing.Point(3, 179);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(857, 344);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Account";
            // 
            // testDPAccount
            // 
            this.testDPAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.testDPAccount.Location = new System.Drawing.Point(770, 19);
            this.testDPAccount.Name = "testDPAccount";
            this.testDPAccount.Size = new System.Drawing.Size(75, 22);
            this.testDPAccount.TabIndex = 34;
            this.testDPAccount.Text = "Test";
            this.testDPAccount.UseVisualStyleBackColor = true;
            this.testDPAccount.Click += new System.EventHandler(this.testDPAccount_Click);
            // 
            // tbPasswordDeutschePost
            // 
            this.tbPasswordDeutschePost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPasswordDeutschePost.Location = new System.Drawing.Point(200, 120);
            this.tbPasswordDeutschePost.Name = "tbPasswordDeutschePost";
            this.tbPasswordDeutschePost.Size = new System.Drawing.Size(640, 20);
            this.tbPasswordDeutschePost.TabIndex = 33;
            this.tbPasswordDeutschePost.TextChanged += new System.EventHandler(this.tbPasswordDeutschePost_TextChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.tbSandboxDP_Price);
            this.groupBox4.Controls.Add(this.tbSandboxDP_ProductCode);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.tbSandboxDP_Password);
            this.groupBox4.Controls.Add(this.tbSandboxDP_Mail);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(10, 209);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(837, 126);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Sandbox";
            // 
            // tbSandboxDP_Price
            // 
            this.tbSandboxDP_Price.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSandboxDP_Price.Location = new System.Drawing.Point(190, 95);
            this.tbSandboxDP_Price.Name = "tbSandboxDP_Price";
            this.tbSandboxDP_Price.Size = new System.Drawing.Size(634, 20);
            this.tbSandboxDP_Price.TabIndex = 41;
            this.tbSandboxDP_Price.TextChanged += new System.EventHandler(this.tbSandboxDP_Price_TextChanged);
            // 
            // tbSandboxDP_ProductCode
            // 
            this.tbSandboxDP_ProductCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSandboxDP_ProductCode.Location = new System.Drawing.Point(190, 70);
            this.tbSandboxDP_ProductCode.Name = "tbSandboxDP_ProductCode";
            this.tbSandboxDP_ProductCode.Size = new System.Drawing.Size(634, 20);
            this.tbSandboxDP_ProductCode.TabIndex = 39;
            this.tbSandboxDP_ProductCode.TextChanged += new System.EventHandler(this.tbSandboxDP_ProductCode_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 75);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 13);
            this.label19.TabIndex = 38;
            this.label19.Text = "Product Code";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 100);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(31, 13);
            this.label20.TabIndex = 40;
            this.label20.Text = "Price";
            // 
            // tbSandboxDP_Password
            // 
            this.tbSandboxDP_Password.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSandboxDP_Password.Location = new System.Drawing.Point(190, 45);
            this.tbSandboxDP_Password.Name = "tbSandboxDP_Password";
            this.tbSandboxDP_Password.Size = new System.Drawing.Size(634, 20);
            this.tbSandboxDP_Password.TabIndex = 37;
            this.tbSandboxDP_Password.TextChanged += new System.EventHandler(this.tbSandboxDP_Password_TextChanged);
            // 
            // tbSandboxDP_Mail
            // 
            this.tbSandboxDP_Mail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSandboxDP_Mail.Location = new System.Drawing.Point(190, 20);
            this.tbSandboxDP_Mail.Name = "tbSandboxDP_Mail";
            this.tbSandboxDP_Mail.Size = new System.Drawing.Size(634, 20);
            this.tbSandboxDP_Mail.TabIndex = 36;
            this.tbSandboxDP_Mail.TextChanged += new System.EventHandler(this.tbSandboxDP_Mail_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 50);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "Password";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(26, 13);
            this.label16.TabIndex = 34;
            this.label16.Text = "Mail";
            // 
            // tbMail
            // 
            this.tbMail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMail.Location = new System.Drawing.Point(200, 95);
            this.tbMail.Name = "tbMail";
            this.tbMail.Size = new System.Drawing.Size(640, 20);
            this.tbMail.TabIndex = 32;
            this.tbMail.TextChanged += new System.EventHandler(this.tbMail_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 125);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "Password";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 100);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 13);
            this.label14.TabIndex = 30;
            this.label14.Text = "Mail";
            // 
            // tbSCHLUESSEL_DPWN_MARKTPLATZ
            // 
            this.tbSCHLUESSEL_DPWN_MARKTPLATZ.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSCHLUESSEL_DPWN_MARKTPLATZ.Location = new System.Drawing.Point(200, 70);
            this.tbSCHLUESSEL_DPWN_MARKTPLATZ.Name = "tbSCHLUESSEL_DPWN_MARKTPLATZ";
            this.tbSCHLUESSEL_DPWN_MARKTPLATZ.Size = new System.Drawing.Size(640, 20);
            this.tbSCHLUESSEL_DPWN_MARKTPLATZ.TabIndex = 29;
            this.tbSCHLUESSEL_DPWN_MARKTPLATZ.TextChanged += new System.EventHandler(this.tbSCHLUESSEL_DPWN_MARKTPLATZ_TextChanged);
            // 
            // tbKEY_PHASE
            // 
            this.tbKEY_PHASE.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbKEY_PHASE.Location = new System.Drawing.Point(200, 45);
            this.tbKEY_PHASE.Name = "tbKEY_PHASE";
            this.tbKEY_PHASE.Size = new System.Drawing.Size(640, 20);
            this.tbKEY_PHASE.TabIndex = 28;
            this.tbKEY_PHASE.TextChanged += new System.EventHandler(this.tbKEY_PHASE_TextChanged);
            // 
            // tbPARTNER_ID
            // 
            this.tbPARTNER_ID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPARTNER_ID.Location = new System.Drawing.Point(200, 20);
            this.tbPARTNER_ID.Name = "tbPARTNER_ID";
            this.tbPARTNER_ID.Size = new System.Drawing.Size(564, 20);
            this.tbPARTNER_ID.TabIndex = 27;
            this.tbPARTNER_ID.TextChanged += new System.EventHandler(this.tbPARTNER_ID_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 75);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(189, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "SCHLUESSEL DPWN MARKTPLATZ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "KEY PHASE";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "PARTNER ID";
            // 
            // tbPriceForCode
            // 
            this.tbPriceForCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPriceForCode.Location = new System.Drawing.Point(200, 170);
            this.tbPriceForCode.Name = "tbPriceForCode";
            this.tbPriceForCode.Size = new System.Drawing.Size(640, 20);
            this.tbPriceForCode.TabIndex = 23;
            this.tbPriceForCode.TextChanged += new System.EventHandler(this.tbPriceForCode_TextChanged);
            // 
            // tbProductCode
            // 
            this.tbProductCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProductCode.Location = new System.Drawing.Point(200, 145);
            this.tbProductCode.Name = "tbProductCode";
            this.tbProductCode.Size = new System.Drawing.Size(640, 20);
            this.tbProductCode.TabIndex = 21;
            this.tbProductCode.TextChanged += new System.EventHandler(this.tbProductCode_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 150);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 13);
            this.label17.TabIndex = 20;
            this.label17.Text = "Product Code";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 175);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "Price";
            // 
            // tabPageDHL
            // 
            this.tabPageDHL.AutoScroll = true;
            this.tabPageDHL.Controls.Add(this.groupBox17);
            this.tabPageDHL.Controls.Add(this.groupBox5);
            this.tabPageDHL.Controls.Add(this.groupBox11);
            this.tabPageDHL.Controls.Add(this.groupBox1);
            this.tabPageDHL.Location = new System.Drawing.Point(4, 22);
            this.tabPageDHL.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageDHL.Name = "tabPageDHL";
            this.tabPageDHL.Size = new System.Drawing.Size(923, 750);
            this.tabPageDHL.TabIndex = 5;
            this.tabPageDHL.Text = "DHL";
            this.tabPageDHL.UseVisualStyleBackColor = true;
            // 
            // groupBox17
            // 
            this.groupBox17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox17.Controls.Add(this.groupBox18);
            this.groupBox17.Controls.Add(this.groupBox19);
            this.groupBox17.Location = new System.Drawing.Point(3, 398);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(873, 627);
            this.groupBox17.TabIndex = 30;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Output";
            // 
            // groupBox18
            // 
            this.groupBox18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox18.Controls.Add(this.cbFileDHL);
            this.groupBox18.Controls.Add(this.panelFileOutputDHL);
            this.groupBox18.Location = new System.Drawing.Point(6, 379);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(855, 242);
            this.groupBox18.TabIndex = 1;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "File";
            // 
            // cbFileDHL
            // 
            this.cbFileDHL.AutoSize = true;
            this.cbFileDHL.Location = new System.Drawing.Point(9, 19);
            this.cbFileDHL.Name = "cbFileDHL";
            this.cbFileDHL.Size = new System.Drawing.Size(59, 17);
            this.cbFileDHL.TabIndex = 7;
            this.cbFileDHL.Text = "Enable";
            this.cbFileDHL.UseVisualStyleBackColor = true;
            this.cbFileDHL.CheckedChanged += new System.EventHandler(this.CbFileDHL_CheckedChanged);
            // 
            // panelFileOutputDHL
            // 
            this.panelFileOutputDHL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelFileOutputDHL.Controls.Add(this.btnSetExportFolderDHL);
            this.panelFileOutputDHL.Controls.Add(this.tbExportFolderDHL);
            this.panelFileOutputDHL.Controls.Add(this.richTextBox2);
            this.panelFileOutputDHL.Controls.Add(this.label29);
            this.panelFileOutputDHL.Controls.Add(this.label40);
            this.panelFileOutputDHL.Controls.Add(this.tb_ExportFileNameDHL);
            this.panelFileOutputDHL.Controls.Add(this.label41);
            this.panelFileOutputDHL.Location = new System.Drawing.Point(4, 42);
            this.panelFileOutputDHL.Name = "panelFileOutputDHL";
            this.panelFileOutputDHL.Size = new System.Drawing.Size(839, 196);
            this.panelFileOutputDHL.TabIndex = 22;
            // 
            // btnSetExportFolderDHL
            // 
            this.btnSetExportFolderDHL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetExportFolderDHL.Location = new System.Drawing.Point(760, 2);
            this.btnSetExportFolderDHL.Name = "btnSetExportFolderDHL";
            this.btnSetExportFolderDHL.Size = new System.Drawing.Size(72, 22);
            this.btnSetExportFolderDHL.TabIndex = 23;
            this.btnSetExportFolderDHL.Text = "Set";
            this.btnSetExportFolderDHL.UseVisualStyleBackColor = true;
            this.btnSetExportFolderDHL.Click += new System.EventHandler(this.btnSetExportFolderDHL_Click);
            // 
            // tbExportFolderDHL
            // 
            this.tbExportFolderDHL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbExportFolderDHL.Location = new System.Drawing.Point(190, 3);
            this.tbExportFolderDHL.Name = "tbExportFolderDHL";
            this.tbExportFolderDHL.Size = new System.Drawing.Size(563, 20);
            this.tbExportFolderDHL.TabIndex = 22;
            this.tbExportFolderDHL.TextChanged += new System.EventHandler(this.tbExportFolderDHL_TextChanged);
            // 
            // richTextBox2
            // 
            this.richTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox2.Location = new System.Drawing.Point(190, 66);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(637, 124);
            this.richTextBox2.TabIndex = 21;
            this.richTextBox2.Text = "";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(3, 54);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(44, 13);
            this.label29.TabIndex = 20;
            this.label29.Text = "Content";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(3, 6);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(66, 13);
            this.label40.TabIndex = 15;
            this.label40.Text = "Export folder";
            // 
            // tb_ExportFileNameDHL
            // 
            this.tb_ExportFileNameDHL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_ExportFileNameDHL.Location = new System.Drawing.Point(190, 30);
            this.tb_ExportFileNameDHL.Name = "tb_ExportFileNameDHL";
            this.tb_ExportFileNameDHL.Size = new System.Drawing.Size(637, 20);
            this.tb_ExportFileNameDHL.TabIndex = 19;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(3, 30);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(52, 13);
            this.label41.TabIndex = 18;
            this.label41.Text = "File name";
            // 
            // groupBox19
            // 
            this.groupBox19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox19.Controls.Add(this.cbWebhookDHL);
            this.groupBox19.Controls.Add(this.panelWebhookOutputDHL);
            this.groupBox19.Location = new System.Drawing.Point(6, 19);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(855, 358);
            this.groupBox19.TabIndex = 0;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Webhook";
            // 
            // cbWebhookDHL
            // 
            this.cbWebhookDHL.AutoSize = true;
            this.cbWebhookDHL.Location = new System.Drawing.Point(9, 19);
            this.cbWebhookDHL.Name = "cbWebhookDHL";
            this.cbWebhookDHL.Size = new System.Drawing.Size(59, 17);
            this.cbWebhookDHL.TabIndex = 0;
            this.cbWebhookDHL.Text = "Enable";
            this.cbWebhookDHL.UseVisualStyleBackColor = true;
            this.cbWebhookDHL.CheckedChanged += new System.EventHandler(this.CbWebhookDHL_CheckedChanged);
            // 
            // panelWebhookOutputDHL
            // 
            this.panelWebhookOutputDHL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelWebhookOutputDHL.Controls.Add(this.dgvPostDHL);
            this.panelWebhookOutputDHL.Controls.Add(this.label42);
            this.panelWebhookOutputDHL.Controls.Add(this.dgvGetDHL);
            this.panelWebhookOutputDHL.Controls.Add(this.label43);
            this.panelWebhookOutputDHL.Controls.Add(this.label44);
            this.panelWebhookOutputDHL.Controls.Add(this.tbDHL_webhookURL);
            this.panelWebhookOutputDHL.Location = new System.Drawing.Point(4, 38);
            this.panelWebhookOutputDHL.Name = "panelWebhookOutputDHL";
            this.panelWebhookOutputDHL.Size = new System.Drawing.Size(844, 316);
            this.panelWebhookOutputDHL.TabIndex = 7;
            // 
            // dgvPostDHL
            // 
            this.dgvPostDHL.AllowUserToOrderColumns = true;
            this.dgvPostDHL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPostDHL.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPostDHL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPostDHL.Location = new System.Drawing.Point(20, 203);
            this.dgvPostDHL.Name = "dgvPostDHL";
            this.dgvPostDHL.Size = new System.Drawing.Size(813, 107);
            this.dgvPostDHL.TabIndex = 6;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label42.Location = new System.Drawing.Point(19, 47);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(30, 20);
            this.label42.TabIndex = 3;
            this.label42.Text = "Get";
            // 
            // dgvGetDHL
            // 
            this.dgvGetDHL.AllowUserToOrderColumns = true;
            this.dgvGetDHL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvGetDHL.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGetDHL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGetDHL.Location = new System.Drawing.Point(20, 70);
            this.dgvGetDHL.Name = "dgvGetDHL";
            this.dgvGetDHL.Size = new System.Drawing.Size(813, 107);
            this.dgvGetDHL.TabIndex = 5;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(13, 13);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(29, 13);
            this.label43.TabIndex = 1;
            this.label43.Text = "URL";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.label44.Location = new System.Drawing.Point(19, 180);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(37, 20);
            this.label44.TabIndex = 4;
            this.label44.Text = "Post";
            // 
            // tbDHL_webhookURL
            // 
            this.tbDHL_webhookURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDHL_webhookURL.Location = new System.Drawing.Point(190, 5);
            this.tbDHL_webhookURL.Multiline = true;
            this.tbDHL_webhookURL.Name = "tbDHL_webhookURL";
            this.tbDHL_webhookURL.Size = new System.Drawing.Size(643, 21);
            this.tbDHL_webhookURL.TabIndex = 2;
            this.tbDHL_webhookURL.TextChanged += new System.EventHandler(this.TbDHL_webhookURL_TextChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.textBox5);
            this.groupBox5.Controls.Add(this.label45);
            this.groupBox5.Controls.Add(this.button2);
            this.groupBox5.Controls.Add(this.tbInputFolderDHL);
            this.groupBox5.Controls.Add(this.btnSetInputFolderDHL);
            this.groupBox5.Controls.Add(this.label39);
            this.groupBox5.Location = new System.Drawing.Point(3, 97);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(873, 76);
            this.groupBox5.TabIndex = 29;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Input";
            // 
            // textBox5
            // 
            this.textBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox5.Location = new System.Drawing.Point(200, 46);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(577, 20);
            this.textBox5.TabIndex = 21;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(10, 53);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(66, 13);
            this.label45.TabIndex = 20;
            this.label45.Text = "File mapping";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(784, 45);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 22);
            this.button2.TabIndex = 22;
            this.button2.Text = "Set";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // tbInputFolderDHL
            // 
            this.tbInputFolderDHL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbInputFolderDHL.Location = new System.Drawing.Point(200, 15);
            this.tbInputFolderDHL.Name = "tbInputFolderDHL";
            this.tbInputFolderDHL.Size = new System.Drawing.Size(577, 20);
            this.tbInputFolderDHL.TabIndex = 15;
            this.tbInputFolderDHL.TextChanged += new System.EventHandler(this.tbInputFolderDHL_TextChanged);
            // 
            // btnSetInputFolderDHL
            // 
            this.btnSetInputFolderDHL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetInputFolderDHL.Location = new System.Drawing.Point(785, 14);
            this.btnSetInputFolderDHL.Name = "btnSetInputFolderDHL";
            this.btnSetInputFolderDHL.Size = new System.Drawing.Size(75, 22);
            this.btnSetInputFolderDHL.TabIndex = 16;
            this.btnSetInputFolderDHL.Text = "Set";
            this.btnSetInputFolderDHL.UseVisualStyleBackColor = true;
            this.btnSetInputFolderDHL.Click += new System.EventHandler(this.btnSetInputFolderDHL_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(10, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(60, 13);
            this.label39.TabIndex = 14;
            this.label39.Text = "Input folder";
            // 
            // groupBox11
            // 
            this.groupBox11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox11.Controls.Add(this.btnTemplateLabelDHL);
            this.groupBox11.Controls.Add(this.tbPathLabelTemplateDHL);
            this.groupBox11.Controls.Add(this.label31);
            this.groupBox11.Controls.Add(this.lbPrintNameDHL);
            this.groupBox11.Controls.Add(this.label34);
            this.groupBox11.Controls.Add(this.btnSetPrinterDHL);
            this.groupBox11.Location = new System.Drawing.Point(3, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(873, 88);
            this.groupBox11.TabIndex = 27;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Printer";
            // 
            // btnTemplateLabelDHL
            // 
            this.btnTemplateLabelDHL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTemplateLabelDHL.Location = new System.Drawing.Point(785, 50);
            this.btnTemplateLabelDHL.Name = "btnTemplateLabelDHL";
            this.btnTemplateLabelDHL.Size = new System.Drawing.Size(75, 22);
            this.btnTemplateLabelDHL.TabIndex = 35;
            this.btnTemplateLabelDHL.Text = "Change";
            this.btnTemplateLabelDHL.UseVisualStyleBackColor = true;
            this.btnTemplateLabelDHL.Click += new System.EventHandler(this.btnTemplateLabelDHL_Click);
            // 
            // tbPathLabelTemplateDHL
            // 
            this.tbPathLabelTemplateDHL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPathLabelTemplateDHL.Enabled = false;
            this.tbPathLabelTemplateDHL.Location = new System.Drawing.Point(200, 51);
            this.tbPathLabelTemplateDHL.Name = "tbPathLabelTemplateDHL";
            this.tbPathLabelTemplateDHL.Size = new System.Drawing.Size(579, 20);
            this.tbPathLabelTemplateDHL.TabIndex = 34;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(7, 55);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(76, 13);
            this.label31.TabIndex = 33;
            this.label31.Text = "Template label";
            // 
            // lbPrintNameDHL
            // 
            this.lbPrintNameDHL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPrintNameDHL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbPrintNameDHL.Location = new System.Drawing.Point(200, 16);
            this.lbPrintNameDHL.Name = "lbPrintNameDHL";
            this.lbPrintNameDHL.Size = new System.Drawing.Size(579, 21);
            this.lbPrintNameDHL.TabIndex = 32;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(7, 21);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(66, 13);
            this.label34.TabIndex = 31;
            this.label34.Text = "Printer name";
            // 
            // btnSetPrinterDHL
            // 
            this.btnSetPrinterDHL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetPrinterDHL.Image = ((System.Drawing.Image)(resources.GetObject("btnSetPrinterDHL.Image")));
            this.btnSetPrinterDHL.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSetPrinterDHL.Location = new System.Drawing.Point(831, 12);
            this.btnSetPrinterDHL.Name = "btnSetPrinterDHL";
            this.btnSetPrinterDHL.Size = new System.Drawing.Size(30, 31);
            this.btnSetPrinterDHL.TabIndex = 30;
            this.btnSetPrinterDHL.UseVisualStyleBackColor = true;
            this.btnSetPrinterDHL.Click += new System.EventHandler(this.btnSetPrinterDHL_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tbAccNumber);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.tbProduct);
            this.groupBox1.Controls.Add(this.tbCode);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.btnTestDhlSettings);
            this.groupBox1.Controls.Add(this.tbSignature);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.tbUser);
            this.groupBox1.Controls.Add(this.tbToken);
            this.groupBox1.Controls.Add(this.tbApplicationID);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Location = new System.Drawing.Point(3, 179);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(873, 213);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Account";
            // 
            // tbAccNumber
            // 
            this.tbAccNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAccNumber.Location = new System.Drawing.Point(200, 121);
            this.tbAccNumber.Name = "tbAccNumber";
            this.tbAccNumber.Size = new System.Drawing.Size(659, 20);
            this.tbAccNumber.TabIndex = 41;
            this.tbAccNumber.TextChanged += new System.EventHandler(this.tbAccNumber_TextChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(5, 126);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(85, 13);
            this.label28.TabIndex = 40;
            this.label28.Text = "Account number";
            // 
            // tbProduct
            // 
            this.tbProduct.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProduct.Location = new System.Drawing.Point(200, 177);
            this.tbProduct.Name = "tbProduct";
            this.tbProduct.Size = new System.Drawing.Size(659, 20);
            this.tbProduct.TabIndex = 39;
            this.tbProduct.TextChanged += new System.EventHandler(this.tbProduct_TextChanged);
            // 
            // tbCode
            // 
            this.tbCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCode.Location = new System.Drawing.Point(200, 148);
            this.tbCode.Name = "tbCode";
            this.tbCode.Size = new System.Drawing.Size(659, 20);
            this.tbCode.TabIndex = 38;
            this.tbCode.TextChanged += new System.EventHandler(this.tbCode_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 183);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 13);
            this.label27.TabIndex = 37;
            this.label27.Text = "DHL product";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(4, 152);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(95, 13);
            this.label21.TabIndex = 35;
            this.label21.Text = "DHL product code";
            // 
            // btnTestDhlSettings
            // 
            this.btnTestDhlSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestDhlSettings.Location = new System.Drawing.Point(785, 19);
            this.btnTestDhlSettings.Name = "btnTestDhlSettings";
            this.btnTestDhlSettings.Size = new System.Drawing.Size(75, 22);
            this.btnTestDhlSettings.TabIndex = 34;
            this.btnTestDhlSettings.Text = "Test";
            this.btnTestDhlSettings.UseVisualStyleBackColor = true;
            this.btnTestDhlSettings.Click += new System.EventHandler(this.btnTestDhlSettings_Click);
            // 
            // tbSignature
            // 
            this.tbSignature.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSignature.Location = new System.Drawing.Point(200, 95);
            this.tbSignature.Name = "tbSignature";
            this.tbSignature.Size = new System.Drawing.Size(659, 20);
            this.tbSignature.TabIndex = 32;
            this.tbSignature.TextChanged += new System.EventHandler(this.tbSignature_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(5, 100);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 13);
            this.label22.TabIndex = 30;
            this.label22.Text = "Signature";
            // 
            // tbUser
            // 
            this.tbUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUser.Location = new System.Drawing.Point(200, 70);
            this.tbUser.Name = "tbUser";
            this.tbUser.Size = new System.Drawing.Size(659, 20);
            this.tbUser.TabIndex = 29;
            this.tbUser.TextChanged += new System.EventHandler(this.tbUser_TextChanged);
            // 
            // tbToken
            // 
            this.tbToken.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbToken.Location = new System.Drawing.Point(200, 45);
            this.tbToken.Name = "tbToken";
            this.tbToken.Size = new System.Drawing.Size(659, 20);
            this.tbToken.TabIndex = 28;
            this.tbToken.TextChanged += new System.EventHandler(this.tbToken_TextChanged);
            // 
            // tbApplicationID
            // 
            this.tbApplicationID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbApplicationID.Location = new System.Drawing.Point(200, 20);
            this.tbApplicationID.Name = "tbApplicationID";
            this.tbApplicationID.Size = new System.Drawing.Size(579, 20);
            this.tbApplicationID.TabIndex = 27;
            this.tbApplicationID.TextChanged += new System.EventHandler(this.tbApplicationID_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(5, 75);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 13);
            this.label23.TabIndex = 26;
            this.label23.Text = "User";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(5, 50);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(38, 13);
            this.label24.TabIndex = 25;
            this.label24.Text = "Token";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(5, 25);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(70, 13);
            this.label25.TabIndex = 24;
            this.label25.Text = "ApplicationID";
            // 
            // tabPageSMTP
            // 
            this.tabPageSMTP.AutoScroll = true;
            this.tabPageSMTP.Controls.Add(this.groupBox13);
            this.tabPageSMTP.Controls.Add(this.groupBox9);
            this.tabPageSMTP.Location = new System.Drawing.Point(4, 22);
            this.tabPageSMTP.Name = "tabPageSMTP";
            this.tabPageSMTP.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSMTP.Size = new System.Drawing.Size(923, 750);
            this.tabPageSMTP.TabIndex = 4;
            this.tabPageSMTP.Text = "SMTP";
            this.tabPageSMTP.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox13.Controls.Add(this.btnTuningMailBody);
            this.groupBox13.Controls.Add(this.btnTemplateMail);
            this.groupBox13.Controls.Add(this.tbPathMailTemplate);
            this.groupBox13.Controls.Add(this.label32);
            this.groupBox13.Location = new System.Drawing.Point(8, 237);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(907, 96);
            this.groupBox13.TabIndex = 36;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Output";
            // 
            // btnTuningMailBody
            // 
            this.btnTuningMailBody.Location = new System.Drawing.Point(9, 60);
            this.btnTuningMailBody.Name = "btnTuningMailBody";
            this.btnTuningMailBody.Size = new System.Drawing.Size(102, 23);
            this.btnTuningMailBody.TabIndex = 19;
            this.btnTuningMailBody.Text = "Tuning Mail Body";
            this.btnTuningMailBody.UseVisualStyleBackColor = true;
            this.btnTuningMailBody.Click += new System.EventHandler(this.btnTuningMailBody_Click);
            // 
            // btnTemplateMail
            // 
            this.btnTemplateMail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTemplateMail.Location = new System.Drawing.Point(824, 17);
            this.btnTemplateMail.Name = "btnTemplateMail";
            this.btnTemplateMail.Size = new System.Drawing.Size(75, 22);
            this.btnTemplateMail.TabIndex = 18;
            this.btnTemplateMail.Text = "Change";
            this.btnTemplateMail.UseVisualStyleBackColor = true;
            this.btnTemplateMail.Click += new System.EventHandler(this.btnTemplateMail_Click);
            // 
            // tbPathMailTemplate
            // 
            this.tbPathMailTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPathMailTemplate.Enabled = false;
            this.tbPathMailTemplate.Location = new System.Drawing.Point(107, 19);
            this.tbPathMailTemplate.Name = "tbPathMailTemplate";
            this.tbPathMailTemplate.Size = new System.Drawing.Size(711, 20);
            this.tbPathMailTemplate.TabIndex = 17;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 26);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(51, 13);
            this.label32.TabIndex = 16;
            this.label32.Text = "Template";
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.label3);
            this.groupBox9.Controls.Add(this.label33);
            this.groupBox9.Controls.Add(this.label4);
            this.groupBox9.Controls.Add(this.tbFromMail);
            this.groupBox9.Controls.Add(this.label5);
            this.groupBox9.Controls.Add(this.tbPasswordSMTP);
            this.groupBox9.Controls.Add(this.btnTestSMTP);
            this.groupBox9.Controls.Add(this.tbUserName);
            this.groupBox9.Controls.Add(this.label6);
            this.groupBox9.Controls.Add(this.tbFrom);
            this.groupBox9.Controls.Add(this.label7);
            this.groupBox9.Controls.Add(this.tbPort);
            this.groupBox9.Controls.Add(this.tbHost);
            this.groupBox9.Location = new System.Drawing.Point(8, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(907, 225);
            this.groupBox9.TabIndex = 35;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Account";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Host";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(16, 171);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(51, 13);
            this.label33.TabIndex = 33;
            this.label33.Text = "From mail";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Port";
            // 
            // tbFromMail
            // 
            this.tbFromMail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFromMail.Location = new System.Drawing.Point(108, 168);
            this.tbFromMail.Name = "tbFromMail";
            this.tbFromMail.Size = new System.Drawing.Size(793, 20);
            this.tbFromMail.TabIndex = 34;
            this.tbFromMail.TextChanged += new System.EventHandler(this.tbFromMail_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 141);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "From";
            // 
            // tbPasswordSMTP
            // 
            this.tbPasswordSMTP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPasswordSMTP.Location = new System.Drawing.Point(108, 108);
            this.tbPasswordSMTP.Name = "tbPasswordSMTP";
            this.tbPasswordSMTP.Size = new System.Drawing.Size(793, 20);
            this.tbPasswordSMTP.TabIndex = 32;
            this.tbPasswordSMTP.TextChanged += new System.EventHandler(this.tbPassword_TextChanged);
            // 
            // btnTestSMTP
            // 
            this.btnTestSMTP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestSMTP.Location = new System.Drawing.Point(826, 196);
            this.btnTestSMTP.Name = "btnTestSMTP";
            this.btnTestSMTP.Size = new System.Drawing.Size(75, 23);
            this.btnTestSMTP.TabIndex = 28;
            this.btnTestSMTP.Text = "Test";
            this.btnTestSMTP.UseVisualStyleBackColor = true;
            this.btnTestSMTP.Click += new System.EventHandler(this.btnTestSMTP_Click);
            // 
            // tbUserName
            // 
            this.tbUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUserName.Location = new System.Drawing.Point(108, 78);
            this.tbUserName.Name = "tbUserName";
            this.tbUserName.Size = new System.Drawing.Size(793, 20);
            this.tbUserName.TabIndex = 30;
            this.tbUserName.TextChanged += new System.EventHandler(this.tbUserName_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "User name";
            // 
            // tbFrom
            // 
            this.tbFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFrom.Location = new System.Drawing.Point(108, 138);
            this.tbFrom.Name = "tbFrom";
            this.tbFrom.Size = new System.Drawing.Size(793, 20);
            this.tbFrom.TabIndex = 27;
            this.tbFrom.TextChanged += new System.EventHandler(this.tbFrom_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Password";
            // 
            // tbPort
            // 
            this.tbPort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPort.Location = new System.Drawing.Point(108, 48);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(793, 20);
            this.tbPort.TabIndex = 25;
            this.tbPort.TextChanged += new System.EventHandler(this.tbPort_TextChanged);
            // 
            // tbHost
            // 
            this.tbHost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbHost.Location = new System.Drawing.Point(108, 18);
            this.tbHost.Name = "tbHost";
            this.tbHost.Size = new System.Drawing.Size(793, 20);
            this.tbHost.TabIndex = 23;
            this.tbHost.TextChanged += new System.EventHandler(this.tbHost_TextChanged);
            // 
            // tabPageGeneral
            // 
            this.tabPageGeneral.Location = new System.Drawing.Point(4, 22);
            this.tabPageGeneral.Name = "tabPageGeneral";
            this.tabPageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGeneral.Size = new System.Drawing.Size(923, 750);
            this.tabPageGeneral.TabIndex = 0;
            this.tabPageGeneral.Text = "General";
            this.tabPageGeneral.UseVisualStyleBackColor = true;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageGeneral);
            this.tabControl.Controls.Add(this.tabPageDeutschePost);
            this.tabControl.Controls.Add(this.tabPageDHL);
            this.tabControl.Controls.Add(this.tabPageSMTP);
            this.tabControl.Controls.Add(this.tabPageDatabase);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(931, 776);
            this.tabControl.TabIndex = 0;
            // 
            // tabPageDatabase
            // 
            this.tabPageDatabase.Controls.Add(this.checkBox2);
            this.tabPageDatabase.Controls.Add(this.checkBox1);
            this.tabPageDatabase.Controls.Add(this.groupBox2);
            this.tabPageDatabase.Location = new System.Drawing.Point(4, 22);
            this.tabPageDatabase.Name = "tabPageDatabase";
            this.tabPageDatabase.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDatabase.Size = new System.Drawing.Size(923, 750);
            this.tabPageDatabase.TabIndex = 7;
            this.tabPageDatabase.Text = "Database";
            this.tabPageDatabase.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(93, 14);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(58, 17);
            this.checkBox2.TabIndex = 2;
            this.checkBox2.Text = "SQLite";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(6, 14);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(61, 17);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "MySQL";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(6, 37);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(907, 205);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "MySQL";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 776);
            this.Controls.Add(this.tabControl);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.tabPageDeutschePost.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.panelFileOutputDP.ResumeLayout(false);
            this.panelFileOutputDP.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.panelWebhookOutputDP.ResumeLayout(false);
            this.panelWebhookOutputDP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPostDP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGetDP)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPageDHL.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.panelFileOutputDHL.ResumeLayout(false);
            this.panelFileOutputDHL.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.panelWebhookOutputDHL.ResumeLayout(false);
            this.panelWebhookOutputDHL.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPostDHL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGetDHL)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPageSMTP.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPageDatabase.ResumeLayout(false);
            this.tabPageDatabase.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialogMail;
        private System.Windows.Forms.TabPage tabPageDeutschePost;
        private System.Windows.Forms.TabPage tabPageDHL;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbAccNumber;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tbProduct;
        private System.Windows.Forms.TextBox tbCode;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btnTestDhlSettings;
        private System.Windows.Forms.TextBox tbSignature;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbUser;
        private System.Windows.Forms.TextBox tbToken;
        private System.Windows.Forms.TextBox tbApplicationID;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TabPage tabPageSMTP;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tbFromMail;
        private System.Windows.Forms.TextBox tbPasswordSMTP;
        private System.Windows.Forms.TextBox tbUserName;
        private System.Windows.Forms.TextBox tbFrom;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.TextBox tbHost;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnTestSMTP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPageGeneral;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label lbPrintNameDHL;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button btnSetPrinterDHL;
        private System.Windows.Forms.Button btnTemplateLabelDHL;
        private System.Windows.Forms.TextBox tbPathLabelTemplateDHL;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tbInputFolderDHL;
        private System.Windows.Forms.Button btnSetInputFolderDHL;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.CheckBox cbFileDHL;
        private System.Windows.Forms.Panel panelFileOutputDHL;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox tb_ExportFileNameDHL;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.CheckBox cbWebhookDHL;
        private System.Windows.Forms.Panel panelWebhookOutputDHL;
        private System.Windows.Forms.DataGridView dgvPostDHL;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.DataGridView dgvGetDHL;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tbDHL_webhookURL;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.TextBox tbInputFolderDP;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSetInputFolderDP;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.CheckBox cbFileDP;
        private System.Windows.Forms.Panel panelFileOutputDP;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox tbExportFolderDP;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_ExportFileNameDP;
        private System.Windows.Forms.Button btnSetExportFolderDP;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.CheckBox cbWebhookDP;
        private System.Windows.Forms.Panel panelWebhookOutputDP;
        private System.Windows.Forms.DataGridView dgvPostDP;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.DataGridView dgvGetDP;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tbDP_webhookURL;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button testDPAccount;
        private System.Windows.Forms.TextBox tbPasswordDeutschePost;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbSandboxDP_Price;
        private System.Windows.Forms.TextBox tbSandboxDP_ProductCode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbSandboxDP_Password;
        private System.Windows.Forms.TextBox tbSandboxDP_Mail;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbMail;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbSCHLUESSEL_DPWN_MARKTPLATZ;
        private System.Windows.Forms.TextBox tbKEY_PHASE;
        private System.Windows.Forms.TextBox tbPARTNER_ID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbPriceForCode;
        private System.Windows.Forms.TextBox tbProductCode;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button btnTemplateLabelDP;
        private System.Windows.Forms.TextBox tbPathLabelTemplateDP;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbPrintNameDP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSetPrinterDP;
        private System.Windows.Forms.Button btnSetExportFolderDHL;
        private System.Windows.Forms.TextBox tbExportFolderDHL;
        private System.Windows.Forms.Button btnTuningMailBody;
        private System.Windows.Forms.Button btnTemplateMail;
        private System.Windows.Forms.TextBox tbPathMailTemplate;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage tabPageDatabase;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}