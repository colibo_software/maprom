﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabelPrint.Forms
{
    public partial class TestSMTP : Form
    {
        private string m_host;
        private string m_port;
        private string m_userName;
        private string m_password;
        private string m_from;
        private string m_fromMail;
        public TestSMTP(string Host, string Port, string UserName, string Password, string From, string FromMail)
        {
            m_host = Host;
            m_port = Port;
            m_userName = UserName;
            m_password = Password;
            m_from = From;
            m_fromMail = FromMail;

            InitializeComponent();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            var client = new SmtpClient(m_host, int.Parse(m_port))
            {
                Credentials = new NetworkCredential(m_userName, m_password),
                EnableSsl = true
            };
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                System.Security.Cryptography.X509Certificates.X509Chain chain,
                System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

            try
            {
                client.Send(m_from, tbRecipient.Text, m_fromMail, tbBody.Text);
            }
            catch (Exception exception)
            {
                MessageBox.Show($"{exception.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void TestSMTP_Load(object sender, EventArgs e)
        {
            lbHost.Text = m_host;
            lbPort.Text = m_port;
            lbUserName.Text = m_userName;
            lbPassword.Text = m_password;
            lbFrom.Text = m_from;
            lbFromMail.Text = m_fromMail;
        }
    }
}
