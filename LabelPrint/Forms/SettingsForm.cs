﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DbEntity;
using LabelPrint.DeutschePostService;
using LabelPrint.DHL_Service;
using LabelPrint.Forms;
using LabelPrint.Services;
using Newtonsoft.Json;

namespace LabelPrint
{
    public partial class SettingsForm : System.Windows.Forms.Form
    {
        private Settings _settings;
        private bool _status;
        private bool _resize;
        public SettingsForm(Settings settings, bool status)
        {
            InitializeComponent();
            _settings = settings;
            _status = status;
            _resize = false;
        }

        #region tabPagePrinters

        private void btnSetPrinterDP_Click(object sender, EventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                lbPrintNameDP.Text = printDialog.PrinterSettings.PrinterName;
                _settings.SettingsPrinter.PrinterNameDP = lbPrintNameDP.Text;//ToDo
            }
        }
        private void btnSetPrinterDHL_Click(object sender, EventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                lbPrintNameDHL.Text = printDialog.PrinterSettings.PrinterName;
                _settings.SettingsPrinter.PrinterNameDHL = lbPrintNameDHL.Text;//ToDo
            }
        }
        private void btnTemplateLabelDP_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _settings.SettingsPrinter.PathTemplateLabelDeutschePost = openFileDialog.FileName;
                tbPathLabelTemplateDP.Text = _settings.SettingsPrinter.PathTemplateLabelDeutschePost;
            }
        }

        private void btnTemplateLabelDHL_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _settings.SettingsPrinter.PathTemplateLabelDHL = openFileDialog.FileName;
                tbPathLabelTemplateDHL.Text = _settings.SettingsPrinter.PathTemplateLabelDHL;
            }
        }

        private void btnTemplateMail_Click(object sender, EventArgs e)
        {
            if (openFileDialogMail.ShowDialog() == DialogResult.OK)
            {
                _settings.MailSettings.PathTemplateMail = openFileDialogMail.FileName;
                tbPathMailTemplate.Text = _settings.MailSettings.PathTemplateMail;
            }

        }

        //private void btnTemplateInvoiceDHL_Click(object sender, EventArgs e)
        //{
        //    if (openFileDialog.ShowDialog() == DialogResult.OK)
        //    {
        //        _settings.SettingsPrinter.PathTemplateInvoiceDHL = openFileDialog.FileName;
        //        tbPathIncoiceTemplateDHL.Text = _settings.SettingsPrinter.PathTemplateInvoiceDHL;
        //    }
        //}


        #endregion

        #region tabPageGeneral

        #region DP

        private void tbInputFolderDP_TextChanged(object sender, EventArgs e)
        {
            _settings.GeneralSettings.InputFolderDP = tbInputFolderDP.Text;
        }
        private void tbExportFolderDP_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DeutschePostAccount.OutputSettings.FileHook.ExportFolderDP = tbExportFolderDP.Text;
        }

        private void btnSetInputFolderDP_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                _settings.GeneralSettings.InputFolderDP = folderBrowserDialog.SelectedPath;
                tbInputFolderDP.Text = _settings.GeneralSettings.InputFolderDP;
            }
        }

        private void btnSetExportFolderDP_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                _settings.SettingsAccount.DeutschePostAccount.OutputSettings.FileHook.ExportFolderDP = folderBrowserDialog.SelectedPath;
                tbExportFolderDP.Text = _settings.SettingsAccount.DeutschePostAccount.OutputSettings.FileHook.ExportFolderDP;
            }

        }

        #endregion

        #region DHL

        private void tbInputFolderDHL_TextChanged(object sender, EventArgs e)
        {
            _settings.GeneralSettings.InputFolderDHL = tbInputFolderDHL.Text;
        }

        private void tbExportFolderDHL_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DhlAccount.OutputSettings.FileHook.ExportFolderDHL = tbExportFolderDHL.Text;
        }

        private void btnSetInputFolderDHL_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                _settings.GeneralSettings.InputFolderDHL = folderBrowserDialog.SelectedPath;
                tbInputFolderDHL.Text = _settings.GeneralSettings.InputFolderDHL;
            }
        }

        private void btnSetExportFolderDHL_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                _settings.SettingsAccount.DhlAccount.OutputSettings.FileHook.ExportFolderDHL = folderBrowserDialog.SelectedPath;
                tbExportFolderDHL.Text = _settings.SettingsAccount.DhlAccount.OutputSettings.FileHook.ExportFolderDHL;
            }
        }

        #endregion

        #endregion



        #region Deutsche Post

        private void tbPasswordDeutschePost_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DeutschePostAccount.Password = tbPasswordDeutschePost.Text;
        }

        private void tbKEY_PHASE_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DeutschePostAccount.KEY_PHASE = tbKEY_PHASE.Text;
        }

        private void tbSCHLUESSEL_DPWN_MARKTPLATZ_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DeutschePostAccount.SCHLUESSEL_DPWN_MARKTPLATZ = tbSCHLUESSEL_DPWN_MARKTPLATZ.Text;
        }

        private void tbMail_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DeutschePostAccount.EMail = tbMail.Text;
        }

        private void tbPARTNER_ID_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DeutschePostAccount.PARTNER_ID = tbPARTNER_ID.Text;
        }

        private void tbPriceForCode_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbPriceForCode.Text))
            {
                tbPriceForCode.ResetBackColor();
                if (Int32.TryParse(tbPriceForCode.Text.Trim(), out int price))
                {
                    _settings.SettingsAccount.DeutschePostAccount.PriceForCode = price;
                }
                else
                {
                    tbPriceForCode.BackColor = Color.Salmon;
                }
            }
            else
            {
                tbPriceForCode.BackColor = Color.Salmon;
            }
        }

        private void tbProductCode_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbProductCode.Text))
            {
                tbProductCode.ResetBackColor();
                if (Int32.TryParse(tbProductCode.Text.Trim(), out int code))
                {
                    _settings.SettingsAccount.DeutschePostAccount.ProductCode = code;
                }
                else
                {
                    tbProductCode.BackColor = Color.Salmon;
                }
            }
            else
            {
                tbProductCode.BackColor = Color.Salmon;
            }
        }

        private void testDPAccount_Click(object sender, EventArgs e)
        {
            CustomHeaderMessageInspector._partnerId = tbPARTNER_ID.Text;
            CustomHeaderMessageInspector._keyPhase = tbKEY_PHASE.Text;
            CustomHeaderMessageInspector._signatureKey = tbSCHLUESSEL_DPWN_MARKTPLATZ.Text;

            Task.Run(() =>
            {
                using (OneClickForAppPortTypeV3Client client = new OneClickForAppPortTypeV3Client())
                {
                    try
                    {
                        AuthenticateUserResponseType authInfo = client.authenticateUser(
                            new AuthenticateUserRequestType()
                            {
                                password = tbPasswordDeutschePost.Text,
                                username = tbMail.Text
                            });
                        MessageBox.Show("OK", "Result", MessageBoxButtons.OK, MessageBoxIcon.None);
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Message, "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            });
        }

        #region Sandbox

        private void tbSandboxDP_Mail_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DeutschePostAccount.SandboxDeutschePostAccount.EMail = tbSandboxDP_Mail.Text;
        }

        private void tbSandboxDP_Password_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DeutschePostAccount.SandboxDeutschePostAccount.Password = tbSandboxDP_Password.Text;
        }

        private void tbSandboxDP_Price_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbSandboxDP_Price.Text))
            {
                tbSandboxDP_Price.ResetBackColor();
                if (Int32.TryParse(tbSandboxDP_Price.Text.Trim(), out int price))
                {
                    _settings.SettingsAccount.DeutschePostAccount.SandboxDeutschePostAccount.PriceForCode = price;
                }
                else
                {
                    tbSandboxDP_Price.BackColor = Color.Salmon;
                }
            }
            else
            {
                tbSandboxDP_Price.BackColor = Color.Salmon;
            }
        }

        private void tbSandboxDP_ProductCode_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbSandboxDP_ProductCode.Text))
            {
                tbSandboxDP_ProductCode.ResetBackColor();
                if (Int32.TryParse(tbSandboxDP_ProductCode.Text.Trim(), out int price))
                {
                    _settings.SettingsAccount.DeutschePostAccount.SandboxDeutschePostAccount.ProductCode = price;
                }
                else
                {
                    tbSandboxDP_ProductCode.BackColor = Color.Salmon;
                }
            }
            else
            {
                tbSandboxDP_ProductCode.BackColor = Color.Salmon;
            }
        }
        #endregion

        #endregion

        #region SMTP

        private void tbFrom_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.SmtpAccount.From = tbFrom.Text;
        }

        private void tbHost_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.SmtpAccount.Host = tbHost.Text;
        }

        private void tbPort_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbPort.Text))
            {
                tbPort.ResetBackColor();
                if (Int32.TryParse(tbPort.Text.Trim(), out int port))
                {
                    _settings.SettingsAccount.SmtpAccount.Port = port;
                }
                else
                {
                    tbPort.BackColor = Color.Salmon;
                }
            }
            else
            {
                tbPort.BackColor = Color.Salmon;
            }
        }

        private void tbUserName_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.SmtpAccount.UserName = tbUserName.Text;
        }

        private void tbPassword_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.SmtpAccount.Password = tbPasswordSMTP.Text;
        }

        private void tbFromMail_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.SmtpAccount.FromMail = tbFromMail.Text;
        }

        private void btnTestSMTP_Click(object sender, EventArgs e)
        {
            TestSMTP testSmtp = new TestSMTP(tbHost.Text, tbPort.Text, tbUserName.Text, tbPasswordSMTP.Text, tbFrom.Text, tbFromMail.Text);
            testSmtp.ShowDialog(this);
        }

        #endregion

        #region DHL

        private void tbProduct_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DhlAccount.Product = tbProduct.Text;
        }

        private void tbToken_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DhlAccount.Token = tbToken.Text;
        }

        private void tbUser_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DhlAccount.User = tbUser.Text;
        }

        private void tbSignature_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DhlAccount.Signature = tbSignature.Text;
        }

        private void tbCode_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DhlAccount.Code = tbCode.Text;
        }

        private void tbApplicationID_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DhlAccount.ApplicationID = tbApplicationID.Text;
        }
        private void tbAccNumber_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DhlAccount.AccountNumber = tbAccNumber.Text;
        }

        private void btnTestDhlSettings_Click(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                using (GKVAPIServicePortTypeClient client = new GKVAPIServicePortTypeClient())
                {
                    client.ClientCredentials.UserName.UserName = _settings.SettingsAccount.DhlAccount.ApplicationID;
                    client.ClientCredentials.UserName.Password = _settings.SettingsAccount.DhlAccount.Token;
                    GetManifestResponse tmp = null;
                    try
                    {
                        tmp = client.getManifest(
                            new AuthentificationType()
                            {
                                user = _settings.SettingsAccount.DhlAccount.User,
                                signature = _settings.SettingsAccount.DhlAccount.Signature
                            },
                            new GetManifestRequest()
                            {
                                Version = new DHL_Service.Version() { majorRelease = "2", minorRelease = "0" },
                                manifestDate = DateTime.Now.ToString("yyyy-MM-dd")
                            });
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        client.Abort();
                        return;
                    }

                    if (!tmp.Status.statusCode.Equals("2000"))
                    {
                        MessageBox.Show(tmp.Status.statusText, "Result", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show($"OK");
                    }

                }
            });
        }
        #endregion


        private void SettingsForm_Load(object sender, EventArgs e)
        {
            DoubleBuffered = true;

            tbPathLabelTemplateDP.Text = _settings.SettingsPrinter.PathTemplateLabelDeutschePost;
            tbPathLabelTemplateDHL.Text = _settings.SettingsPrinter.PathTemplateLabelDHL;
            tbPathMailTemplate.Text = _settings.MailSettings.PathTemplateMail;

            tbInputFolderDP.Text = _settings.GeneralSettings.InputFolderDP;
            tbExportFolderDP.Text = _settings.SettingsAccount.DeutschePostAccount.OutputSettings.FileHook.ExportFolderDP;
            tbInputFolderDHL.Text = _settings.GeneralSettings.InputFolderDHL;
            tbExportFolderDHL.Text = _settings.SettingsAccount.DhlAccount.OutputSettings.FileHook.ExportFolderDHL;

            lbPrintNameDHL.Text = _settings.SettingsPrinter.PrinterNameDHL;
            lbPrintNameDP.Text = _settings.SettingsPrinter.PrinterNameDP;

            tbHost.Text = _settings.SettingsAccount.SmtpAccount.Host;
            tbPort.Text = _settings.SettingsAccount.SmtpAccount.Port.ToString();
            tbUserName.Text = _settings.SettingsAccount.SmtpAccount.UserName;
            tbPasswordSMTP.Text = _settings.SettingsAccount.SmtpAccount.Password;
            tbFrom.Text = _settings.SettingsAccount.SmtpAccount.From;
            tbFromMail.Text = _settings.SettingsAccount.SmtpAccount.FromMail;

            tbPARTNER_ID.Text = _settings.SettingsAccount.DeutschePostAccount.PARTNER_ID;
            tbKEY_PHASE.Text = _settings.SettingsAccount.DeutschePostAccount.KEY_PHASE;
            tbSCHLUESSEL_DPWN_MARKTPLATZ.Text = _settings.SettingsAccount.DeutschePostAccount.SCHLUESSEL_DPWN_MARKTPLATZ;
            tbMail.Text = _settings.SettingsAccount.DeutschePostAccount.EMail;
            tbPasswordDeutschePost.Text = _settings.SettingsAccount.DeutschePostAccount.Password;

            tbProductCode.Text = _settings.SettingsAccount.DeutschePostAccount.ProductCode.ToString();
            tbPriceForCode.Text = _settings.SettingsAccount.DeutschePostAccount.PriceForCode.ToString();

            tbSandboxDP_Mail.Text = _settings.SettingsAccount.DeutschePostAccount.SandboxDeutschePostAccount.EMail;
            tbSandboxDP_Password.Text = _settings.SettingsAccount.DeutschePostAccount.SandboxDeutschePostAccount.Password;
            tbSandboxDP_Price.Text = _settings.SettingsAccount.DeutschePostAccount.SandboxDeutschePostAccount.PriceForCode.ToString();
            tbSandboxDP_ProductCode.Text = _settings.SettingsAccount.DeutschePostAccount.SandboxDeutschePostAccount.ProductCode.ToString();

            tbApplicationID.Text = _settings.SettingsAccount.DhlAccount.ApplicationID;
            tbToken.Text = _settings.SettingsAccount.DhlAccount.Token;
            tbUser.Text = _settings.SettingsAccount.DhlAccount.User;
            tbSignature.Text = _settings.SettingsAccount.DhlAccount.Signature;
            tbCode.Text = _settings.SettingsAccount.DhlAccount.Code;
            tbProduct.Text = _settings.SettingsAccount.DhlAccount.Product;
            tbAccNumber.Text = _settings.SettingsAccount.DhlAccount.AccountNumber;

            tabPageGeneral.Enabled = !_status;
            tabPageDeutschePost.Enabled = !_status;
            tabPageDHL.Enabled = !_status;
            tabPageSMTP.Enabled = !_status;


            dgvGetDP.DataSource = _settings.SettingsAccount.DeutschePostAccount.OutputSettings.WebHook.PairsGET;
            dgvPostDP.DataSource = _settings.SettingsAccount.DeutschePostAccount.OutputSettings.WebHook.PairsPOST;

            dgvGetDHL.DataSource = _settings.SettingsAccount.DhlAccount.OutputSettings.WebHook.PairsGET;
            dgvPostDHL.DataSource = _settings.SettingsAccount.DhlAccount.OutputSettings.WebHook.PairsPOST;

            cbWebhookDP.Checked = _settings.SettingsAccount.DeutschePostAccount.OutputSettings.IsWebHook;
            cbFileDP.Checked = _settings.SettingsAccount.DeutschePostAccount.OutputSettings.IsFile;
            cbWebhookDHL.Checked = _settings.SettingsAccount.DhlAccount.OutputSettings.IsWebHook;
            cbFileDHL.Checked = _settings.SettingsAccount.DhlAccount.OutputSettings.IsFile;

            panelWebhookOutputDP.Enabled = cbWebhookDP.Checked;
            panelFileOutputDP.Enabled = cbFileDP.Checked;
            panelWebhookOutputDHL.Enabled = cbWebhookDHL.Checked;
            panelFileOutputDHL.Enabled = cbFileDHL.Checked;

            tbDP_webhookURL.Text = _settings.SettingsAccount.DeutschePostAccount.OutputSettings.WebHook.Url;
            tbDHL_webhookURL.Text = _settings.SettingsAccount.DhlAccount.OutputSettings.WebHook.Url;

        }
        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _settings.Serialize();//ToDO проверка перед закрытием
        }



        private void btnTuningMailBody_Click(object sender, EventArgs e)
        {
            TestMailBody mailBodyForm = new TestMailBody(_settings);
            mailBodyForm.ShowDialog();
        }

        private void DgvGetDP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        #region resize lag

        protected override void OnResizeBegin(EventArgs e)
        {
            SuspendLayout();
            base.OnResizeBegin(e);
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            ResumeLayout();
            base.OnResizeEnd(e);
        }

        #endregion

        private void CbWebhookDP_CheckedChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DeutschePostAccount.OutputSettings.IsWebHook = cbWebhookDP.Checked;
            panelWebhookOutputDP.Enabled = cbWebhookDP.Checked;
        }

        private void CbFileDP_CheckedChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DeutschePostAccount.OutputSettings.IsFile = cbFileDP.Checked;
            panelFileOutputDP.Enabled = cbFileDP.Checked;
        }

        private void CbWebhookDHL_CheckedChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DhlAccount.OutputSettings.IsWebHook = cbWebhookDHL.Checked;
            panelWebhookOutputDHL.Enabled = cbWebhookDHL.Checked;
        }

        private void CbFileDHL_CheckedChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DhlAccount.OutputSettings.IsFile = cbFileDHL.Checked;
            panelFileOutputDHL.Enabled = cbFileDHL.Checked;
        }

        private void TbDP_webhookURL_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DeutschePostAccount.OutputSettings.WebHook.Url = tbDP_webhookURL.Text;
        }

        private void TbDHL_webhookURL_TextChanged(object sender, EventArgs e)
        {
            _settings.SettingsAccount.DhlAccount.OutputSettings.WebHook.Url = tbDHL_webhookURL.Text;
        }
    }
}
