﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Runtime;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml.Serialization;
using CsvHelper;
using DbEntity;
using DbEntity.db;
using DbEntity.dp;
using DbEntity.json;
using DeutschePost;
using GemBox.Document;
using LabelPrint.DeutschePostService;
using LabelPrint.handlers;
using LabelPrint.Services;
using Microsoft.VisualBasic.FileIO;
using NLog;
using FieldType = GemBox.Document.FieldType;
using Logger = NLog.Logger;
using Timer = System.Timers.Timer;

namespace LabelPrint
{
    public partial class Form1 : Form
    {

        private bool status;
        private Settings Settings;
        private Timer timer;
        private MyContext _myContext;

        private HandlerDeutschePost _deutschePostHandler;

        private HandlerDHL _handlerDhl;
        //private LoggerForm LoggerForm;
        private Logger _logger;
        public Form1()
        {
            InitializeComponent();
            ComponentInfo.SetLicense("DKMF-J4A9-6EMI-KGDV"); //HACK | 30.11.12 - no hack

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            _logger = LogManager.GetCurrentClassLogger();
            try
            {
                string md = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                Directory.CreateDirectory(md + "\\Maprom");
                if (!File.Exists(md + "\\Maprom\\database.db"))
                {
                    File.Copy("database\\database.db", md + "\\Maprom\\database.db");
                }

                AppDomain.CurrentDomain.SetData("DataDirectory", md + "\\Maprom\\");

                _myContext = new MyContext();
                try
                {

                    _myContext.Settingses.Load();

                    Settings = _myContext.Settingses.Local.FirstOrDefault();

                    if (Settings == null)
                    {
                        Settings = new Settings(_myContext);
                    }
                    else
                    {
                        Settings._context = _myContext;
                        Settings.Deserialize();
                    }
                }
                catch (ArgumentNullException exception)
                {
                    _logger.Fatal(exception);
                    _logger.Warn("Set the settings!");
                }
                catch (Exception exception)
                {
                    _logger.Fatal(exception);
                }


                checkBox1.Checked = Settings.IsSandbox;


                status = false;


                //_myContext.Logs.Add(new Log(){Message = "22"});

                //var g = _myContext.Logs.Local;


                //_handler = new Handler(Settings, _myContext, _logger);

                //LoggerForm = new LoggerForm(rtbLog);

                timer = new Timer(3000);
                //timer.Enabled = true;
                timer.Elapsed += Timer_Elapsed;
            }
            catch (Exception exception)
            {
                _logger.Fatal(exception);
            }

        }


        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _logger.Trace("tick");

            //Debug.WriteLine("tick");
            Task.Run(() =>
            {
                //Debug.WriteLine("new task");
                _logger.Trace("stop");
                timer.Stop();
                try
                {
                    DP();
                    DHL();
                }
                catch (Exception exception)
                {
                    _logger.Fatal(exception);
                }
                finally
                {
                    if (status)
                    {
                        _logger.Trace("start");
                        timer.Start();
                    }
                }
            });

        }

        private void DHL()
        {
            string getStreet(string str)
            {
                return str.Replace(getStreetNr(str), "").Trim('.', ' ', ',', '-');
            }
            string getStreetNr(string str)
            {
                string tmp = Regex.Match(str, "[.,:\\\\/|;\\s]*(\\d[-,\\\\|/]*)+.{0,4}$", RegexOptions.IgnoreCase).Value.Trim('.', ' ', ',', '-');
                if (string.IsNullOrWhiteSpace(tmp))
                {
                    tmp = Regex.Match(str, "^(\\d[\\w]*)+", RegexOptions.IgnoreCase).Value.Trim('.', ' ', ',', '-');
                    if (string.IsNullOrWhiteSpace(tmp))
                    {
                        return "";
                    }
                    else
                    {
                        return tmp;
                    }
                }
                return tmp;
            }
            try
            {
                var files = Directory.GetFiles(Settings.GeneralSettings.InputFolderDHL).Where(s => Path.GetExtension(s).ToLower() == ".txt").ToList();
                if (files.Count() != 0)
                {

                    _handlerDhl = new HandlerDHL(Settings, _myContext, _logger);
                    Etikett tmpEtikett = null;
                    foreach (var file in files)
                    {
                        try
                        {
                            using (StreamReader streamReader = new StreamReader(file))
                            {
                                using (CsvReader csvReader = new CsvReader(streamReader) { Configuration = { Delimiter = ";", HasHeaderRecord = true } })
                                {

                                    while (csvReader.Read())
                                    {
                                        var records = csvReader.GetRecord<CsvData>();
                                        tmpEtikett = new Etikett()
                                        {
                                            order_id = Int32.Parse(records.order_id),
                                            order_nr = records.order_id,
                                            Date = DateTime.Parse(records.date),
                                            sender = new Sender()
                                            {
                                                company = records.senderCompanyOrName,
                                                street = getStreet(records.senderStreet),
                                                street_nr = getStreetNr(records.senderStreet),
                                                city = records.senderCity,
                                                country = records.senderCountryId,
                                                postal_code = records.senderPostalCode,
                                                email = records.senderMail,
                                                phone = records.senderPhone,
                                            },
                                            delivery = new Delivery()
                                            {
                                                company = records.deliveryCompanyOrName,
                                                street = getStreet(records.deliveryStreet),
                                                street_nr = getStreetNr(records.deliveryStreet),
                                                city = records.deliveryCity,
                                                country = records.deliveryCountryId,
                                                postal_code = records.deliveryPostalCode,
                                                email = records.deliveryMail,
                                                phone = records.deliveryPhone,
                                            },
                                            invoice = new Invoice(),
                                            smtp = new SmtpServer()
                                            {
                                                FromMail = records.smtpFromMail,
                                                From = records.smtpFromName,
                                                Password = records.smtpPassword,
                                                Host = records.smtpHost,
                                                UserName = records.smtpUser,
                                                Port = Int32.Parse(records.smtpPort),
                                            },
                                            delivery_name = "dhl",
                                            user = "",
                                            weight = decimal.Parse(records.weight),
                                            api = new ApiDHL()
                                            {
                                                AccNumber = string.Concat(records.dhl_accNum_codeProduct.Take(10)),
                                                Code = string.Concat(records.dhl_accNum_codeProduct.Skip(10)),
                                                AppId = records.dhlAppId,
                                                Product = records.DHL_Product.Trim('.'),
                                                Signature = records.dhlSignature,
                                                Token = records.dhlToken,
                                                User = records.dhlUser
                                            },
                                            BankData = new BankData()
                                            {
                                                accountReference = records.accountReference,
                                                cashOnDeliveryPrice = records.cashOnDeliveryPrice,
                                                iban = records.iban,
                                                bic = records.bic,
                                                accountOwner = records.accountOwner,
                                                bankName = records.bankName
                                            }
                                        };

                                        if (tmpEtikett != null)
                                        {
                                            try
                                            {
                                                _handlerDhl.Job(tmpEtikett, false);
                                            }
                                            catch (Exception)
                                            {
                                                throw;
                                            }
                                            finally
                                            {
                                                File.Delete(file);
                                            }

                                            //File.Delete(file);//todo удаление после гарантированного завершения
                                        }
                                        else
                                        {
                                            throw new Exception("Etikett was null");

                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception exception)
                        {
                            _logger.Fatal(exception);
                        }


                    }
                }
            }
            catch (ArgumentException exception)
            {
                _logger.Fatal(exception.Message);
            }
            catch (DirectoryNotFoundException exception)
            {
                _logger.Fatal(exception.Message);
            }
            catch (Exception ex)
            {
                _logger.Fatal(ex);
            }
        }

        private void DP()
        {
            try
            {
                var files = Directory.GetFiles(Settings.GeneralSettings.InputFolderDP).Where(s => Path.GetExtension(s).ToLower() == ".xml").ToList();
                if (files.Count() != 0)
                {

                    _deutschePostHandler = new HandlerDeutschePost(Settings, _myContext, _logger);
                    foreach (var file in files)
                    {
                        XmlAttributeOverrides attrOverrides = new XmlAttributeOverrides();
                        XmlAttributes attrs = new XmlAttributes();
                        XmlElementAttribute attr = new XmlElementAttribute("api", typeof(ApiDP));
                        attrs.XmlElements.Add(attr);
                        attrOverrides.Add(typeof(Etikett), "api", attrs);

                        XmlSerializer formatter = new XmlSerializer(typeof(Etikett), attrOverrides);
                        using (FileStream fs = new FileStream(file, FileMode.OpenOrCreate))
                        {
                            try
                            {

                                Etikett tmpEtikett = (Etikett)formatter.Deserialize(fs);

                                if (tmpEtikett != null)
                                {
                                    _deutschePostHandler.Job(tmpEtikett, false);

                                }
                                else
                                {
                                    throw new Exception("Etikett was null");

                                }
                            }
                            catch (Exception exception)
                            {
                                _logger.Fatal(exception);
                            }
                            finally
                            {
                                File.Delete(file);
                            }
                        }
                    }
                }
            }
            catch (ArgumentException exception)
            {
                _logger.Fatal(exception.Message);
            }
            catch (DirectoryNotFoundException exception)
            {
                _logger.Fatal(exception.Message);
            }
            catch (Exception ex)
            {
                _logger.Fatal(ex);
            }
        }

        private void toolStripButtonSettings_Click(object sender, EventArgs e)
        {
            SettingsForm settingsForm = new SettingsForm(Settings, status);
            settingsForm.ShowDialog(this);


        }


        private void toolStripButtonStop_Click(object sender, EventArgs e)
        {
            //ToDo try
            status = false;
            timer.Stop();
            _logger.Info("Stop");
            toolStripButton1.Enabled = !status;
            toolStripButton2.Enabled = status;
        }
        private void toolStripButtonStart_Click(object sender, EventArgs e)
        {
            try
            {
                if (Settings.IsSandbox && MessageBox.Show("The mode sandbox is turned on.\n Do you want to continue?", "Sandbox", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
                {
                    return;
                }
                status = true;
                timer.Start();
                _logger.Info("Start");
                toolStripButton1.Enabled = !status;
                toolStripButton2.Enabled = status;
            }
            catch (Exception exception)
            {
                status = false;
                toolStripButton1.Enabled = !status;
                toolStripButton2.Enabled = status;
                _logger.Fatal(exception);
            }


        }

        private void tsBtnLog_Click(object sender, EventArgs e)
        {
            LogForm logForm = new LogForm(_myContext);
            logForm.ShowDialog();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            //_deutschePostHandler = new HandlerDeutschePost(Settings, _myContext, _logger);
            //XmlSerializer formatter = new XmlSerializer(typeof(Etikett));
            //using (FileStream fs = new FileStream("w.xml", FileMode.OpenOrCreate))
            //{
            //    try
            //    {
            //        Etikett tmpEtikett = (Etikett)formatter.Deserialize(fs);
            //        if (tmpEtikett != null)
            //        {
            //            _deutschePostHandler.Job(tmpEtikett, false);

            //        }
            //        else
            //        {
            //            _logger.Warn(""); //ToDo ?
            //        }
            //    }
            //    catch (Exception exception)
            //    {
            //        _logger.Fatal(exception.Message);
            //    }


            //}

            //todo user and delivery

            var unprocessedItem = _myContext.Etiketten
                .Include(x => x.Analyzer).Include(x => x.api).Include(x => x.invoice).Include(x => x.delivery).Include(x => x.sender).Include(x => x.smtp).Include(x => x.BankData)
                .Where(order => order.State == State.unprocessed).ToList();
            if (unprocessedItem.Count == 0)
            {
                return;
            }
            int counter = 0;
            StringBuilder stringBuilder = new StringBuilder();

            unprocessedItem.Select(x => x.user).Distinct().ToList().ForEach(user =>
            {
                stringBuilder.AppendLine($"{++counter}) {user}:");
                unprocessedItem.Where(x => x.user.Equals(user)).Select(x => x.delivery_name).Distinct().ToList().ForEach(s => stringBuilder.AppendLine(
                    $"\t{s} - {unprocessedItem.Count(x => x.delivery_name.Equals(s) && x.user.Equals(user))} orders"));
            });

            //foreach (var user in unprocessedItem.Select(x => x.user).Distinct())
            //{
            //    stringBuilder.AppendLine($"{++counter}) {user}:");
            //    unprocessedItem.Where(x => x.user.Equals(user)).Select(x => x.delivery_name).Distinct().ToList().ForEach(s => stringBuilder.AppendLine(
            //        $"\t{s} - {unprocessedItem.Count(x => x.delivery_name.Equals(s) && x.user.Equals(user))} orders"));
            //}
            if (MessageBox.Show(
                $"You have unprocessed {unprocessedItem.Count} orders!{Environment.NewLine}(OK - process now, Cancel - later){Environment.NewLine}{Environment.NewLine}" +
                $"{stringBuilder.ToString()}"
                , "Unprocessed Order", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {//ToDo list !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! несколько служб
                try
                {
                    _deutschePostHandler = new HandlerDeutschePost(Settings, _myContext, _logger);
                    _handlerDhl = new HandlerDHL(Settings, _myContext, _logger);
                    Task.Run(() =>
                    {
                        foreach (var etikett in unprocessedItem)
                        {
                            if (string.Equals(etikett.delivery_name, "dhl"))
                            {
                                _handlerDhl.Job(etikett, true);
                            }
                            else
                            {
                                _deutschePostHandler.Job(etikett, true);
                            }
                        }
                    });
                }
                catch (Exception exception)
                {
                    _logger.Fatal(exception);
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                Settings.IsSandbox = true;
            else
                Settings.IsSandbox = false;
            Settings.Serialize();
        }
    }
}
