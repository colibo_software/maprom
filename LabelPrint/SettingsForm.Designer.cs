﻿namespace LabelPrint
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageGeneral = new System.Windows.Forms.TabPage();
            this.cbLogging = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbInterval = new System.Windows.Forms.TextBox();
            this.tbTimeOut = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnChangeLogFolder = new System.Windows.Forms.Button();
            this.tbLogFolderPath = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.tabPagePrinterSettings = new System.Windows.Forms.TabPage();
            this.tabPagePrintersPanel = new System.Windows.Forms.Panel();
            this.tabControlPrinterSettings = new System.Windows.Forms.TabControl();
            this.tabPagePrinterUserSettings = new System.Windows.Forms.TabPage();
            this.btnAddPairUserPrinter = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.tabPagePrinterDocumentSettings = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnTemplateInvoiceDeutschePost = new System.Windows.Forms.Button();
            this.tbPathIncoiceTemplateDeutsche = new System.Windows.Forms.TextBox();
            this.btnTemplateLabelDeutschePost = new System.Windows.Forms.Button();
            this.tbPathLabelTemplateDeutsche = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPageAccounts = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.tbEMail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbKeyPhase = new System.Windows.Forms.TextBox();
            this.tbPartnerId = new System.Windows.Forms.TextBox();
            this.tbSchluesselDpwnMarktplatz = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPageData = new System.Windows.Forms.TabPage();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbDeutschePostURL = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tabControl.SuspendLayout();
            this.tabPageGeneral.SuspendLayout();
            this.tabPagePrinterSettings.SuspendLayout();
            this.tabPagePrintersPanel.SuspendLayout();
            this.tabControlPrinterSettings.SuspendLayout();
            this.tabPagePrinterUserSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.tabPagePrinterDocumentSettings.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPageAccounts.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPageData.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageGeneral);
            this.tabControl.Controls.Add(this.tabPagePrinterSettings);
            this.tabControl.Controls.Add(this.tabPageAccounts);
            this.tabControl.Controls.Add(this.tabPageData);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(611, 277);
            this.tabControl.TabIndex = 0;
            // 
            // tabPageGeneral
            // 
            this.tabPageGeneral.Controls.Add(this.cbLogging);
            this.tabPageGeneral.Controls.Add(this.label8);
            this.tabPageGeneral.Controls.Add(this.tbInterval);
            this.tabPageGeneral.Controls.Add(this.tbTimeOut);
            this.tabPageGeneral.Controls.Add(this.label7);
            this.tabPageGeneral.Controls.Add(this.label6);
            this.tabPageGeneral.Controls.Add(this.btnChangeLogFolder);
            this.tabPageGeneral.Controls.Add(this.tbLogFolderPath);
            this.tabPageGeneral.Controls.Add(this.label);
            this.tabPageGeneral.Location = new System.Drawing.Point(4, 22);
            this.tabPageGeneral.Name = "tabPageGeneral";
            this.tabPageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGeneral.Size = new System.Drawing.Size(603, 251);
            this.tabPageGeneral.TabIndex = 0;
            this.tabPageGeneral.Text = "General";
            this.tabPageGeneral.UseVisualStyleBackColor = true;
            // 
            // cbLogging
            // 
            this.cbLogging.AutoSize = true;
            this.cbLogging.Checked = true;
            this.cbLogging.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLogging.Location = new System.Drawing.Point(154, 116);
            this.cbLogging.Name = "cbLogging";
            this.cbLogging.Size = new System.Drawing.Size(15, 14);
            this.cbLogging.TabIndex = 8;
            this.cbLogging.UseVisualStyleBackColor = true;
            this.cbLogging.CheckedChanged += new System.EventHandler(this.cbLogging_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Logging";
            // 
            // tbInterval
            // 
            this.tbInterval.Location = new System.Drawing.Point(154, 44);
            this.tbInterval.Name = "tbInterval";
            this.tbInterval.Size = new System.Drawing.Size(62, 20);
            this.tbInterval.TabIndex = 6;
            this.tbInterval.TextChanged += new System.EventHandler(this.tbInterval_TextChanged);
            // 
            // tbTimeOut
            // 
            this.tbTimeOut.Location = new System.Drawing.Point(154, 79);
            this.tbTimeOut.Name = "tbTimeOut";
            this.tbTimeOut.Size = new System.Drawing.Size(62, 20);
            this.tbTimeOut.TabIndex = 5;
            this.tbTimeOut.TextChanged += new System.EventHandler(this.tbTimeOut_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Timeout request(ms)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Delay between requests(ms)";
            // 
            // btnChangeLogFolder
            // 
            this.btnChangeLogFolder.Location = new System.Drawing.Point(520, 8);
            this.btnChangeLogFolder.Name = "btnChangeLogFolder";
            this.btnChangeLogFolder.Size = new System.Drawing.Size(75, 22);
            this.btnChangeLogFolder.TabIndex = 2;
            this.btnChangeLogFolder.Text = "Change";
            this.btnChangeLogFolder.UseVisualStyleBackColor = true;
            this.btnChangeLogFolder.Click += new System.EventHandler(this.btnChangeLogFolder_Click);
            // 
            // tbLogFolderPath
            // 
            this.tbLogFolderPath.Location = new System.Drawing.Point(68, 9);
            this.tbLogFolderPath.Name = "tbLogFolderPath";
            this.tbLogFolderPath.Size = new System.Drawing.Size(446, 20);
            this.tbLogFolderPath.TabIndex = 1;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(8, 12);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(54, 13);
            this.label.TabIndex = 0;
            this.label.Text = "Log folder";
            // 
            // tabPagePrinterSettings
            // 
            this.tabPagePrinterSettings.AutoScroll = true;
            this.tabPagePrinterSettings.Controls.Add(this.tabPagePrintersPanel);
            this.tabPagePrinterSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPagePrinterSettings.Name = "tabPagePrinterSettings";
            this.tabPagePrinterSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePrinterSettings.Size = new System.Drawing.Size(603, 251);
            this.tabPagePrinterSettings.TabIndex = 1;
            this.tabPagePrinterSettings.Text = "Printer";
            this.tabPagePrinterSettings.UseVisualStyleBackColor = true;
            // 
            // tabPagePrintersPanel
            // 
            this.tabPagePrintersPanel.AutoScroll = true;
            this.tabPagePrintersPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tabPagePrintersPanel.Controls.Add(this.tabControlPrinterSettings);
            this.tabPagePrintersPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPagePrintersPanel.Location = new System.Drawing.Point(3, 3);
            this.tabPagePrintersPanel.Name = "tabPagePrintersPanel";
            this.tabPagePrintersPanel.Size = new System.Drawing.Size(597, 245);
            this.tabPagePrintersPanel.TabIndex = 1;
            // 
            // tabControlPrinterSettings
            // 
            this.tabControlPrinterSettings.Controls.Add(this.tabPagePrinterUserSettings);
            this.tabControlPrinterSettings.Controls.Add(this.tabPagePrinterDocumentSettings);
            this.tabControlPrinterSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPrinterSettings.HotTrack = true;
            this.tabControlPrinterSettings.Location = new System.Drawing.Point(0, 0);
            this.tabControlPrinterSettings.Name = "tabControlPrinterSettings";
            this.tabControlPrinterSettings.SelectedIndex = 0;
            this.tabControlPrinterSettings.Size = new System.Drawing.Size(597, 245);
            this.tabControlPrinterSettings.TabIndex = 0;
            // 
            // tabPagePrinterUserSettings
            // 
            this.tabPagePrinterUserSettings.Controls.Add(this.btnAddPairUserPrinter);
            this.tabPagePrinterUserSettings.Controls.Add(this.dataGridView);
            this.tabPagePrinterUserSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPagePrinterUserSettings.Name = "tabPagePrinterUserSettings";
            this.tabPagePrinterUserSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePrinterUserSettings.Size = new System.Drawing.Size(589, 219);
            this.tabPagePrinterUserSettings.TabIndex = 0;
            this.tabPagePrinterUserSettings.Text = "User/Printer";
            this.tabPagePrinterUserSettings.UseVisualStyleBackColor = true;
            // 
            // btnAddPairUserPrinter
            // 
            this.btnAddPairUserPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddPairUserPrinter.Image = ((System.Drawing.Image)(resources.GetObject("btnAddPairUserPrinter.Image")));
            this.btnAddPairUserPrinter.Location = new System.Drawing.Point(556, 3);
            this.btnAddPairUserPrinter.Name = "btnAddPairUserPrinter";
            this.btnAddPairUserPrinter.Size = new System.Drawing.Size(30, 31);
            this.btnAddPairUserPrinter.TabIndex = 3;
            this.btnAddPairUserPrinter.UseVisualStyleBackColor = true;
            this.btnAddPairUserPrinter.Click += new System.EventHandler(this.btnAddPairUserPrinter_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(3, 39);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(583, 174);
            this.dataGridView.TabIndex = 2;
            // 
            // tabPagePrinterDocumentSettings
            // 
            this.tabPagePrinterDocumentSettings.Controls.Add(this.groupBox2);
            this.tabPagePrinterDocumentSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPagePrinterDocumentSettings.Name = "tabPagePrinterDocumentSettings";
            this.tabPagePrinterDocumentSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePrinterDocumentSettings.Size = new System.Drawing.Size(589, 219);
            this.tabPagePrinterDocumentSettings.TabIndex = 1;
            this.tabPagePrinterDocumentSettings.Text = "Documents";
            this.tabPagePrinterDocumentSettings.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnTemplateInvoiceDeutschePost);
            this.groupBox2.Controls.Add(this.tbPathIncoiceTemplateDeutsche);
            this.groupBox2.Controls.Add(this.btnTemplateLabelDeutschePost);
            this.groupBox2.Controls.Add(this.tbPathLabelTemplateDeutsche);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(577, 90);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Deutsche post";
            // 
            // btnTemplateInvoiceDeutschePost
            // 
            this.btnTemplateInvoiceDeutschePost.Location = new System.Drawing.Point(496, 56);
            this.btnTemplateInvoiceDeutschePost.Name = "btnTemplateInvoiceDeutschePost";
            this.btnTemplateInvoiceDeutschePost.Size = new System.Drawing.Size(75, 22);
            this.btnTemplateInvoiceDeutschePost.TabIndex = 16;
            this.btnTemplateInvoiceDeutschePost.Text = "Change";
            this.btnTemplateInvoiceDeutschePost.UseVisualStyleBackColor = true;
            this.btnTemplateInvoiceDeutschePost.Click += new System.EventHandler(this.btnTemplateInvoiceDeutschePost_Click);
            // 
            // tbPathIncoiceTemplateDeutsche
            // 
            this.tbPathIncoiceTemplateDeutsche.Location = new System.Drawing.Point(107, 57);
            this.tbPathIncoiceTemplateDeutsche.Name = "tbPathIncoiceTemplateDeutsche";
            this.tbPathIncoiceTemplateDeutsche.Size = new System.Drawing.Size(383, 20);
            this.tbPathIncoiceTemplateDeutsche.TabIndex = 15;
            // 
            // btnTemplateLabelDeutschePost
            // 
            this.btnTemplateLabelDeutschePost.Location = new System.Drawing.Point(496, 17);
            this.btnTemplateLabelDeutschePost.Name = "btnTemplateLabelDeutschePost";
            this.btnTemplateLabelDeutschePost.Size = new System.Drawing.Size(75, 22);
            this.btnTemplateLabelDeutschePost.TabIndex = 14;
            this.btnTemplateLabelDeutschePost.Text = "Change";
            this.btnTemplateLabelDeutschePost.UseVisualStyleBackColor = true;
            this.btnTemplateLabelDeutschePost.Click += new System.EventHandler(this.btnTemplateLabelDeutschePost_Click);
            // 
            // tbPathLabelTemplateDeutsche
            // 
            this.tbPathLabelTemplateDeutsche.Location = new System.Drawing.Point(107, 18);
            this.tbPathLabelTemplateDeutsche.Name = "tbPathLabelTemplateDeutsche";
            this.tbPathLabelTemplateDeutsche.Size = new System.Drawing.Size(383, 20);
            this.tbPathLabelTemplateDeutsche.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Template invoice";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Template label";
            // 
            // tabPageAccounts
            // 
            this.tabPageAccounts.Controls.Add(this.groupBox1);
            this.tabPageAccounts.Location = new System.Drawing.Point(4, 22);
            this.tabPageAccounts.Name = "tabPageAccounts";
            this.tabPageAccounts.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAccounts.Size = new System.Drawing.Size(603, 251);
            this.tabPageAccounts.TabIndex = 2;
            this.tabPageAccounts.Text = "Login/password";
            this.tabPageAccounts.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbPassword);
            this.groupBox1.Controls.Add(this.tbEMail);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbKeyPhase);
            this.groupBox1.Controls.Add(this.tbPartnerId);
            this.groupBox1.Controls.Add(this.tbSchluesselDpwnMarktplatz);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(6, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(589, 169);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Deutsche post";
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(208, 138);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(375, 20);
            this.tbPassword.TabIndex = 15;
            this.tbPassword.TextChanged += new System.EventHandler(this.tbPassword_TextChanged);
            // 
            // tbEMail
            // 
            this.tbEMail.Location = new System.Drawing.Point(208, 108);
            this.tbEMail.Name = "tbEMail";
            this.tbEMail.Size = new System.Drawing.Size(375, 20);
            this.tbEMail.TabIndex = 14;
            this.tbEMail.TextChanged += new System.EventHandler(this.tbEMail_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Password";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "E-Mail";
            // 
            // tbKeyPhase
            // 
            this.tbKeyPhase.Location = new System.Drawing.Point(208, 78);
            this.tbKeyPhase.Name = "tbKeyPhase";
            this.tbKeyPhase.Size = new System.Drawing.Size(375, 20);
            this.tbKeyPhase.TabIndex = 11;
            this.tbKeyPhase.TextChanged += new System.EventHandler(this.tbKeyPhase_TextChanged);
            // 
            // tbPartnerId
            // 
            this.tbPartnerId.Location = new System.Drawing.Point(208, 18);
            this.tbPartnerId.Name = "tbPartnerId";
            this.tbPartnerId.Size = new System.Drawing.Size(375, 20);
            this.tbPartnerId.TabIndex = 9;
            this.tbPartnerId.TextChanged += new System.EventHandler(this.tbPartnerId_TextChanged);
            // 
            // tbSchluesselDpwnMarktplatz
            // 
            this.tbSchluesselDpwnMarktplatz.Location = new System.Drawing.Point(208, 48);
            this.tbSchluesselDpwnMarktplatz.Name = "tbSchluesselDpwnMarktplatz";
            this.tbSchluesselDpwnMarktplatz.Size = new System.Drawing.Size(375, 20);
            this.tbSchluesselDpwnMarktplatz.TabIndex = 10;
            this.tbSchluesselDpwnMarktplatz.TextChanged += new System.EventHandler(this.tbSchluesselDpwnMarktplatz_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "PARTNER_ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "SCHLUESSEL_DPWN_MARKTPLATZ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "KEY_PHASE";
            // 
            // tabPageData
            // 
            this.tabPageData.Controls.Add(this.textBox3);
            this.tabPageData.Controls.Add(this.textBox2);
            this.tabPageData.Controls.Add(this.label13);
            this.tabPageData.Controls.Add(this.label10);
            this.tabPageData.Controls.Add(this.tbDeutschePostURL);
            this.tabPageData.Controls.Add(this.label9);
            this.tabPageData.Location = new System.Drawing.Point(4, 22);
            this.tabPageData.Name = "tabPageData";
            this.tabPageData.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageData.Size = new System.Drawing.Size(603, 251);
            this.tabPageData.TabIndex = 3;
            this.tabPageData.Text = "Data";
            this.tabPageData.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(118, 69);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(477, 20);
            this.textBox3.TabIndex = 7;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(118, 39);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(477, 20);
            this.textBox2.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "DHL URL";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "UPS URL";
            // 
            // tbDeutschePostURL
            // 
            this.tbDeutschePostURL.Location = new System.Drawing.Point(118, 9);
            this.tbDeutschePostURL.Name = "tbDeutschePostURL";
            this.tbDeutschePostURL.Size = new System.Drawing.Size(477, 20);
            this.tbDeutschePostURL.TabIndex = 3;
            this.tbDeutschePostURL.TextChanged += new System.EventHandler(this.tbDeutschePostURL_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Deutsche post URL";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "docx files (*.docx)|*.docx";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 277);
            this.Controls.Add(this.tabControl);
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPageGeneral.ResumeLayout(false);
            this.tabPageGeneral.PerformLayout();
            this.tabPagePrinterSettings.ResumeLayout(false);
            this.tabPagePrintersPanel.ResumeLayout(false);
            this.tabControlPrinterSettings.ResumeLayout(false);
            this.tabPagePrinterUserSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.tabPagePrinterDocumentSettings.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPageAccounts.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPageData.ResumeLayout(false);
            this.tabPageData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageGeneral;
        private System.Windows.Forms.TabPage tabPageAccounts;
        private System.Windows.Forms.TabPage tabPagePrinterSettings;
        private System.Windows.Forms.Panel tabPagePrintersPanel;
        private System.Windows.Forms.Button btnChangeLogFolder;
        private System.Windows.Forms.TextBox tbLogFolderPath;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.TextBox tbEMail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbKeyPhase;
        private System.Windows.Forms.TextBox tbPartnerId;
        private System.Windows.Forms.TextBox tbSchluesselDpwnMarktplatz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbInterval;
        private System.Windows.Forms.TextBox tbTimeOut;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabControl tabControlPrinterSettings;
        private System.Windows.Forms.TabPage tabPagePrinterUserSettings;
        private System.Windows.Forms.TabPage tabPagePrinterDocumentSettings;
        private System.Windows.Forms.Button btnAddPairUserPrinter;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnTemplateInvoiceDeutschePost;
        private System.Windows.Forms.TextBox tbPathIncoiceTemplateDeutsche;
        private System.Windows.Forms.Button btnTemplateLabelDeutschePost;
        private System.Windows.Forms.TextBox tbPathLabelTemplateDeutsche;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.CheckBox cbLogging;
        private System.Windows.Forms.TabPage tabPageData;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbDeutschePostURL;
        private System.Windows.Forms.Label label9;
    }
}