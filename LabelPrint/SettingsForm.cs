﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace LabelPrint
{
    public partial class SettingsForm : Form
    {
        private Settings Settings;
        private bool _status;
        public SettingsForm(Settings settings, bool status)
        {
            InitializeComponent();
            Settings = settings;
            _status = status;
        }

        #region tabPagePrinters

        private void DataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var changeObject = (UserPrinter)dataGridView.Rows[e.RowIndex].DataBoundItem;
                if (changeObject != null)
                {
                    var changePrinter = new AddPrinter(changeObject);
                    changePrinter.ShowDialog();
                    dataGridView.Refresh();
                    dataGridView.RefreshEdit();
                }
            }
        }

        private void btnAddPairUserPrinter_Click(object sender, EventArgs e)
        {
            UserPrinter tmp = new UserPrinter();
            var addPrinter = new AddPrinter(tmp);
            addPrinter.ShowDialog();
            if (!string.IsNullOrWhiteSpace(tmp.User) && !string.IsNullOrWhiteSpace(tmp.Printer))
            {
                Settings.SettingsPrinter.UserPrinter.Add(tmp);
            }
        }

        private void btnTemplateLabelDeutschePost_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Settings.SettingsPrinter.PathTemplateLabelDeutschePost = openFileDialog.FileName;
                tbPathLabelTemplateDeutsche.Text = Settings.SettingsPrinter.PathTemplateLabelDeutschePost;
            }
        }

        private void btnTemplateInvoiceDeutschePost_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Settings.SettingsPrinter.PathTemplateInvoiceDeutschePost = openFileDialog.FileName;
                tbPathIncoiceTemplateDeutsche.Text = Settings.SettingsPrinter.PathTemplateInvoiceDeutschePost;
            }
        }

        #endregion

        #region tabPageGeneral

        private void btnChangeLogFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                tbLogFolderPath.Text = folderBrowserDialog.SelectedPath;
                Settings.GeneralSettings.LogFolderPath = folderBrowserDialog.SelectedPath;
            }
        }
        private void tbTimeOut_TextChanged(object sender, EventArgs e)
        {
            int tmp;
            if (Int32.TryParse(tbTimeOut.Text, out tmp))
            {
                Settings.GeneralSettings.TimeoutRequest = tmp;
            }
            else
            {
                tbTimeOut.BackColor = Color.Salmon;
            }
        }
        private void tbInterval_TextChanged(object sender, EventArgs e)
        {
            int tmp;
            if (Int32.TryParse(tbInterval.Text, out tmp))
            {
                Settings.GeneralSettings.TimerInterval = tmp;
            }
            else
            {
                tbInterval.BackColor = Color.Salmon;
            }
        }
        private void cbLogging_CheckedChanged(object sender, EventArgs e)
        {
            Settings.GeneralSettings.IsLogging = cbLogging.Checked;
        }

        #endregion

        #region tabPageAccounts

        private void tbSchluesselDpwnMarktplatz_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbSchluesselDpwnMarktplatz.Text))
            {
                tbSchluesselDpwnMarktplatz.ResetBackColor();
                Settings.SettingsAccount.DeutschePostAccount.SCHLUESSEL_DPWN_MARKTPLATZ = tbSchluesselDpwnMarktplatz.Text.Trim();
            }
            else
            {
                tbSchluesselDpwnMarktplatz.BackColor = Color.Salmon;
            }
        }
        private void tbPartnerId_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbPartnerId.Text))
            {
                tbPartnerId.ResetBackColor();
                Settings.SettingsAccount.DeutschePostAccount.PARTNER_ID = tbPartnerId.Text.Trim();
            }
            else
            {
                tbPartnerId.BackColor = Color.Salmon;
            }
        }
        private void tbKeyPhase_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbKeyPhase.Text))
            {
                tbKeyPhase.ResetBackColor();
                Settings.SettingsAccount.DeutschePostAccount.KEY_PHASE = tbKeyPhase.Text.Trim();
            }
            else
            {
                tbKeyPhase.BackColor = Color.Salmon;
            }
        }
        private void tbEMail_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbEMail.Text))
            {
                tbEMail.ResetBackColor();
                Settings.SettingsAccount.DeutschePostAccount.EMail = tbEMail.Text.Trim();
            }
            else
            {
                tbEMail.BackColor = Color.Salmon;
            }
        }
        private void tbPassword_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbPassword.Text))
            {
                tbPassword.ResetBackColor();
                Settings.SettingsAccount.DeutschePostAccount.Password = tbPassword.Text.Trim();
            }
            else
            {
                tbPassword.BackColor = Color.Salmon;
            }
        }

        #endregion


        private void SettingsForm_Load(object sender, EventArgs e)
        {
            dataGridView.DataSource = Settings.SettingsPrinter.UserPrinter;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView.CellDoubleClick += DataGridView_CellDoubleClick;
            tbPathLabelTemplateDeutsche.Text = Settings.SettingsPrinter.PathTemplateLabelDeutschePost;
            tbPathIncoiceTemplateDeutsche.Text = Settings.SettingsPrinter.PathTemplateInvoiceDeutschePost;

            tbLogFolderPath.Text = Settings.GeneralSettings.LogFolderPath;
            tbInterval.Text = Settings.GeneralSettings.TimerInterval.ToString();
            tbTimeOut.Text = Settings.GeneralSettings.TimeoutRequest.ToString();
            cbLogging.Checked = Settings.GeneralSettings.IsLogging;

            tbKeyPhase.Text = Settings.SettingsAccount.DeutschePostAccount.KEY_PHASE;
            tbPartnerId.Text = Settings.SettingsAccount.DeutschePostAccount.PARTNER_ID;
            tbSchluesselDpwnMarktplatz.Text = Settings.SettingsAccount.DeutschePostAccount.SCHLUESSEL_DPWN_MARKTPLATZ;
            tbEMail.Text = Settings.SettingsAccount.DeutschePostAccount.EMail;
            tbPassword.Text = Settings.SettingsAccount.DeutschePostAccount.Password;

            tbDeutschePostURL.Text = Settings.SettingsData.DeutschePostURL;

            tabPageGeneral.Enabled = !_status;
            tabPagePrinterSettings.Enabled = !_status;
            tabPageAccounts.Enabled = !_status;
            tabPageData.Enabled = !_status;
        }
        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Serialize();//ToDO проверка перед закрытием
        }

        private void tbDeutschePostURL_TextChanged(object sender, EventArgs e)
        {
            Settings.SettingsData.DeutschePostURL = tbDeutschePostURL.Text;
        }
    }
}
