﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Runtime;
using System.ServiceModel;
using System.Threading;
using LabelPrint.DeutschePostService;
using LabelPrint.Services;
using Timer = System.Timers.Timer;

namespace LabelPrint
{
    public partial class Form1 : Form
    {
        private bool status;
        private Settings Settings;
        private Timer timer;
        private HandlerDeutschePost _handlerDeutschePost;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Settings = Settings.Deserialize(); //toDo проверка
            status = false;

            
            timer = new Timer(Settings.GeneralSettings.TimerInterval);
            timer.Elapsed += Timer_Elapsed;

            cldDeliveryName.ItemCheck += CldDeliveryName_ItemCheck;
            clbWorkUsers.ItemCheck += ClbWorkUsers_ItemCheck;

            foreach (var user in Settings.SettingsPrinter.UserPrinter.Select(printer => printer.User))
            {
                clbWorkUsers.Items.Add(user);
            }

            for (int index = 0; index < cldDeliveryName.Items.Count; index++)
            {
                if (Settings.WorkingSettings.deliveryName.Contains(cldDeliveryName.Items[index]))
                {
                    cldDeliveryName.SetItemChecked(index, true);
                }
            }
            for (int index = 0; index < clbWorkUsers.Items.Count; index++)
            {
                if (Settings.WorkingSettings.userWork.Contains(clbWorkUsers.Items[index]))
                {
                    clbWorkUsers.SetItemChecked(index, true);
                }
            }

            _handlerDeutschePost = new HandlerDeutschePost(Settings);
        }


        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //Debug.WriteLine("tick");
            if (Settings.WorkingSettings.deliveryName.Contains("Deutsche post"))
            {
                Task.Run(() =>
                            {
                //Debug.WriteLine("new task");
                try
                                {
                                    WebRequest request = WebRequest.Create("https://mv3.colibo.de/sales/printer/deutsche-post-orders");
                                    request.Timeout = Settings.GeneralSettings.TimeoutRequest;
                                    WebResponse response = request.GetResponse();
                                    string tmp = string.Empty;
                                    using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                                    {
                                        tmp = streamReader.ReadLine();
                                        try
                                        {
                                            _handlerDeutschePost.Parse(tmp);
                                        }
                                        catch (Exception)
                                        {
                            //Log.WriteLine(tmp);
                            throw;
                                        }
                                    }
                                }
                                catch (WebException exception)
                                {

                                    if (exception.Status == WebExceptionStatus.Timeout)
                                    { //Log.WriteLine("WebException - Timeout(2.5s)");
                    }
                                    else
                                    {
                                        timer.Stop();
                        //Debug.WriteLine("stop");
                        //btnStart.Enabled = !state;
                        //btnStop.Enabled = state;

                        Thread.Sleep(60000);

                        //MessageBox.Show(
                        //    $"Please check:" +
                        //    $"{Environment.NewLine}1. Your connection to the Internet" +
                        //    $"{Environment.NewLine}2. Site availability",
                        //    "Failed request!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        //Log.WriteLine("Failed request!(connection to the Internet/site availability)");

                        //if (state)
                        // timer.Start();

                        //Debug.WriteLine("start");
                    }
                                }
                                catch (Exception exception)
                                {
                    //Log.WriteLine(exception.Message);
                }
                            });
            }
            if (Settings.WorkingSettings.deliveryName.Contains("DHL")) { }
            if (Settings.WorkingSettings.deliveryName.Contains("UPS")) { }
        }

        private void toolStripButtonSettings_Click(object sender, EventArgs e)
        {
            SettingsForm settingsForm = new SettingsForm(Settings, status);
            settingsForm.ShowDialog(this);
        }
        #region WorkingSettings

        private void ClbWorkUsers_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (!((CheckedListBox) sender).Text.Equals(string.Empty))
            {
                if (e.NewValue == CheckState.Checked)
                {
                    Settings.WorkingSettings.userWork.Add(((CheckedListBox) sender).Text);
                }
                else
                {
                    Settings.WorkingSettings.userWork.Remove(((CheckedListBox) sender).Text);
                }
                Settings.Serialize();
            }
        }

        private void CldDeliveryName_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (!((CheckedListBox)sender).Text.Equals(string.Empty))
            {
                if (e.NewValue == CheckState.Checked)
                {
                    Settings.WorkingSettings.deliveryName.Add(((CheckedListBox)sender).Text);
                }
                else
                {
                    Settings.WorkingSettings.deliveryName.Remove(((CheckedListBox)sender).Text);
                }
                Settings.Serialize();
            }
        }

        #endregion

        private void toolStripButtonStop_Click(object sender, EventArgs e)
        {
            cldDeliveryName.Enabled = status;
            clbWorkUsers.Enabled = status;
            status = false;
        }
        private void toolStripButtonStart_Click(object sender, EventArgs e)
        {
            cldDeliveryName.Enabled = status;
            clbWorkUsers.Enabled = status;
            status = true;
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            if (Settings.WorkingSettings.deliveryName.Contains("Deutsche post"))
            {
                Task.Run(() =>
                {
                    //Debug.WriteLine("new task");
                    try
                    {
                        WebRequest request = WebRequest.Create(Settings.SettingsData.DeutschePostURL);
                        request.Timeout = Settings.GeneralSettings.TimeoutRequest;
                        WebResponse response = request.GetResponse();
                        string tmp = string.Empty;
                        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                        {
                            tmp = streamReader.ReadLine();
                            try
                            {
                                _handlerDeutschePost.Parse(tmp);
                            }
                            catch (Exception)
                            {
                                //Log.WriteLine(tmp);
                                throw;
                            }
                        }
                    }
                    catch (WebException exception)
                    {

                        if (exception.Status == WebExceptionStatus.Timeout)
                        { 
                            //Log.WriteLine("WebException - Timeout(2.5s)");
                        }
                        else
                        {
                            timer.Stop();
                            //Debug.WriteLine("stop");
                            //btnStart.Enabled = !state;
                            //btnStop.Enabled = state;

                            Thread.Sleep(60000);

                            //MessageBox.Show(
                            //    $"Please check:" +
                            //    $"{Environment.NewLine}1. Your connection to the Internet" +
                            //    $"{Environment.NewLine}2. Site availability",
                            //    "Failed request!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                            //Log.WriteLine("Failed request!(connection to the Internet/site availability)");

                            //if (state)
                            // timer.Start();

                            //Debug.WriteLine("start");
                        }
                    }
                    catch (Exception exception)
                    {
                        //Log.WriteLine(exception.Message);
                    }
                });
            }
            if (Settings.WorkingSettings.deliveryName.Contains("DHL")) { }
            if (Settings.WorkingSettings.deliveryName.Contains("UPS")) { }
        }
    }
}
