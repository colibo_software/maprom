﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using DbEntity;
using DbEntity.json;
using DeutschePost;
using GemBox.Document;
using LabelPrint.DeutschePostService;
using LabelPrint.handlers;
using LabelPrint.Services;
using Newtonsoft.Json;
using NLog;
using TemplateEngine.Docx;

namespace LabelPrint
{
    public class HandlerDeutschePost //todo логика исключений и return
    {
        private MyContext _context;
        private Settings _settings;
        private Logger _logger;



        public void Job(Etikett etikett, bool isAgain)
        {
            if (etikett == null)
            {
                return;
            }
            _logger.Info($"in processing {etikett.order_id}({etikett.order_nr}) etikett");//todo перенести +
            if (!isAgain)
            {
                _context.Etiketten.Add(etikett);
                _context.SaveChanges();
            }

            bool isValidEtikettApi =
                ((ApiDP) (etikett.api)).IsValid();
                

            if (isValidEtikettApi)
            {
                CustomHeaderMessageInspector._partnerId = ((ApiDP)(etikett.api)).PARTNER_ID;
                CustomHeaderMessageInspector._keyPhase = ((ApiDP)(etikett.api)).KEY_PHASE;
                CustomHeaderMessageInspector._signatureKey = ((ApiDP)(etikett.api)).SCHLUESSEL_DPWN_MARKTPLATZ;
            }
            else
            {
                CustomHeaderMessageInspector._partnerId = _settings.SettingsAccount.DeutschePostAccount.PARTNER_ID;
                CustomHeaderMessageInspector._keyPhase = _settings.SettingsAccount.DeutschePostAccount.KEY_PHASE;
                CustomHeaderMessageInspector._signatureKey = _settings.SettingsAccount.DeutschePostAccount.SCHLUESSEL_DPWN_MARKTPLATZ;
            }




            _context.Etiketten.Load();
            var tt = _context.Etiketten.Local.ToList();
            Etikett tmpEtikett = tt.FirstOrDefault(e => e.Id == etikett.Id);
            ShoppingCartResponseType stamp = null;
            Image im = null;
            //todo try -> catch ?
            try
            {
                if (tmpEtikett.Analyzer.StateStamp == State.unprocessed)
                {
                    using (OneClickForAppPortTypeV3Client client = new OneClickForAppPortTypeV3Client())
                    {
                        AuthenticateUserResponseType authInfo = client.authenticateUser(
                            new AuthenticateUserRequestType()
                            {
                                password = _settings.IsSandbox ? _settings.SettingsAccount.DeutschePostAccount.SandboxDeutschePostAccount.Password : isValidEtikettApi ? ((ApiDP)(etikett.api)).Password : _settings.SettingsAccount.DeutschePostAccount.Password,
                                username = _settings.IsSandbox ? _settings.SettingsAccount.DeutschePostAccount.SandboxDeutschePostAccount.EMail : isValidEtikettApi ? ((ApiDP)(etikett.api)).Mail : _settings.SettingsAccount.DeutschePostAccount.EMail
                            });
                        //client.checkoutShoppingCartPDF(new ShoppingCartPDFRequestType()
                        //{
                        //    userToken = authInfo.userToken,
                        //    shopEtikettId =
                        //        client.createShopEtikettId(
                        //            new CreateShopEtikettIdRequest() { userToken = authInfo.userToken }).shopEtikettId,
                        //    total = 45,
                        //});
                        stamp = client
                            .checkoutShoppingCartPNG(new ShoppingCartPNGRequestType()
                            {
                                userToken = authInfo.userToken,
                                positions = new ShoppingCartPosition[1] { new ShoppingCartPosition()
                                {
                                    productCode = _settings.IsSandbox?
                                        _settings.SettingsAccount.DeutschePostAccount.SandboxDeutschePostAccount.ProductCode :_settings.SettingsAccount.DeutschePostAccount.ProductCode
                                } },
                                shopOrderId =
                                    client.createShopOrderId(
                                        new CreateShopOrderIdRequest() { userToken = authInfo.userToken }).shopOrderId,
                                total = _settings.IsSandbox ?
                                    _settings.SettingsAccount.DeutschePostAccount.SandboxDeutschePostAccount.PriceForCode : _settings.SettingsAccount.DeutschePostAccount.PriceForCode,

                            });
                    }
                    if (stamp != null)
                    {
                        tmpEtikett.Analyzer.StampLink = stamp.link;
                        tmpEtikett.Analyzer.ShipmentNumber = stamp.shoppingCart.voucherList[0].voucherId;
                        tmpEtikett.Analyzer.StateStamp = State.done;
                        _context.Set<Analyzer>().AddOrUpdate();
                        _context.SaveChanges();

                        _logger.Info($"{etikett.order_id} - label received");
                    }
                    else

                    {
                        _logger.Warn($"{etikett.order_nr} - label not received");
                        return;
                    }
                }
                if (tmpEtikett.Analyzer.StateFile == State.unprocessed)
                {
                    using (WebClient webClient = new WebClient())
                    {
                        Directory.CreateDirectory("temp files");
                        if (File.Exists("temp files\\{etikett.order_id}.zip"))
                        {
                            File.Delete("temp files\\{etikett.order_id}.zip");
                        }
                        webClient.DownloadFile(tmpEtikett.Analyzer.StampLink, $"temp files\\{etikett.order_id}.zip"); // todo config or settings //StampLink

                        tmpEtikett.Analyzer.StateFile = State.done;
                        tmpEtikett.Analyzer.PathFile = $"temp files\\{etikett.order_id}.zip";
                        _context.Set<Analyzer>().AddOrUpdate();
                        _context.SaveChanges();


                    }
                }
                if (tmpEtikett.Analyzer.StateDocument == State.unprocessed)
                {
                    using (Stream fileStream = new FileStream($"temp files\\{etikett.order_id}.zip", FileMode.Open))//210123550
                    {
                        //todo move to using +
                        using (ZipArchive zipArchive = new ZipArchive(fileStream, ZipArchiveMode.Read, false))
                        {
                            ZipArchiveEntry zipArchiveEntry = zipArchive.GetEntry(zipArchive.Entries[0].FullName);
                            im = Image.FromStream(zipArchiveEntry.Open());
                        }
                    }

                    Directory.CreateDirectory("testFolder");
                    if (File.Exists("testFolder\\{etikett.order_id}.docx"))
                    {
                        File.Delete($"testFolder\\{etikett.order_id}.docx");//todo delete
                    }
                    
                    File.Copy(_settings.SettingsPrinter.PathTemplateLabelDeutschePost, $"testFolder\\{etikett.order_id}.docx");

                    List<string> itemsSender = new List<string>() { "SenderItem1", "SenderItem2", "SenderItem3", "SenderItem4", "SenderItem5" };
                    List<string> items = new List<string>() { "Item1", "Item2", "Item3", "Item4", "Item5" };
                    List<IContentItem> contentItems = new List<IContentItem>();
                    int j = 0;

                    if (!string.IsNullOrWhiteSpace(etikett.sender.company))
                    {
                        contentItems.Add(new FieldContent(itemsSender[j++], etikett.sender.company));
                    }

                    if (!string.IsNullOrWhiteSpace(etikett.sender.additional))
                    {
                        contentItems.Add(new FieldContent(itemsSender[j++], etikett.sender.additional));
                    }

                    string full_name = String.Empty;
                    if (!string.IsNullOrWhiteSpace(etikett.sender.firstname))
                    {
                        full_name += etikett.sender.firstname + " ";
                    }
                    if (!string.IsNullOrWhiteSpace(etikett.sender.lastname))
                    {
                        full_name += etikett.sender.lastname;
                    }
                    if (!string.IsNullOrWhiteSpace(full_name)) //!
                    {
                        contentItems.Add(new FieldContent(itemsSender[j++], full_name));
                    }
                    //else
                    //{
                    //    contentItems.Add(new FieldContent(items[j++], full_name));
                    //    contentItems.Add(new FieldContent(items[j++], etikett.sender.name));
                    //}



                    if (!string.IsNullOrWhiteSpace(etikett.sender.street)/*&& !string.IsNullOrWhiteSpace(etikett.sender.street_nr)*/)
                    {
                        contentItems.Add(new FieldContent(itemsSender[j++], $"{etikett.sender.street} {etikett.sender.street_nr}"));
                    }



                    if (!string.IsNullOrWhiteSpace(etikett.sender.postal_code) && !string.IsNullOrWhiteSpace(etikett.sender.city))
                    {
                        contentItems.Add(new FieldContent(itemsSender[j++], $"{etikett.sender.postal_code} {etikett.sender.city}"));
                    }
                    for (; j < 5; j++)
                    {
                        contentItems.Add(new FieldContent(itemsSender[j], $""));
                    }



                    j = 0;

                    if (!string.IsNullOrWhiteSpace(etikett.delivery.company))
                    {
                        contentItems.Add(new FieldContent(items[j++], etikett.delivery.company));
                    }

                    if (!string.IsNullOrWhiteSpace(etikett.delivery.additional))
                    {
                        contentItems.Add(new FieldContent(items[j++], etikett.delivery.additional));
                    }

                    full_name = String.Empty;
                    if (!string.IsNullOrWhiteSpace(etikett.delivery.firstname))
                    {
                        full_name += etikett.delivery.firstname + " ";
                    }
                    if (!string.IsNullOrWhiteSpace(etikett.delivery.lastname))
                    {
                        full_name += etikett.delivery.lastname;
                    }
                    if (string.IsNullOrWhiteSpace(full_name))
                    {
                        if (!string.IsNullOrWhiteSpace(etikett.delivery.name))
                        {
                            contentItems.Add(new FieldContent(items[j++], etikett.delivery.name));
                        }
                    }
                    else
                    {
                        contentItems.Add(new FieldContent(items[j++], full_name));
                    }



                    if (!string.IsNullOrWhiteSpace(etikett.delivery.street)/*&& !string.IsNullOrWhiteSpace(etikett.delivery.street_nr)*/)
                    {
                        contentItems.Add(new FieldContent(items[j++], $"{etikett.delivery.street} {etikett.delivery.street_nr}"));
                    }



                    if (!string.IsNullOrWhiteSpace(etikett.delivery.postal_code) && !string.IsNullOrWhiteSpace(etikett.delivery.city))
                    {
                        contentItems.Add(new FieldContent(items[j++], $"{etikett.delivery.postal_code} {etikett.delivery.city}"));
                    }
                    for (; j < 5; j++)
                    {
                        contentItems.Add(new FieldContent(items[j], $""));
                    }

                    //new FieldContent("Referenznummer", $"{etikett.order_nr}"),
                    //new FieldContent("Gewicht", $"{etikett.weight:F} kg"),
                    //new ImageContent("image", ImageToByteArray(im)) // todo stream test
                    contentItems.Add(new FieldContent("Referenznummer", $"{etikett.order_nr}"));
                    contentItems.Add(new FieldContent("Gewicht", $"{(etikett.weight / 1000):F3} kg"));
                    contentItems.Add(new ImageContent("image", ImageToByteArray(im)));

                    var valuesToFill = new Content(
                        //new FieldContent("text1", "333-222-111"),

                        //new FieldContent("sender", $"{sender.ToString()}"),

                        contentItems.ToArray()



                    //new FieldContent("delivery", $"{delivery.ToString()}"),

                    //new FieldContent("name", $"{etikett.delivery.name}"),
                    //new FieldContent("street", $"{etikett.delivery.street} {etikett.delivery.street_nr}"),
                    //new FieldContent("town", $"{etikett.delivery.postal_code} {etikett.delivery.city}"),
                    //new FieldContent("Referenznummer", $"{etikett.order_nr}"),
                    //new FieldContent("Gewicht", $"{etikett.weight:F} kg"),
                    //new ImageContent("image", ImageToByteArray(im)) // todo stream test
                    );
                    using (var outputDocument = new TemplateProcessor($"testFolder\\{etikett.order_id}.docx")
                        .SetRemoveContentControls(true))
                    {
                        outputDocument.FillContent(valuesToFill);
                        outputDocument.SaveChanges();
                    }


                    var doc = DocumentModel.Load($"testFolder\\{etikett.order_id}.docx");//Microsoft Print to PDF
                    doc.Print(_settings.SettingsPrinter.PrinterNameDP);
                    _logger.Info($"{etikett.order_id} - sent to print");


                    tmpEtikett.Analyzer.StateDocument = State.done;
                    _context.Set<Analyzer>().AddOrUpdate();
                    _context.SaveChanges();
                }


                //_logger.Debug($"print doc {etikett.order_id}.docx"); //todo печать


                if (tmpEtikett.Analyzer.SetTrackingNr == State.unprocessed)
                {

                    ExportOrder exportOrder = new ExportOrder(tmpEtikett.Analyzer.ShipmentNumber, DateTime.Now, tmpEtikett.order_id);

                    XmlSerializer formatter = new XmlSerializer(typeof(ExportOrder));


                    using (FileStream fs = new FileStream($"{_settings.SettingsAccount.DeutschePostAccount.OutputSettings.FileHook.ExportFolderDP}/{tmpEtikett.order_id}.xml", FileMode.OpenOrCreate))//ToDo исправить
                    {
                        formatter.Serialize(fs, exportOrder);
                    }

                    tmpEtikett.Analyzer.SetTrackingNr = State.done;
                    _context.Set<Analyzer>().AddOrUpdate();
                    _context.SaveChanges();

                }

                if (tmpEtikett.Analyzer.SendMail == State.unprocessed)
                {
                    try
                    {
                        MailHandler.SendMail(tmpEtikett, _settings);
                        _logger.Info($"{etikett.order_id} - mail sent");
                        tmpEtikett.Analyzer.SendMail = State.done;

                        _context.Set<Analyzer>().AddOrUpdate();
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        _logger.Warn($"{etikett.order_id} - mail wasn't sent");
                        _logger.Fatal(e.Message);
                    }
                }

                if (tmpEtikett.Analyzer.StateFile == State.done && tmpEtikett.Analyzer.StateDocument == State.done &&
                    tmpEtikett.Analyzer.StateStamp == State.done && tmpEtikett.Analyzer.SetTrackingNr == State.done && tmpEtikett.Analyzer.SendMail == State.done)
                {

                    tmpEtikett.State = State.done;
                    _context.Set<Analyzer>().AddOrUpdate();
                    _context.SaveChanges();

                    _logger.Info($"{etikett.order_id} - done");

                }
                /*ZipArchive z;
                    Image.FromStream((z = new ZipArchive(new MemoryStream(new WebClient().DownloadData(client
                        .checkoutShoppingCartPNG(new ShoppingCartPNGRequestType()
                        {
                            userToken = authInfo.userToken,
                            //createManifest = true,
                            //createShippingList = ShippingList.Item0,
                            positions = new ShoppingCartPosition[1] {new ShoppingCartPosition() {productCode = 9861}},
                            //ppl = 22,
                            shopEtikettId =
                                client.createShopEtikettId(
                                    new CreateShopEtikettIdRequest() {userToken = authInfo.userToken}).shopEtikettId,
                            total = 45,

                        }).link)), ZipArchiveMode.Read, false)).GetEntry(z.Entries[0].FullName).Open());*/ //ToDo =)
            }
            catch (Exception e)
            {
                _logger.Fatal(e);
            }


        }
        private byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                //TODO check imageIn for null +
                if (imageIn != null)
                {
                    imageIn.Save(ms, imageIn.RawFormat);
                    return ms.ToArray();
                }
                return null;
            }
        }

        public HandlerDeutschePost(Settings settings, MyContext context, NLog.Logger logger)
        {
            _logger = logger;
            this._context = context;
            _settings = settings;
            //CustomHeaderMessageInspector._partnerId = this._settings.SettingsAccount.DeutschePostAccount.PARTNER_ID;
            //CustomHeaderMessageInspector._keyPhase = this._settings.SettingsAccount.DeutschePostAccount.KEY_PHASE;
            //CustomHeaderMessageInspector._signatureKey = this._settings.SettingsAccount.DeutschePostAccount.SCHLUESSEL_DPWN_MARKTPLATZ;
        }
    }
}
