﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using DbEntity;
using DbEntity.dp;
using DeutschePost;
using GemBox.Document;
using Ghostscript.NET.Rasterizer;
using LabelPrint.DHL_Service;
using NLog;
using TemplateEngine.Docx;
using Color = System.Drawing.Color;

namespace LabelPrint.handlers
{
    public class HandlerDHL
    {
        private MyContext _context;
        private Settings _settings;
        private Logger _logger;
        public HandlerDHL(Settings settings, MyContext context, NLog.Logger logger)
        {
            _logger = logger;
            this._context = context;
            _settings = settings;


        }
        public bool Job(Etikett etikett, bool isAgain)
        {
            if (etikett == null)
                return false;
            _logger.Info($"in processing {etikett.order_id}({etikett.order_nr}) etikett");//todo перенести +
            if (!isAgain)
            {
                _context.Etiketten.Add(etikett);
                _context.SaveChanges();
            } // todo зачем дважды звгружать ?
            _context.Etiketten.Load();
            var tt = _context.Etiketten.Local.ToList();
            Etikett tmpEtikett = tt.FirstOrDefault(e => e.Id == etikett.Id);
            CreateShipmentOrderResponse stamp = null;
            Image im = null;
            bool status = false;
            //todo try -> catch ?
            try
            {
                if (etikett.Analyzer.StateStamp == State.unprocessed)
                {
                    using (GKVAPIServicePortTypeClient client = new GKVAPIServicePortTypeClient())
                    {
                        AuthentificationType authentificationType = null;
                        if (((ApiDHL) (etikett.api)).IsValid())
                        {
                            client.ClientCredentials.UserName.UserName = ((ApiDHL)(etikett.api)).AppId;
                            client.ClientCredentials.UserName.Password = ((ApiDHL)(etikett.api)).Token;

                            authentificationType = new AuthentificationType()
                            {
                                user = ((ApiDHL)(etikett.api)).User,
                                signature = ((ApiDHL)(etikett.api)).Signature
                            };

                        }
                        else
                        {
                            client.ClientCredentials.UserName.UserName = _settings.SettingsAccount.DhlAccount.ApplicationID;
                            client.ClientCredentials.UserName.Password = _settings.SettingsAccount.DhlAccount.Token;

                            authentificationType = new AuthentificationType()
                            {
                                user = _settings.SettingsAccount.DhlAccount.User,
                                signature = _settings.SettingsAccount.DhlAccount.Signature
                            };
                        }


                        stamp = client.createShipmentOrder(
                authentificationType,
                CreateShipmentOrderRequest(etikett));
                    }
                    if (stamp != null && stamp.Status.statusCode.Equals("0"))
                    {
                        if (!string.Equals(stamp.Status.statusText, "ok", StringComparison.InvariantCultureIgnoreCase))
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendLine($"Status code - {stamp.Status.statusCode}");
                            sb.AppendLine($"Message: ");
                            stamp.Status.statusMessage.ToList().ForEach(s => sb.AppendLine($"\t{s}"));

                            //foreach (var s in stamp.Status.statusMessage)
                            //{
                            //    sb.AppendLine($"\t{s}");
                            //}
                            sb.AppendLine($"Text - {stamp.Status.statusText}");
                            _logger.Warn(sb.ToString);
                        }


                        etikett.Analyzer.StampLink = (string)stamp.CreationState[0].LabelData.Item;
                        etikett.Analyzer.ShipmentNumber = stamp.CreationState[0].LabelData.shipmentNumber;
                        etikett.Analyzer.StateStamp = State.done;
                        _context.Set<Analyzer>().AddOrUpdate();
                        _context.SaveChanges();

                        _logger.Info($"{etikett.order_id} - label received");
                    }
                    else
                    {
                        _logger.Fatal($"{etikett.order_id} - label not received");
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine($"Status code - {stamp.Status.statusCode}");
                        sb.AppendLine($"Message: ");

                        stamp.Status.statusMessage.ToList().ForEach(s => sb.AppendLine($"\t{s}"));

                        //foreach (var s in stamp.Status.statusMessage)
                        //{
                        //    sb.AppendLine($"\t{s}");
                        //}
                        sb.AppendLine($"Text - {stamp.Status.statusText}");
                        _logger.Warn(sb.ToString);

                        return false;
                    }
                }

                if (etikett.Analyzer.StateFile == State.unprocessed)
                    DownloadFile(etikett);

                if (etikett.Analyzer.StateDocument == State.unprocessed)
                    Doc(etikett, stamp);

                if (etikett.Analyzer.SetTrackingNr == State.unprocessed)
                    ExportFile(etikett);

                if (etikett.Analyzer.SendMail == State.unprocessed)
                {
                    try
                    {
                        MailHandler.SendMail(etikett, _settings);
                        _logger.Info($"{etikett.order_id} - mail sent");
                        etikett.Analyzer.SendMail = State.done;

                        _context.Set<Analyzer>().AddOrUpdate();
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        _logger.Warn($"{etikett.order_id} - mail wasn't sent");
                        _logger.Fatal(e.Message);
                    }
                }
                if (etikett.Analyzer.StateFile == State.done && etikett.Analyzer.StateDocument == State.done &&
                    etikett.Analyzer.StateStamp == State.done && etikett.Analyzer.SetTrackingNr == State.done && etikett.Analyzer.SendMail == State.done)
                {

                    etikett.State = State.done;
                    _context.Set<Analyzer>().AddOrUpdate();
                    _context.SaveChanges();
                    status = true;
                    _logger.Info($"{etikett.order_id} - done");

                }

            }
            catch (Exception e)
            {
                _logger.Fatal(e.Message);
            }
            return true;
        }

        private CreateShipmentOrderRequest CreateShipmentOrderRequest(Etikett etikett)
        {
            if (!decimal.TryParse(etikett.BankData.cashOnDeliveryPrice, out var cash))
            {
                cash = -1;
            }
            ServiceconfigurationCashOnDelivery _cashOnDelivery = null;
            BankType _bankData= null;
            if (cash>=0)
            {
                _bankData = new BankType()
                {
                    bic = etikett.BankData.bic,
                    iban = etikett.BankData.iban,
                    bankName = etikett.BankData.bankName,
                    accountOwner = etikett.BankData.accountOwner,
                    accountreference = etikett.BankData.accountReference,
                };
                _cashOnDelivery = new ServiceconfigurationCashOnDelivery()
                {
                    active = ServiceconfigurationCashOnDeliveryActive.Item1,
                    codAmount = cash
                };
            }

            return new CreateShipmentOrderRequest()
            {
                Version = new DHL_Service.Version()
                {
                    majorRelease = "2",
                    minorRelease = "0",
                }
                ,
                ShipmentOrder = new ShipmentOrderType[] {new ShipmentOrderType()
                    {
                        PrintOnlyIfCodeable = new Serviceconfiguration(){active = ServiceconfigurationActive.Item0},
                        //labelResponseTypeSpecified = true,
                        labelResponseType = ShipmentOrderTypeLabelResponseType.URL,
                        sequenceNumber = "1",
                        Shipment = new ShipmentOrderTypeShipment()
                        {
                            ShipmentDetails = new ShipmentDetailsTypeType()
                            {
                                product = (((ApiDHL)(etikett.api)).IsValid()) ?
                                    ((ApiDHL)(etikett.api)).Product: _settings.SettingsAccount.DhlAccount.Product,
                                accountNumber = (((ApiDHL)(etikett.api)).IsValid()) ?
                                    $"{((ApiDHL)(etikett.api)).AccNumber}{((ApiDHL)(etikett.api)).Code}":$"{_settings.SettingsAccount.DhlAccount.AccountNumber}{_settings.SettingsAccount.DhlAccount.Code}",
                                shipmentDate = etikett.Date.ToString("yyyy-MM-dd"),
                                ShipmentItem = new ShipmentItemTypeType()
                                {
                                    weightInKG = etikett.weight,
                                },
                                //BankData = _bankData,
                                //Service = new ShipmentService()
                                //{
                                //    CashOnDelivery = _cashOnDelivery,
                                //}
                            },
                            Shipper = new ShipperType()
                            {

                                Name = new NameType(){name1 = etikett.sender.company},
                                //todo адресс проверить
                                Address = new NativeAddressType()//Todo Packstation !!
                                {
                                    streetName = etikett.sender.street,
                                    streetNumber = etikett.sender.street_nr,
                                    zip = etikett.sender.postal_code,
                                    city = etikett.sender.city,
                                    Origin = new CountryType()
                                    {
                                        countryISOCode = etikett.sender.country
                                    }
                                },
                                Communication = new CommunicationType()
                                {
                                    phone = etikett.sender.phone,
                                    email = etikett.sender.email
                                }
                            },
                            Receiver = new ReceiverType(){
                                name1 = etikett.delivery.company,
                                Communication = new CommunicationType()
                                {
                                    phone = etikett.delivery.phone,
                                    email = etikett.delivery.email
                                },
                                Item = AddressOrPackstation(etikett),
                            }
                        }}
                }
            };
        }

        private void DownloadFile(Etikett etikett)
        {
            using (WebClient webClient = new WebClient())
            {
                Directory.CreateDirectory("temp files"); //ToDo перенести в настройки
                webClient.DownloadFile(etikett.Analyzer.StampLink,
                    $"temp files\\{etikett.order_id}.pdf"); // todo config or settings //StampLink

                etikett.Analyzer.StateFile = State.done;
                etikett.Analyzer.PathFile = $"temp files\\{etikett.order_id}.pdf";
                _context.Set<Analyzer>().AddOrUpdate();
                _context.SaveChanges();
            }
        }

        private void ExportFile(Etikett etikett)
        {
            ExportOrder exportOrder = new ExportOrder(etikett.Analyzer.ShipmentNumber, DateTime.Now, etikett.order_id);

            XmlSerializer formatter = new XmlSerializer(typeof(ExportOrder));


            using (FileStream fs = new FileStream($"{_settings.SettingsAccount.DhlAccount.OutputSettings.FileHook.ExportFolderDHL}/{etikett.order_id}.xml",
                FileMode.OpenOrCreate)) //ToDo исправить
            {
                formatter.Serialize(fs, exportOrder);
            }

            

            etikett.Analyzer.SetTrackingNr = State.done;
            _context.Set<Analyzer>().AddOrUpdate();
            _context.SaveChanges();
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }

        private void Doc(Etikett etikett, CreateShipmentOrderResponse stamp)
        {
            Image im;
            if (File.Exists($"testFolder\\{etikett.order_id}.docx"))
            {
                File.Delete($"testFolder\\{etikett.order_id}.docx");
            } //todo delete

            Directory.CreateDirectory("testFolder");
            File.Copy(_settings.SettingsPrinter.PathTemplateLabelDHL, $"testFolder\\{etikett.order_id}.docx"); //!!!!!!!!!!!!!!

            im = PdfToPng($"temp files\\{etikett.order_id}.pdf"); //todo 

            if (!string.Equals(stamp.Status.statusText, "ok"))
            {
                using (Graphics g = Graphics.FromImage(im))
                {
                    int y = 1000;
                    foreach (var s in stamp.Status.statusMessage)
                    {
                        g.DrawString($"{s}",
                            new Font(FontFamily.GenericMonospace, 40),
                            new HatchBrush(HatchStyle.Vertical, Color.Red),
                            new PointF(100, y: y));
                        y += 150;
                    }
                }

                //g.Dispose();
            }


            using (var outputDocument = new TemplateProcessor($"testFolder\\{etikett.order_id}.docx")
                .SetRemoveContentControls(true))
            {
                outputDocument.FillContent(new Content(new ImageContent("image", ImageToByteArray(im))));
                outputDocument.SaveChanges();
            }

            var doc = DocumentModel.Load($"testFolder\\{etikett.order_id}.docx"); //Microsoft Print to PDF
            doc.Print(_settings.SettingsPrinter.PrinterNameDHL);
            _logger.Info($"{etikett.order_id} - sent to print");

            etikett.Analyzer.StateDocument = State.done;
            _context.Set<Analyzer>().AddOrUpdate();
            _context.SaveChanges();
        }

        private static Image PdfToPng(string inputFile/*, string outputFileName*/)
        {
            var xDpi = 300; //set the x DPI 2500
            var yDpi = 300; //set the y DPI
            var pageNumber = 1; // the pages in a PDF document

            var lib = File.ReadAllBytes($"lib\\gsdll32.dll");

            using (var rasterizer = new GhostscriptRasterizer()) //create an instance for GhostscriptRasterizer
            {
                //rasterizer.Open(inputFile, new GhostscriptVersionInfo(new Version(1,1),"","",new GhostscriptLicense()),true); //opens the PDF file for rasterizing
                rasterizer.Open(inputFile, lib); //opens the PDF file for rasterizing

                //set the output image(png's) complete path
                //var outputPNGPath = Path.Combine("outputFolder\\", string.Format("{0}.jpeg", outputFileName));

                //converts the PDF pages to png's 
                var pdf2PNG = rasterizer.GetPage(xDpi, yDpi, pageNumber);

                //pdf2PNG = pdf2PNG.Crop(new Rectangle(280, 75, 1195, 2207));

                //save the png's
                //pdf2PNG.Save(outputPNGPath, ImageFormat.Png);

                return pdf2PNG;

            }
        }
        private byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                //TODO check imageIn for null +
                if (imageIn != null)
                {
                    imageIn.Save(ms, ImageFormat.Jpeg);
                    //imageIn.Save(ms, imageIn.RawFormat); //todo что за фигня
                    return ms.ToArray();
                }
                return null;
            }
        }

        private object AddressOrPackstation(Etikett etikett)
        {
            if (etikett.delivery.street.Contains("Packstation"))
            {
                return new PackStationType()
                {
                    packstationNumber = etikett.delivery.street_nr,
                    city = etikett.delivery.city,
                    zip = etikett.delivery.postal_code,
                    postNumber = Regex.Replace(etikett.delivery.additional, @"[^\d]+", ""), //todo string +
                    Origin = new CountryType()
                    {
                        //country = etikett.delivery.country,
                        countryISOCode = etikett.delivery.country
                    }
                };
            }
            else
            {
                return new ReceiverNativeAddressType()
                {
                    streetName = etikett.delivery.street,
                    streetNumber = etikett.delivery.street_nr,
                    zip = etikett.delivery.postal_code,
                    city = etikett.delivery.city,
                    Origin = new CountryType()
                    {
                        //country = etikett.delivery.country,
                        countryISOCode = etikett.delivery.country
                    }
                };
            }
        }
    }
}