﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using DbEntity;
using DeutschePost;

namespace LabelPrint.handlers
{
    public class MailHandler
    {
        public static void SendMail(Etikett etikett, Settings settings)
        {
            try
            {
                string mess = File.ReadAllText(settings.MailSettings.PathTemplateMail);

                foreach (var s in settings.MailSettings.Collection)
                {
                    if (s.Contains("\\"))
                    {
                        var words = s.Split('\\');

                        mess = mess.Replace($"@{s}@", typeof(Etikett).GetProperty(words[0]).PropertyType.GetProperty(words[1]).GetValue(typeof(Etikett).GetProperty(words[0]).GetValue(etikett)).ToString());
                    }
                    else
                    {
                        //mess = Regex.Replace(mess, $"\\w*@{VARIABLE}@\\w*",typeof(Etikett).GetProperty(VARIABLE).GetValue(etikett).ToString());
                        mess = mess.Replace($"@{s}@", typeof(Etikett).GetProperty(s).GetValue(etikett).ToString());
                    }

                }
                if (etikett.smtp.IsValid())
                {
                
                    Send(etikett.smtp.Host, etikett.smtp.Port, etikett.smtp.UserName, etikett.smtp.Password, etikett.smtp.From,
                        etikett.delivery.email, etikett.smtp.FromMail, mess);
                }
                else
                {
                    Send(settings.SettingsAccount.SmtpAccount.Host, settings.SettingsAccount.SmtpAccount.Port,
                        settings.SettingsAccount.SmtpAccount.UserName, settings.SettingsAccount.SmtpAccount.Password,
                        settings.SettingsAccount.SmtpAccount.From, etikett.delivery.email,
                        settings.SettingsAccount.SmtpAccount.FromMail, mess);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        static void Send(string host, int port, string userName, string password, string from, string recipients, string subject, string body)
        {
            var client = new SmtpClient(host, port)
            {
                Credentials = new NetworkCredential(userName, password),
                EnableSsl = true
            };
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                System.Security.Cryptography.X509Certificates.X509Chain chain,
                System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };
            client.Send(from, recipients, subject, body);
        }
    }
}