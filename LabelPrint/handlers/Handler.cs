﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DbEntity;
using DeutschePost;
using Newtonsoft.Json;
using NLog;

namespace LabelPrint
{
    public class Handler
    {
        //private Response _response;
        private MyContext _context;
        private Settings _settings;
        private Logger _logger;

        private HandlerDeutschePost _handlerDeutschePost;


        public Handler(Settings settings, MyContext context, NLog.Logger logger)
        {
            _logger = logger;
            _context = context;
            _settings = settings;

            _handlerDeutschePost = new HandlerDeutschePost(_settings, _context, _logger);


            _context.Etiketten.Load();
            _context.Analyzers.Load();
            _context.Deliveries.Load();
            _context.Senders.Load();
            _context.Invoices.Load(); //Todo загрузка
        }

        public void Parse(string json)
        {
            ////todo try -> catch ?
            //try
            //{
            //    _response = JsonConvert.DeserializeObject<Response>(json);



            //    //todo _response.data - > check for null +
            //    if (_response?.data != null)
            //    {
            //        int tmp;
            //        if ((tmp = _response.data.Count) != 0)
            //        {


            //            Job(_response.data, false);
            //        }

            //    }
            //    else
            //    {
            //        _logger.Fatal("response is null");
            //    }
            //}
            //catch (Exception e)
            //{
            //    _logger.Fatal(e.Message);
            //}
        }

        public void Job(List<Etikett> etiketts, bool isAgain)
        {
            //_handlerDeutschePost.Job(etiketts.Where(order => order.delivery_id == 1136).ToList(), isAgain);
        }
    }
}