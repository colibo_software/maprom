﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace LabelPrint
{
    public class MyBindingSource
    {
        private DataSet _dataSet;
        private BindingSource _bindingSource;
        private Dictionary<string,int> _tableNames;
        public MyBindingSource()
        {
            _dataSet = new DataSet();
            _bindingSource = new BindingSource();
            _tableNames = new Dictionary<string, int>();

            _bindingSource.DataSource = _dataSet;
        }
        public void AddTable<T>(string nameTable, List<T> list, List<string> unrecorded)
        {
            //if (list.Count == 0)
            //    throw new ArgumentOutOfRangeException();
            _tableNames.Add(nameTable,list.Count);

            DataTable dataTable = new DataTable(nameTable);

            Type type = typeof(T);

            PropertyInfo[] propertyInfos = type.GetProperties().Where(x => !unrecorded.Contains(x.Name)).ToArray();

            for (int i = 0; i < propertyInfos.Length; i++)
            {
                dataTable.Columns.Add(propertyInfos[i].Name, propertyInfos[i].PropertyType);
            }

            for (int i = 0; i < list.Count; i++)
            {
                object[] param = new object[propertyInfos.Length];
                for (int j = 0; j < propertyInfos.Length; j++)
                {
                    param[j] = type.GetProperty(propertyInfos[j].Name).GetValue(list[i]);
                }

                //typeof(MyClass).GetProperty(memberlist[i].Name).GetValue(_myContext.MyClasses.Local[i]),

                dataTable.Rows.Add(param);
            }

            _dataSet.Tables.Add(dataTable);
            SetDataMember(nameTable);
        }

        public void SetDataMember(string nameDataMember)
        {
            _bindingSource.RemoveFilter();
            _bindingSource.DataMember = nameDataMember;
        }
        public string GetCurrentDataMember() => _bindingSource.DataMember;
        public int GetCountCurrentDataMember() => _tableNames[_bindingSource.DataMember];
        public List<string> GetDataMembers() => _tableNames.Keys.ToList();
        public BindingSource GetBindingSource() => _bindingSource;

    }
}