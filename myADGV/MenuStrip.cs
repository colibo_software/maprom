﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace myADGV
{
    [DesignerCategory("")]
    internal class MenuStrip : ContextMenuStrip
    {
        private static Point _resizeStartPoint = new Point(1, 1);
        private Hashtable _textStrings = new Hashtable();
        private Point _resizeEndPoint = new Point(-1, -1);
        private bool _checkTextFilterChangedEnabled = true;
        private TreeNodeItemSelector[] _initialNodes = new TreeNodeItemSelector[0];
        private TreeNodeItemSelector[] _restoreNodes = new TreeNodeItemSelector[0];
        private bool _checkTextFilterRemoveNodesOnSearch = true;
        private MenuStrip.FilterType _activeFilterType;
        private MenuStrip.SortType _activeSortType;
        private TreeNodeItemSelector[] _startingNodes;
        private TreeNodeItemSelector[] _filterNodes;
        private string _sortString;
        private string _filterString;
        private bool _checkTextFilterSetByText;
        private IContainer components;
        private ToolStripMenuItem sortASCMenuItem;
        private ToolStripMenuItem sortDESCMenuItem;
        private ToolStripMenuItem cancelSortMenuItem;
        private ToolStripSeparator toolStripSeparator1MenuItem;
        private ToolStripSeparator toolStripSeparator2MenuItem;
        private ToolStripSeparator toolStripSeparator3MenuItem;
        private ToolStripMenuItem cancelFilterMenuItem;
        private ToolStripMenuItem customFilterLastFiltersListMenuItem;
        private ToolStripMenuItem customFilterMenuItem;
        private ToolStripMenuItem customFilterLastFilter1MenuItem;
        private ToolStripMenuItem customFilterLastFilter2MenuItem;
        private ToolStripMenuItem customFilterLastFilter3MenuItem;
        private ToolStripMenuItem customFilterLastFilter4MenuItem;
        private ToolStripMenuItem customFilterLastFilter5MenuItem;
        private System.Windows.Forms.TreeView checkList;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Button button_cancel;
        private ToolStripControlHost checkFilterListControlHost;
        private ToolStripControlHost checkFilterListButtonsControlHost;
        private ToolStripControlHost resizeBoxControlHost;
        private Panel checkFilterListPanel;
        private Panel checkFilterListButtonsPanel;
        private System.Windows.Forms.TextBox checkTextFilter;
        private ToolStripControlHost checkTextFilterControlHost;

        public MenuStrip(Type dataType)
        {
            this._textStrings.Add((object)"SORTDATETIMEASC", (object)"Sort Oldest to Newest");
            this._textStrings.Add((object)"SORTDATETIMEDESC", (object)"Sort Newest to Oldest");
            this._textStrings.Add((object)"SORTBOOLASC", (object)"Sort by False/True");
            this._textStrings.Add((object)"SORTBOOLDESC", (object)"Sort by True/False");
            this._textStrings.Add((object)"SORTNUMASC", (object)"Sort Smallest to Largest");
            this._textStrings.Add((object)"SORTNUMDESC", (object)"Sort Largest to Smallest");
            this._textStrings.Add((object)"SORTTEXTASC", (object)"Sort А to Z");
            this._textStrings.Add((object)"SORTTEXTDESC", (object)"Sort Z to A");
            this._textStrings.Add((object)"ADDCUSTOMFILTER", (object)"Add a Custom Filter");
            this._textStrings.Add((object)"CUSTOMFILTER", (object)"Custom Filter");
            this._textStrings.Add((object)"CLEARFILTER", (object)"Clear Filter");
            this._textStrings.Add((object)"CLEARSORT", (object)"Clear Sort");
            this._textStrings.Add((object)"BUTTONOK", (object)"Filter");
            this._textStrings.Add((object)"BUTTONCANCEL", (object)"Cancel");
            this._textStrings.Add((object)"NODESELECTALL", (object)"(Select All)");
            this._textStrings.Add((object)"NODESELECTEMPTY", (object)"(Blanks)");
            this._textStrings.Add((object)"FILTERCHECKLISTDISABLED", (object)"Filter list is disabled");
            this.InitializeComponent();
            this.DataType = dataType;
            if (this.DataType == typeof(DateTime) || this.DataType == typeof(TimeSpan))
            {
                this.customFilterLastFiltersListMenuItem.Text = this._textStrings[(object)"CUSTOMFILTER"].ToString();
                this.sortASCMenuItem.Text = this._textStrings[(object)"SORTDATETIMEASC"].ToString();
                this.sortDESCMenuItem.Text = this._textStrings[(object)"SORTDATETIMEDESC"].ToString();
                this.sortASCMenuItem.Image = (Image)Resources.MenuStrip_OrderASCnum;
                this.sortDESCMenuItem.Image = (Image)Resources.MenuStrip_OrderDESCnum;
            }
            else if (this.DataType == typeof(bool))
            {
                this.customFilterLastFiltersListMenuItem.Text = this._textStrings[(object)"CUSTOMFILTER"].ToString();
                this.sortASCMenuItem.Text = this._textStrings[(object)"SORTBOOLASC"].ToString();
                this.sortDESCMenuItem.Text = this._textStrings[(object)"SORTBOOLDESC"].ToString();
                this.sortASCMenuItem.Image = (Image)Resources.MenuStrip_OrderASCbool;
                this.sortDESCMenuItem.Image = (Image)Resources.MenuStrip_OrderDESCbool;
            }
            else if (this.DataType == typeof(int) || this.DataType == typeof(long) || (this.DataType == typeof(short) || this.DataType == typeof(uint)) || (this.DataType == typeof(ulong) || this.DataType == typeof(ushort) || (this.DataType == typeof(byte) || this.DataType == typeof(sbyte))) || (this.DataType == typeof(Decimal) || this.DataType == typeof(float) || this.DataType == typeof(double)))
            {
                this.customFilterLastFiltersListMenuItem.Text = this._textStrings[(object)"CUSTOMFILTER"].ToString();
                this.sortASCMenuItem.Text = this._textStrings[(object)"SORTNUMASC"].ToString();
                this.sortDESCMenuItem.Text = this._textStrings[(object)"SORTNUMDESC"].ToString();
                this.sortASCMenuItem.Image = (Image)Resources.MenuStrip_OrderASCnum;
                this.sortDESCMenuItem.Image = (Image)Resources.MenuStrip_OrderDESCnum;
            }
            else
            {
                this.customFilterLastFiltersListMenuItem.Text = this._textStrings[(object)"CUSTOMFILTER"].ToString();
                this.sortASCMenuItem.Text = this._textStrings[(object)"SORTTEXTASC"].ToString();
                this.sortDESCMenuItem.Text = this._textStrings[(object)"SORTTEXTDESC"].ToString();
                this.sortASCMenuItem.Image = (Image)Resources.MenuStrip_OrderASCtxt;
                this.sortDESCMenuItem.Image = (Image)Resources.MenuStrip_OrderDESCtxt;
            }
            if (this.DataType == typeof(DateTime) || this.DataType == typeof(TimeSpan) || this.DataType == typeof(bool))
                this.checkTextFilter.Enabled = false;
            this.IsFilterNOTINLogicEnabled = false;
            this.IsSortEnabled = true;
            this.IsFilterEnabled = true;
            this.IsFilterChecklistEnabled = true;
            this.IsFilterDateAndTimeEnabled = true;
            this.customFilterLastFiltersListMenuItem.Enabled = this.DataType != typeof(bool);
            this.customFilterLastFiltersListMenuItem.Checked = this.ActiveFilterType == MenuStrip.FilterType.Custom;
            this.MinimumSize = new Size(this.PreferredSize.Width, this.PreferredSize.Height);
            this.ResizeBox(this.MinimumSize.Width, this.MinimumSize.Height);
        }

        private void MenuStrip_Closed(object sender, EventArgs e)
        {
            this.ResizeClean();
            this._startingNodes = (TreeNodeItemSelector[])null;
            this._checkTextFilterChangedEnabled = false;
            this.checkTextFilter.Text = "";
            this._checkTextFilterChangedEnabled = true;
        }

        private void MenuStrip_LostFocus(object sender, EventArgs e)
        {
            if (this.ContainsFocus)
                return;
            this.Close();
        }

        private ImageList GetCheckListStateImages()
        {
            ImageList imageList = new ImageList();
            Bitmap bitmap1 = new Bitmap(16, 16);
            Bitmap bitmap2 = new Bitmap(16, 16);
            Bitmap bitmap3 = new Bitmap(16, 16);
            using (Bitmap bitmap4 = new Bitmap(16, 16))
            {
                using (Graphics g = Graphics.FromImage((Image)bitmap4))
                {
                    CheckBoxRenderer.DrawCheckBox(g, new Point(0, 1), CheckBoxState.UncheckedNormal);
                    bitmap1 = (Bitmap)bitmap4.Clone();
                    CheckBoxRenderer.DrawCheckBox(g, new Point(0, 1), CheckBoxState.CheckedNormal);
                    bitmap2 = (Bitmap)bitmap4.Clone();
                    CheckBoxRenderer.DrawCheckBox(g, new Point(0, 1), CheckBoxState.MixedNormal);
                    bitmap3 = (Bitmap)bitmap4.Clone();
                }
            }
            imageList.Images.Add("uncheck", (Image)bitmap1);
            imageList.Images.Add("check", (Image)bitmap2);
            imageList.Images.Add("mixed", (Image)bitmap3);
            return imageList;
        }

        public event EventHandler SortChanged;

        public event EventHandler FilterChanged;

        public MenuStrip.SortType ActiveSortType {
            get {
                return this._activeSortType;
            }
        }

        public MenuStrip.FilterType ActiveFilterType {
            get {
                return this._activeFilterType;
            }
        }

        public Type DataType { get; private set; }

        public bool IsSortEnabled { get; set; }

        public bool IsFilterEnabled { get; set; }

        public bool IsFilterChecklistEnabled { get; set; }

        public bool IsFilterDateAndTimeEnabled { get; set; }

        public bool IsFilterNOTINLogicEnabled { get; set; }

        public bool DoesTextFilterRemoveNodesOnSearch {
            get {
                return this._checkTextFilterRemoveNodesOnSearch;
            }
            set {
                this._checkTextFilterRemoveNodesOnSearch = value;
            }
        }

        public void SetSortEnabled(bool enabled)
        {
            if (!this.IsSortEnabled)
                enabled = false;
            this.cancelSortMenuItem.Enabled = enabled;
            this.sortASCMenuItem.Enabled = enabled;
            this.sortDESCMenuItem.Enabled = enabled;
        }

        public void SetFilterEnabled(bool enabled)
        {
            if (!this.IsFilterEnabled)
                enabled = false;
            this.cancelFilterMenuItem.Enabled = enabled;
            this.button_ok.Enabled = enabled;
            this.button_cancel.Enabled = enabled;
            this.checkList.Enabled = enabled;
            this.checkTextFilter.Enabled = enabled;
            if (enabled)
                this.customFilterLastFiltersListMenuItem.Enabled = this.DataType != typeof(bool);
            else
                this.customFilterLastFiltersListMenuItem.Enabled = false;
        }

        public void SetFilterChecklistEnabled(bool enabled)
        {
            if (!this.IsFilterEnabled)
                enabled = false;
            this.IsFilterChecklistEnabled = enabled;
            this.checkList.Enabled = enabled;
            this.checkTextFilter.ReadOnly = !enabled;
            if (this.IsFilterChecklistEnabled)
                return;
            this.checkList.BeginUpdate();
            this.checkList.Nodes.Clear();
            TreeNodeItemSelector node = TreeNodeItemSelector.CreateNode(this._textStrings[(object)"FILTERCHECKLISTDISABLED"].ToString() + "            ", (object)null, CheckState.Checked, TreeNodeItemSelector.CustomNodeType.SelectAll);
            node.NodeFont = new Font(this.checkList.Font, FontStyle.Bold);
            this.checkList.Nodes.Add((TreeNode)node);
        }

        public void SetLoadedMode(bool enabled)
        {
            this.customFilterMenuItem.Enabled = !enabled;
            this.cancelFilterMenuItem.Enabled = enabled;
            if (enabled)
            {
                this._activeFilterType = MenuStrip.FilterType.Loaded;
                this._sortString = (string)null;
                this._filterString = (string)null;
                this._filterNodes = (TreeNodeItemSelector[])null;
                this.customFilterLastFiltersListMenuItem.Checked = false;
                for (int index = 2; index < this.customFilterLastFiltersListMenuItem.DropDownItems.Count - 1; ++index)
                    (this.customFilterLastFiltersListMenuItem.DropDownItems[index] as ToolStripMenuItem).Checked = false;
                this.checkList.Nodes.Clear();
                TreeNodeItemSelector node = TreeNodeItemSelector.CreateNode("(Select All)            ", (object)null, CheckState.Checked, TreeNodeItemSelector.CustomNodeType.SelectAll);
                node.NodeFont = new Font(this.checkList.Font, FontStyle.Bold);
                node.CheckState = CheckState.Indeterminate;
                this.checkList.Nodes.Add((TreeNode)node);
                this.SetSortEnabled(false);
                this.SetFilterEnabled(false);
            }
            else
            {
                this._activeFilterType = MenuStrip.FilterType.None;
                this.SetSortEnabled(true);
                this.SetFilterEnabled(true);
            }
        }

        public void Show(Control control, int x, int y, IEnumerable<DataGridViewCell> vals)
        {
            this.BuildNodes(vals);
            if (this._checkTextFilterRemoveNodesOnSearch && this.checkList.Nodes.Count != ((IEnumerable<TreeNodeItemSelector>)this._initialNodes).Count<TreeNodeItemSelector>())
            {
                this._initialNodes = new TreeNodeItemSelector[this.checkList.Nodes.Count];
                this._restoreNodes = new TreeNodeItemSelector[this.checkList.Nodes.Count];
                int index = 0;
                foreach (TreeNodeItemSelector node in this.checkList.Nodes)
                {
                    this._initialNodes[index] = node.Clone();
                    this._restoreNodes[index] = node.Clone();
                    ++index;
                }
            }
            if (this._activeFilterType == MenuStrip.FilterType.Custom)
                this.SetNodesCheckState(this.checkList.Nodes, false);
            this.DuplicateNodes();
            this.Show(control, x, y);
            this._checkTextFilterChangedEnabled = false;
            this.checkTextFilter.Text = "";
            this._checkTextFilterChangedEnabled = true;
        }

        public void Show(Control control, int x, int y, bool _restoreFilter)
        {
            this._checkTextFilterChangedEnabled = false;
            this.checkTextFilter.Text = "";
            this._checkTextFilterChangedEnabled = true;
            if (_restoreFilter)
                this.RestoreFilterNodes();
            this.DuplicateNodes();
            this.Show(control, x, y);
            if (!this._checkTextFilterRemoveNodesOnSearch || !this._checkTextFilterSetByText)
                return;
            this._restoreNodes = new TreeNodeItemSelector[((IEnumerable<TreeNodeItemSelector>)this._initialNodes).Count<TreeNodeItemSelector>()];
            int index = 0;
            foreach (TreeNodeItemSelector initialNode in this._initialNodes)
            {
                this._restoreNodes[index] = initialNode.Clone();
                ++index;
            }
            this.checkList.BeginUpdate();
            this.checkList.Nodes.Clear();
            foreach (TreeNode initialNode in this._initialNodes)
                this.checkList.Nodes.Add(initialNode);
            this.checkList.EndUpdate();
        }

        public static IEnumerable<DataGridViewCell> GetValuesForFilter(DataGridView grid, string columnName)
        {
            return grid.Rows.Cast<DataGridViewRow>().Select<DataGridViewRow, DataGridViewCell>((Func<DataGridViewRow, DataGridViewCell>)(nulls => nulls.Cells[columnName]));
        }

        public void SortASC()
        {
            this.SortASCMenuItem_Click((object)this, (EventArgs)null);
        }

        public void SortDESC()
        {
            this.SortDESCMenuItem_Click((object)this, (EventArgs)null);
        }

        public string SortString {
            get {
                if (string.IsNullOrEmpty(this._sortString))
                    return "";
                return this._sortString;
            }
            private set {
                this.cancelSortMenuItem.Enabled = value != null && value.Length > 0;
                this._sortString = value;
            }
        }

        public void CleanSort()
        {
            string sortString = this.SortString;
            this.sortASCMenuItem.Checked = false;
            this.sortDESCMenuItem.Checked = false;
            this._activeSortType = MenuStrip.SortType.None;
            this.SortString = (string)null;
        }

        public string FilterString {
            get {
                if (string.IsNullOrEmpty(this._filterString))
                    return "";
                return this._filterString;
            }
            private set {
                this.cancelFilterMenuItem.Enabled = value != null && value.Length > 0;
                this._filterString = value;
            }
        }

        public void CleanFilter()
        {
            if (this._checkTextFilterRemoveNodesOnSearch)
            {
                this._initialNodes = new TreeNodeItemSelector[0];
                this._restoreNodes = new TreeNodeItemSelector[0];
                this._checkTextFilterSetByText = false;
            }
            for (int index = 2; index < this.customFilterLastFiltersListMenuItem.DropDownItems.Count - 1; ++index)
                (this.customFilterLastFiltersListMenuItem.DropDownItems[index] as ToolStripMenuItem).Checked = false;
            this._activeFilterType = MenuStrip.FilterType.None;
            this.SetNodesCheckState(this.checkList.Nodes, true);
            string filterString = this.FilterString;
            this.FilterString = (string)null;
            this._filterNodes = (TreeNodeItemSelector[])null;
            this.customFilterLastFiltersListMenuItem.Checked = false;
            this.button_ok.Enabled = true;
        }

        public void SetChecklistTextFilterRemoveNodesOnSearchMode(bool enabled)
        {
            if (this._checkTextFilterRemoveNodesOnSearch == enabled)
                return;
            this._checkTextFilterRemoveNodesOnSearch = enabled;
            this.CleanFilter();
        }

        private void SetCheckListFilter()
        {
            this.UnCheckCustomFilters();
            TreeNodeItemSelector selectAllNode = this.GetSelectAllNode();
            this.customFilterLastFiltersListMenuItem.Checked = false;
            if (selectAllNode != null && selectAllNode.Checked)
            {
                this.CancelFilterMenuItem_Click((object)null, new EventArgs());
            }
            else
            {
                string filterString = this.FilterString;
                this.FilterString = "";
                this._activeFilterType = MenuStrip.FilterType.CheckList;
                if (this.checkList.Nodes.Count > 1)
                {
                    TreeNodeItemSelector selectEmptyNode = this.GetSelectEmptyNode();
                    if (selectEmptyNode != null && selectEmptyNode.Checked)
                        this.FilterString = "[{0}] IS NULL";
                    if (this.checkList.Nodes.Count > 2 || selectEmptyNode == null)
                    {
                        string str1 = this.BuildNodesFilterString(!this.IsFilterNOTINLogicEnabled || !(this.DataType != typeof(DateTime)) || (!(this.DataType != typeof(TimeSpan)) || !(this.DataType != typeof(bool))) ? (IEnumerable<TreeNodeItemSelector>)ParallelEnumerable.Cast<TreeNodeItemSelector>(this.checkList.Nodes.AsParallel()).Where<TreeNodeItemSelector>((Func<TreeNodeItemSelector, bool>)(n =>
                        {
                            if (n.NodeType != TreeNodeItemSelector.CustomNodeType.SelectAll && n.NodeType != TreeNodeItemSelector.CustomNodeType.SelectEmpty)
                                return n.CheckState != CheckState.Unchecked;
                            return false;
                        })) : (IEnumerable<TreeNodeItemSelector>)ParallelEnumerable.Cast<TreeNodeItemSelector>(this.checkList.Nodes.AsParallel()).Where<TreeNodeItemSelector>((Func<TreeNodeItemSelector, bool>)(n =>
                        {
                            if (n.NodeType != TreeNodeItemSelector.CustomNodeType.SelectAll && n.NodeType != TreeNodeItemSelector.CustomNodeType.SelectEmpty)
                                return n.CheckState == CheckState.Unchecked;
                            return false;
                        })));
                        if (str1.Length > 0)
                        {
                            if (this.FilterString.Length > 0)
                                this.FilterString += " OR ";
                            if (this.DataType == typeof(DateTime) || this.DataType == typeof(TimeSpan))
                                this.FilterString += str1;
                            else if (this.DataType == typeof(bool))
                            {
                                MenuStrip menuStrip = this;
                                string str2 = menuStrip.FilterString + "{0}=" + str1;
                                menuStrip.FilterString = str2;
                            }
                            else if (this.DataType == typeof(int) || this.DataType == typeof(long) || (this.DataType == typeof(short) || this.DataType == typeof(uint)) || (this.DataType == typeof(ulong) || this.DataType == typeof(ushort) || (this.DataType == typeof(Decimal) || this.DataType == typeof(byte))) || (this.DataType == typeof(sbyte) || this.DataType == typeof(string)))
                            {
                                if (this.IsFilterNOTINLogicEnabled)
                                {
                                    MenuStrip menuStrip = this;
                                    string str2 = menuStrip.FilterString + "[{0}] NOT IN (" + str1 + ")";
                                    menuStrip.FilterString = str2;
                                }
                                else
                                {
                                    MenuStrip menuStrip = this;
                                    string str2 = menuStrip.FilterString + "[{0}] IN (" + str1 + ")";
                                    menuStrip.FilterString = str2;
                                }
                            }
                            else if (this.DataType == typeof(double))
                            {
                                if (this.IsFilterNOTINLogicEnabled)
                                {
                                    MenuStrip menuStrip = this;
                                    string str2 = menuStrip.FilterString + "Convert([{0}],System.String) NOT IN (" + str1 + ")";
                                    menuStrip.FilterString = str2;
                                }
                                else
                                {
                                    MenuStrip menuStrip = this;
                                    string str2 = menuStrip.FilterString + "Convert([{0}],System.String) IN (" + str1 + ")";
                                    menuStrip.FilterString = str2;
                                }
                            }
                            else if (!(this.DataType == typeof(Bitmap)))
                            {
                                if (this.IsFilterNOTINLogicEnabled)
                                {
                                    MenuStrip menuStrip = this;
                                    string str2 = menuStrip.FilterString + "Convert([{0}],System.String) NOT IN (" + str1 + ")";
                                    menuStrip.FilterString = str2;
                                }
                                else
                                {
                                    MenuStrip menuStrip = this;
                                    string str2 = menuStrip.FilterString + "Convert([{0}],System.String) IN (" + str1 + ")";
                                    menuStrip.FilterString = str2;
                                }
                            }
                        }
                    }
                }
                this.DuplicateFilterNodes();
                if (!(filterString != this.FilterString) || this.FilterChanged == null)
                    return;
                this.FilterChanged((object)this, new EventArgs());
            }
        }

        private string BuildNodesFilterString(IEnumerable<TreeNodeItemSelector> nodes)
        {
            StringBuilder stringBuilder = new StringBuilder("");
            string str1 = this.DataType == typeof(DateTime) || this.DataType == typeof(TimeSpan) ? " OR " : ", ";
            if (nodes != null && nodes.Count<TreeNodeItemSelector>() > 0)
            {
                if (this.DataType == typeof(DateTime))
                {
                    foreach (TreeNodeItemSelector node in nodes)
                    {
                        if (node.Checked && ParallelEnumerable.Cast<TreeNodeItemSelector>(node.Nodes.AsParallel()).Where<TreeNodeItemSelector>((Func<TreeNodeItemSelector, bool>)(sn => sn.CheckState != CheckState.Unchecked)).Count<TreeNodeItemSelector>() == 0)
                        {
                            DateTime dateTime = (DateTime)node.Value;
                            stringBuilder.Append("(Convert([{0}], 'System.String') LIKE '%" + Convert.ToString(this.IsFilterDateAndTimeEnabled ? dateTime : dateTime.Date, (IFormatProvider)CultureInfo.CurrentCulture) + "%')" + str1);
                        }
                        else if (node.CheckState != CheckState.Unchecked && node.Nodes.Count > 0)
                        {
                            string str2 = this.BuildNodesFilterString((IEnumerable<TreeNodeItemSelector>)ParallelEnumerable.Cast<TreeNodeItemSelector>(node.Nodes.AsParallel()).Where<TreeNodeItemSelector>((Func<TreeNodeItemSelector, bool>)(sn => sn.CheckState != CheckState.Unchecked)));
                            if (str2.Length > 0)
                                stringBuilder.Append(str2 + str1);
                        }
                    }
                }
                else if (this.DataType == typeof(TimeSpan))
                {
                    foreach (TreeNodeItemSelector node in nodes)
                    {
                        if (node.Checked && ParallelEnumerable.Cast<TreeNodeItemSelector>(node.Nodes.AsParallel()).Where<TreeNodeItemSelector>((Func<TreeNodeItemSelector, bool>)(sn => sn.CheckState != CheckState.Unchecked)).Count<TreeNodeItemSelector>() == 0)
                        {
                            TimeSpan timeSpan = (TimeSpan)node.Value;
                            stringBuilder.Append("(Convert([{0}], 'System.String') LIKE '%P" + (timeSpan.Days > 0 ? timeSpan.Days.ToString() + "D" : "") + (timeSpan.TotalHours > 0.0 ? "T" : "") + (timeSpan.Hours > 0 ? timeSpan.Hours.ToString() + "H" : "") + (timeSpan.Minutes > 0 ? timeSpan.Minutes.ToString() + "M" : "") + (timeSpan.Seconds > 0 ? timeSpan.Seconds.ToString() + "S" : "") + "%')" + str1);
                        }
                        else if (node.CheckState != CheckState.Unchecked && node.Nodes.Count > 0)
                        {
                            string str2 = this.BuildNodesFilterString((IEnumerable<TreeNodeItemSelector>)ParallelEnumerable.Cast<TreeNodeItemSelector>(node.Nodes.AsParallel()).Where<TreeNodeItemSelector>((Func<TreeNodeItemSelector, bool>)(sn => sn.CheckState != CheckState.Unchecked)));
                            if (str2.Length > 0)
                                stringBuilder.Append(str2 + str1);
                        }
                    }
                }
                else if (this.DataType == typeof(bool))
                {
                    using (IEnumerator<TreeNodeItemSelector> enumerator = nodes.GetEnumerator())
                    {
                        if (enumerator.MoveNext())
                        {
                            TreeNodeItemSelector current = enumerator.Current;
                            stringBuilder.Append(current.Value.ToString());
                        }
                    }
                }
                else if (this.DataType == typeof(int) || this.DataType == typeof(long) || (this.DataType == typeof(short) || this.DataType == typeof(uint)) || (this.DataType == typeof(ulong) || this.DataType == typeof(ushort) || (this.DataType == typeof(byte) || this.DataType == typeof(sbyte))))
                {
                    foreach (TreeNodeItemSelector node in nodes)
                        stringBuilder.Append(node.Value.ToString() + str1);
                }
                else if (this.DataType == typeof(float) || this.DataType == typeof(double) || this.DataType == typeof(Decimal))
                {
                    foreach (TreeNodeItemSelector node in nodes)
                        stringBuilder.Append(node.Value.ToString().Replace(",", ".") + str1);
                }
                else if (!(this.DataType == typeof(Bitmap)))
                {
                    foreach (TreeNodeItemSelector node in nodes)
                        stringBuilder.Append("'" + this.FormatFilterString(node.Value.ToString()) + "'" + str1);
                }
            }
            if (stringBuilder.Length > str1.Length && this.DataType != typeof(bool))
                stringBuilder.Remove(stringBuilder.Length - str1.Length, str1.Length);
            stringBuilder.Replace("0:00:00", "");
            return stringBuilder.ToString();
        }

        private string FormatFilterString(string text)
        {
            return text.Replace("'", "''");
        }

        private void BuildNodes(IEnumerable<DataGridViewCell> vals)
        {
            if (!this.IsFilterChecklistEnabled)
                return;
            this.checkList.BeginUpdate();
            this.checkList.Nodes.Clear();
            if (vals != null)
            {
                TreeNodeItemSelector node1 = TreeNodeItemSelector.CreateNode(this._textStrings[(object)"NODESELECTALL"].ToString() + "            ", (object)null, CheckState.Checked, TreeNodeItemSelector.CustomNodeType.SelectAll);
                node1.NodeFont = new Font(this.checkList.Font, FontStyle.Bold);
                this.checkList.Nodes.Add((TreeNode)node1);
                if (vals.Count<DataGridViewCell>() > 0)
                {
                    IEnumerable<DataGridViewCell> source1 = vals.Where<DataGridViewCell>((Func<DataGridViewCell, bool>)(c =>
                    {
                        if (c.Value != null)
                            return c.Value != DBNull.Value;
                        return false;
                    }));
                    if (vals.Count<DataGridViewCell>() != source1.Count<DataGridViewCell>())
                    {
                        TreeNodeItemSelector node2 = TreeNodeItemSelector.CreateNode(this._textStrings[(object)"NODESELECTEMPTY"].ToString() + "               ", (object)null, CheckState.Checked, TreeNodeItemSelector.CustomNodeType.SelectEmpty);
                        node2.NodeFont = new Font(this.checkList.Font, FontStyle.Bold);
                        this.checkList.Nodes.Add((TreeNode)node2);
                    }
                    if (this.DataType == typeof(DateTime))
                    {
                        foreach (IGrouping<int, DataGridViewCell> source2 in (IEnumerable<IGrouping<int, DataGridViewCell>>)source1.GroupBy<DataGridViewCell, int>((Func<DataGridViewCell, int>)(year => ((DateTime)year.Value).Year)).OrderBy<IGrouping<int, DataGridViewCell>, int>((Func<IGrouping<int, DataGridViewCell>, int>)(cy => cy.Key)))
                        {
                            TreeNodeItemSelector node2 = TreeNodeItemSelector.CreateNode(source2.Key.ToString(), (object)source2.Key, CheckState.Checked, TreeNodeItemSelector.CustomNodeType.DateTimeNode);
                            this.checkList.Nodes.Add((TreeNode)node2);
                            foreach (IGrouping<int, DataGridViewCell> source3 in (IEnumerable<IGrouping<int, DataGridViewCell>>)source2.GroupBy<DataGridViewCell, int>((Func<DataGridViewCell, int>)(month => ((DateTime)month.Value).Month)).OrderBy<IGrouping<int, DataGridViewCell>, int>((Func<IGrouping<int, DataGridViewCell>, int>)(cm => cm.Key)))
                            {
                                TreeNodeItemSelector childNode1 = node2.CreateChildNode(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(source3.Key), (object)source3.Key);
                                foreach (IGrouping<int, DataGridViewCell> source4 in (IEnumerable<IGrouping<int, DataGridViewCell>>)source3.GroupBy<DataGridViewCell, int>((Func<DataGridViewCell, int>)(day => ((DateTime)day.Value).Day)).OrderBy<IGrouping<int, DataGridViewCell>, int>((Func<IGrouping<int, DataGridViewCell>, int>)(cd => cd.Key)))
                                {
                                    if (!this.IsFilterDateAndTimeEnabled)
                                    {
                                        childNode1.CreateChildNode(source4.Key.ToString("D2"), source4.First<DataGridViewCell>().Value);
                                    }
                                    else
                                    {
                                        TreeNodeItemSelector childNode2 = childNode1.CreateChildNode(source4.Key.ToString("D2"), (object)source4.Key);
                                        foreach (IGrouping<int, DataGridViewCell> source5 in (IEnumerable<IGrouping<int, DataGridViewCell>>)source4.GroupBy<DataGridViewCell, int>((Func<DataGridViewCell, int>)(hour => ((DateTime)hour.Value).Hour)).OrderBy<IGrouping<int, DataGridViewCell>, int>((Func<IGrouping<int, DataGridViewCell>, int>)(ch => ch.Key)))
                                        {
                                            TreeNodeItemSelector childNode3 = childNode2.CreateChildNode(source5.Key.ToString("D2") + " h", (object)source5.Key);
                                            foreach (IGrouping<int, DataGridViewCell> source6 in (IEnumerable<IGrouping<int, DataGridViewCell>>)source5.GroupBy<DataGridViewCell, int>((Func<DataGridViewCell, int>)(min => ((DateTime)min.Value).Minute)).OrderBy<IGrouping<int, DataGridViewCell>, int>((Func<IGrouping<int, DataGridViewCell>, int>)(cmin => cmin.Key)))
                                            {
                                                TreeNodeItemSelector childNode4 = childNode3.CreateChildNode(source6.Key.ToString("D2") + " m", (object)source6.Key);
                                                foreach (IGrouping<int, DataGridViewCell> source7 in (IEnumerable<IGrouping<int, DataGridViewCell>>)source6.GroupBy<DataGridViewCell, int>((Func<DataGridViewCell, int>)(sec => ((DateTime)sec.Value).Second)).OrderBy<IGrouping<int, DataGridViewCell>, int>((Func<IGrouping<int, DataGridViewCell>, int>)(cs => cs.Key)))
                                                    childNode4.CreateChildNode(source7.Key.ToString("D2") + " s", source7.First<DataGridViewCell>().Value);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (this.DataType == typeof(TimeSpan))
                    {
                        foreach (IGrouping<int, DataGridViewCell> source2 in (IEnumerable<IGrouping<int, DataGridViewCell>>)source1.GroupBy<DataGridViewCell, int>((Func<DataGridViewCell, int>)(day => ((TimeSpan)day.Value).Days)).OrderBy<IGrouping<int, DataGridViewCell>, int>((Func<IGrouping<int, DataGridViewCell>, int>)(cd => cd.Key)))
                        {
                            TreeNodeItemSelector node2 = TreeNodeItemSelector.CreateNode(source2.Key.ToString("D2"), (object)source2.Key, CheckState.Checked, TreeNodeItemSelector.CustomNodeType.DateTimeNode);
                            this.checkList.Nodes.Add((TreeNode)node2);
                            foreach (IGrouping<int, DataGridViewCell> source3 in (IEnumerable<IGrouping<int, DataGridViewCell>>)source2.GroupBy<DataGridViewCell, int>((Func<DataGridViewCell, int>)(hour => ((TimeSpan)hour.Value).Hours)).OrderBy<IGrouping<int, DataGridViewCell>, int>((Func<IGrouping<int, DataGridViewCell>, int>)(ch => ch.Key)))
                            {
                                TreeNodeItemSelector childNode1 = node2.CreateChildNode(source3.Key.ToString("D2") + " h", (object)source3.Key);
                                foreach (IGrouping<int, DataGridViewCell> source4 in (IEnumerable<IGrouping<int, DataGridViewCell>>)source3.GroupBy<DataGridViewCell, int>((Func<DataGridViewCell, int>)(min => ((TimeSpan)min.Value).Minutes)).OrderBy<IGrouping<int, DataGridViewCell>, int>((Func<IGrouping<int, DataGridViewCell>, int>)(cmin => cmin.Key)))
                                {
                                    TreeNodeItemSelector childNode2 = childNode1.CreateChildNode(source4.Key.ToString("D2") + " m", (object)source4.Key);
                                    foreach (IGrouping<int, DataGridViewCell> source5 in (IEnumerable<IGrouping<int, DataGridViewCell>>)source4.GroupBy<DataGridViewCell, int>((Func<DataGridViewCell, int>)(sec => ((TimeSpan)sec.Value).Seconds)).OrderBy<IGrouping<int, DataGridViewCell>, int>((Func<IGrouping<int, DataGridViewCell>, int>)(cs => cs.Key)))
                                        childNode2.CreateChildNode(source5.Key.ToString("D2") + " s", source5.First<DataGridViewCell>().Value);
                                }
                            }
                        }
                    }
                    else if (this.DataType == typeof(bool))
                    {
                        IEnumerable<DataGridViewCell> source2 = source1.Where<DataGridViewCell>((Func<DataGridViewCell, bool>)(c => (bool)c.Value));
                        if (source2.Count<DataGridViewCell>() != source1.Count<DataGridViewCell>())
                            this.checkList.Nodes.Add((TreeNode)TreeNodeItemSelector.CreateNode("False", (object)false, CheckState.Checked, TreeNodeItemSelector.CustomNodeType.Default));
                        if (source2.Count<DataGridViewCell>() > 0)
                            this.checkList.Nodes.Add((TreeNode)TreeNodeItemSelector.CreateNode("True", (object)true, CheckState.Checked, TreeNodeItemSelector.CustomNodeType.Default));
                    }
                    else if (!(this.DataType == typeof(Bitmap)))
                    {
                        foreach (IGrouping<object, DataGridViewCell> source2 in (IEnumerable<IGrouping<object, DataGridViewCell>>)source1.GroupBy<DataGridViewCell, object>((Func<DataGridViewCell, object>)(c => c.Value)).OrderBy<IGrouping<object, DataGridViewCell>, object>((Func<IGrouping<object, DataGridViewCell>, object>)(g => g.Key)))
                            this.checkList.Nodes.Add((TreeNode)TreeNodeItemSelector.CreateNode(source2.First<DataGridViewCell>().FormattedValue.ToString(), source2.Key, CheckState.Checked, TreeNodeItemSelector.CustomNodeType.Default));
                    }
                }
            }
            this.checkList.EndUpdate();
        }

        private void NodeCheckChange(TreeNodeItemSelector node)
        {
            node.CheckState = node.CheckState != CheckState.Checked ? CheckState.Checked : CheckState.Unchecked;
            if (node.NodeType == TreeNodeItemSelector.CustomNodeType.SelectAll)
            {
                this.SetNodesCheckState(this.checkList.Nodes, node.Checked);
                this.button_ok.Enabled = node.Checked;
            }
            else
            {
                if (node.Nodes.Count > 0)
                    this.SetNodesCheckState(node.Nodes, node.Checked);
                CheckState checkState = this.UpdateNodesCheckState(this.checkList.Nodes);
                this.GetSelectAllNode().CheckState = checkState;
                this.button_ok.Enabled = checkState != CheckState.Unchecked;
            }
        }

        private void SetNodesCheckState(TreeNodeCollection nodes, bool isChecked)
        {
            foreach (TreeNodeItemSelector node in nodes)
            {
                node.Checked = isChecked;
                if (node.Nodes != null && node.Nodes.Count > 0)
                    this.SetNodesCheckState(node.Nodes, isChecked);
            }
        }

        private CheckState UpdateNodesCheckState(TreeNodeCollection nodes)
        {
            CheckState checkState = CheckState.Unchecked;
            bool flag1 = true;
            bool flag2 = true;
            foreach (TreeNodeItemSelector node in nodes)
            {
                if (node.NodeType != TreeNodeItemSelector.CustomNodeType.SelectAll)
                {
                    if (node.Nodes.Count > 0)
                        node.CheckState = this.UpdateNodesCheckState(node.Nodes);
                    if (flag1)
                    {
                        checkState = node.CheckState;
                        flag1 = false;
                    }
                    else if (checkState != node.CheckState)
                        flag2 = false;
                }
            }
            if (flag2)
                return checkState;
            return CheckState.Indeterminate;
        }

        private TreeNodeItemSelector GetSelectAllNode()
        {
            TreeNodeItemSelector nodeItemSelector = (TreeNodeItemSelector)null;
            int num = 0;
            foreach (TreeNodeItemSelector node in this.checkList.Nodes)
            {
                if (node.NodeType == TreeNodeItemSelector.CustomNodeType.SelectAll)
                {
                    nodeItemSelector = node;
                    break;
                }
                if (num <= 2)
                    ++num;
                else
                    break;
            }
            return nodeItemSelector;
        }

        private TreeNodeItemSelector GetSelectEmptyNode()
        {
            TreeNodeItemSelector nodeItemSelector = (TreeNodeItemSelector)null;
            int num = 0;
            foreach (TreeNodeItemSelector node in this.checkList.Nodes)
            {
                if (node.NodeType == TreeNodeItemSelector.CustomNodeType.SelectEmpty)
                {
                    nodeItemSelector = node;
                    break;
                }
                if (num <= 2)
                    ++num;
                else
                    break;
            }
            return nodeItemSelector;
        }

        private void DuplicateNodes()
        {
            this._startingNodes = new TreeNodeItemSelector[this.checkList.Nodes.Count];
            int index = 0;
            foreach (TreeNodeItemSelector node in this.checkList.Nodes)
            {
                this._startingNodes[index] = node.Clone();
                ++index;
            }
        }

        private void DuplicateFilterNodes()
        {
            this._filterNodes = new TreeNodeItemSelector[this.checkList.Nodes.Count];
            int index = 0;
            foreach (TreeNodeItemSelector node in this.checkList.Nodes)
            {
                this._filterNodes[index] = node.Clone();
                ++index;
            }
        }

        private void RestoreNodes()
        {
            this.checkList.Nodes.Clear();
            if (this._startingNodes == null)
                return;
            this.checkList.Nodes.AddRange((TreeNode[])this._startingNodes);
        }

        private void RestoreFilterNodes()
        {
            this.checkList.Nodes.Clear();
            if (this._filterNodes == null)
                return;
            this.checkList.Nodes.AddRange((TreeNode[])this._filterNodes);
        }

        private void CheckList_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewHitTestInfo treeViewHitTestInfo = this.checkList.HitTest(e.X, e.Y);
            if (treeViewHitTestInfo == null || treeViewHitTestInfo.Location != TreeViewHitTestLocations.StateImage)
                return;
            this.NodeCheckChange(e.Node as TreeNodeItemSelector);
            //this.Button_ok_Click((object)this, new EventArgs());
        }

        private void CheckList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Space)
                return;
            this.NodeCheckChange(this.checkList.SelectedNode as TreeNodeItemSelector);
        }

        private void CheckList_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNodeItemSelector node = e.Node as TreeNodeItemSelector;
            this.SetNodesCheckState(this.checkList.Nodes, false);
            node.CheckState = CheckState.Unchecked;
            this.NodeCheckChange(node);
            this.Button_ok_Click((object)this, new EventArgs());
        }

        private void CheckList_MouseEnter(object sender, EventArgs e)
        {
            this.checkList.Focus();
        }

        private void CheckList_MouseLeave(object sender, EventArgs e)
        {
            this.Focus();
        }

        private void Button_ok_Click(object sender, EventArgs e)
        {
            this.SetCheckListFilter();
            this.Close();
        }

        private void Button_cancel_Click(object sender, EventArgs e)
        {
            bool flag = false;
            if (this._checkTextFilterRemoveNodesOnSearch && this._checkTextFilterSetByText)
            {
                this._initialNodes = new TreeNodeItemSelector[((IEnumerable<TreeNodeItemSelector>)this._restoreNodes).Count<TreeNodeItemSelector>()];
                int index = 0;
                foreach (TreeNodeItemSelector restoreNode in this._restoreNodes)
                {
                    this._initialNodes[index] = restoreNode.Clone();
                    ++index;
                }
                flag = true;
                this.checkList.BeginUpdate();
                this.checkList.Nodes.Clear();
                foreach (TreeNode restoreNode in this._restoreNodes)
                    this.checkList.Nodes.Add(restoreNode);
                this.checkList.EndUpdate();
            }
            if (!flag)
                this.RestoreNodes();
            this.Close();
        }

        private void UnCheckCustomFilters()
        {
            for (int index = 2; index < this.customFilterLastFiltersListMenuItem.DropDownItems.Count; ++index)
                (this.customFilterLastFiltersListMenuItem.DropDownItems[index] as ToolStripMenuItem).Checked = false;
        }

        private void SetCustomFilter(int filtersMenuItemIndex)
        {
            if (this._activeFilterType == MenuStrip.FilterType.CheckList)
                this.SetNodesCheckState(this.checkList.Nodes, false);
            string str = this.customFilterLastFiltersListMenuItem.DropDownItems[filtersMenuItemIndex].Tag.ToString();
            string text = this.customFilterLastFiltersListMenuItem.DropDownItems[filtersMenuItemIndex].Text;
            if (filtersMenuItemIndex != 2)
            {
                for (int index = filtersMenuItemIndex; index > 2; --index)
                {
                    this.customFilterLastFiltersListMenuItem.DropDownItems[index].Text = this.customFilterLastFiltersListMenuItem.DropDownItems[index - 1].Text;
                    this.customFilterLastFiltersListMenuItem.DropDownItems[index].Tag = this.customFilterLastFiltersListMenuItem.DropDownItems[index - 1].Tag;
                }
                this.customFilterLastFiltersListMenuItem.DropDownItems[2].Text = text;
                this.customFilterLastFiltersListMenuItem.DropDownItems[2].Tag = (object)str;
            }
            for (int index = 3; index < this.customFilterLastFiltersListMenuItem.DropDownItems.Count; ++index)
                (this.customFilterLastFiltersListMenuItem.DropDownItems[index] as ToolStripMenuItem).Checked = false;
            (this.customFilterLastFiltersListMenuItem.DropDownItems[2] as ToolStripMenuItem).Checked = true;
            this._activeFilterType = MenuStrip.FilterType.Custom;
            string filterString = this.FilterString;
            this.FilterString = str;
            this.SetNodesCheckState(this.checkList.Nodes, false);
            this.DuplicateFilterNodes();
            this.customFilterLastFiltersListMenuItem.Checked = true;
            this.button_ok.Enabled = false;
            if (!(filterString != this.FilterString) || this.FilterChanged == null)
                return;
            this.FilterChanged((object)this, new EventArgs());
        }

        private void CancelFilterMenuItem_Click(object sender, EventArgs e)
        {
            string filterString = this.FilterString;
            this.CleanFilter();
            if (!(filterString != this.FilterString) || this.FilterChanged == null)
                return;
            this.FilterChanged((object)this, new EventArgs());
        }

        private void CancelFilterMenuItem_MouseEnter(object sender, EventArgs e)
        {
            if (!(sender as ToolStripMenuItem).Enabled)
                return;
            (sender as ToolStripMenuItem).Select();
        }

        private void CustomFilterMenuItem_Click(object sender, EventArgs e)
        {
            if (this.DataType == typeof(Bitmap))
                return;
            FormCustomFilter formCustomFilter = new FormCustomFilter(this.DataType, this.IsFilterDateAndTimeEnabled);
            if (formCustomFilter.ShowDialog() != DialogResult.OK)
                return;
            string filterString = formCustomFilter.FilterString;
            string stringDescription = formCustomFilter.FilterStringDescription;
            int filtersMenuItemIndex = -1;
            for (int index = 2; index < this.customFilterLastFiltersListMenuItem.DropDownItems.Count && this.customFilterLastFiltersListMenuItem.DropDown.Items[index].Available; ++index)
            {
                if (this.customFilterLastFiltersListMenuItem.DropDownItems[index].Text == stringDescription && this.customFilterLastFiltersListMenuItem.DropDownItems[index].Tag.ToString() == filterString)
                {
                    filtersMenuItemIndex = index;
                    break;
                }
            }
            if (filtersMenuItemIndex < 2)
            {
                for (int index = this.customFilterLastFiltersListMenuItem.DropDownItems.Count - 2; index > 1; --index)
                {
                    if (this.customFilterLastFiltersListMenuItem.DropDownItems[index].Available)
                    {
                        this.customFilterLastFiltersListMenuItem.DropDownItems[index + 1].Text = this.customFilterLastFiltersListMenuItem.DropDownItems[index].Text;
                        this.customFilterLastFiltersListMenuItem.DropDownItems[index + 1].Tag = this.customFilterLastFiltersListMenuItem.DropDownItems[index].Tag;
                    }
                }
                filtersMenuItemIndex = 2;
                this.customFilterLastFiltersListMenuItem.DropDownItems[2].Text = stringDescription;
                this.customFilterLastFiltersListMenuItem.DropDownItems[2].Tag = (object)filterString;
            }
            this.SetCustomFilter(filtersMenuItemIndex);
        }

        private void CustomFilterLastFiltersListMenuItem_MouseEnter(object sender, EventArgs e)
        {
            if (!(sender as ToolStripMenuItem).Enabled)
                return;
            (sender as ToolStripMenuItem).Select();
        }

        private void CustomFilterLastFiltersListMenuItem_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(this.customFilterLastFiltersListMenuItem.Width - 12, 7, 10, 10);
            ControlPaint.DrawMenuGlyph(e.Graphics, rectangle, MenuGlyph.Arrow, Color.Black, Color.Transparent);
        }

        private void CustomFilterLastFilter1MenuItem_VisibleChanged(object sender, EventArgs e)
        {
            this.toolStripSeparator2MenuItem.Visible = !this.customFilterLastFilter1MenuItem.Visible;
            (sender as ToolStripMenuItem).VisibleChanged -= new EventHandler(this.CustomFilterLastFilter1MenuItem_VisibleChanged);
        }

        private void CustomFilterLastFilterMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem toolStripMenuItem = sender as ToolStripMenuItem;
            for (int filtersMenuItemIndex = 2; filtersMenuItemIndex < this.customFilterLastFiltersListMenuItem.DropDownItems.Count; ++filtersMenuItemIndex)
            {
                if (this.customFilterLastFiltersListMenuItem.DropDownItems[filtersMenuItemIndex].Text == toolStripMenuItem.Text && this.customFilterLastFiltersListMenuItem.DropDownItems[filtersMenuItemIndex].Tag.ToString() == toolStripMenuItem.Tag.ToString())
                {
                    this.SetCustomFilter(filtersMenuItemIndex);
                    break;
                }
            }
        }

        private void CustomFilterLastFilterMenuItem_TextChanged(object sender, EventArgs e)
        {
            (sender as ToolStripMenuItem).Available = true;
            (sender as ToolStripMenuItem).TextChanged -= new EventHandler(this.CustomFilterLastFilterMenuItem_TextChanged);
        }

        private void CheckTextFilter_TextChanged(object sender, EventArgs e)
        {
            if (!this._checkTextFilterChangedEnabled)
                return;
            this._checkTextFilterSetByText = !string.IsNullOrEmpty(this.checkTextFilter.Text);
            if (this._checkTextFilterRemoveNodesOnSearch)
            {
                this._startingNodes = this._initialNodes;
                this.checkList.BeginUpdate();
                this.RestoreNodes();
            }
            TreeNodeItemSelector node1 = TreeNodeItemSelector.CreateNode(this._textStrings[(object)"NODESELECTALL"].ToString() + "            ", (object)null, CheckState.Checked, TreeNodeItemSelector.CustomNodeType.SelectAll);
            TreeNodeItemSelector node2 = TreeNodeItemSelector.CreateNode(this._textStrings[(object)"NODESELECTEMPTY"].ToString() + "               ", (object)null, CheckState.Checked, TreeNodeItemSelector.CustomNodeType.SelectEmpty);
            for (int index = this.checkList.Nodes.Count - 1; index >= 0; --index)
            {
                TreeNodeItemSelector node3 = this.checkList.Nodes[index] as TreeNodeItemSelector;
                if (node3.Text == node1.Text)
                    node3.CheckState = CheckState.Indeterminate;
                else if (node3.Text == node2.Text)
                {
                    node3.CheckState = CheckState.Unchecked;
                }
                else
                {
                    node3.Checked = !node3.Text.ToLower().Contains(this.checkTextFilter.Text.ToLower());
                    this.NodeCheckChange(node3);
                }
            }
            if (!this._checkTextFilterRemoveNodesOnSearch)
                return;
            foreach (TreeNodeItemSelector initialNode in this._initialNodes)
                initialNode.CheckState = !(initialNode.Text == node1.Text) ? (!(initialNode.Text == node2.Text) ? (!initialNode.Text.ToLower().Contains(this.checkTextFilter.Text.ToLower()) ? CheckState.Unchecked : CheckState.Checked) : CheckState.Unchecked) : CheckState.Indeterminate;
            this.checkList.EndUpdate();
            for (int index = this.checkList.Nodes.Count - 1; index >= 0; --index)
            {
                TreeNodeItemSelector node3 = this.checkList.Nodes[index] as TreeNodeItemSelector;
                if (!(node3.Text == node1.Text) && !(node3.Text == node2.Text) && !node3.Text.ToLower().Contains(this.checkTextFilter.Text.ToLower()))
                    node3.Remove();
            }
        }

        private void SortASCMenuItem_Click(object sender, EventArgs e)
        {
            if (this.DataType == typeof(Bitmap))
                return;
            this.sortASCMenuItem.Checked = true;
            this.sortDESCMenuItem.Checked = false;
            this._activeSortType = MenuStrip.SortType.ASC;
            string sortString = this.SortString;
            this.SortString = "[{0}] ASC";
            if (!(sortString != this.SortString) || this.SortChanged == null)
                return;
            this.SortChanged((object)this, new EventArgs());
        }

        private void SortASCMenuItem_MouseEnter(object sender, EventArgs e)
        {
            if (!(sender as ToolStripMenuItem).Enabled)
                return;
            (sender as ToolStripMenuItem).Select();
        }

        private void SortDESCMenuItem_Click(object sender, EventArgs e)
        {
            if (this.DataType == typeof(Bitmap))
                return;
            this.sortASCMenuItem.Checked = false;
            this.sortDESCMenuItem.Checked = true;
            this._activeSortType = MenuStrip.SortType.DESC;
            string sortString = this.SortString;
            this.SortString = "[{0}] DESC";
            if (!(sortString != this.SortString) || this.SortChanged == null)
                return;
            this.SortChanged((object)this, new EventArgs());
        }

        private void SortDESCMenuItem_MouseEnter(object sender, EventArgs e)
        {
            if (!(sender as ToolStripMenuItem).Enabled)
                return;
            (sender as ToolStripMenuItem).Select();
        }

        private void CancelSortMenuItem_Click(object sender, EventArgs e)
        {
            string sortString = this.SortString;
            this.CleanSort();
            if (!(sortString != this.SortString) || this.SortChanged == null)
                return;
            this.SortChanged((object)this, new EventArgs());
        }

        private void CancelSortMenuItem_MouseEnter(object sender, EventArgs e)
        {
            if (!(sender as ToolStripMenuItem).Enabled)
                return;
            (sender as ToolStripMenuItem).Select();
        }

        private void ResizeBox(int w, int h)
        {
            this.sortASCMenuItem.Width = w - 1;
            this.sortDESCMenuItem.Width = w - 1;
            this.cancelSortMenuItem.Width = w - 1;
            this.cancelFilterMenuItem.Width = w - 1;
            this.customFilterMenuItem.Width = w - 1;
            this.customFilterLastFiltersListMenuItem.Width = w - 1;
            this.checkFilterListControlHost.Size = new Size(w - 35, h - 160 - 25);
            this.checkFilterListPanel.Size = new Size(w - 35, h - 160 - 25);
            this.checkTextFilterControlHost.Width = w - 35;
            this.checkList.Bounds = new Rectangle(4, 4, w - 35 - 8, h - 160 - 25 - 8);
            this.checkFilterListButtonsControlHost.Size = new Size(w - 35, 24);
            this.button_ok.Location = new Point(w - 35 - 164, 0);
            this.button_cancel.Location = new Point(w - 35 - 79, 0);
            this.resizeBoxControlHost.Margin = new Padding(w - 46, 0, 0, 0);
            this.Size = new Size(w, h);
        }

        private void ResizeClean()
        {
            if (this._resizeEndPoint.X == -1)
                return;
            Point screen = this.PointToScreen(MenuStrip._resizeStartPoint);
            ControlPaint.DrawReversibleFrame(new Rectangle(screen.X, screen.Y, this._resizeEndPoint.X, this._resizeEndPoint.Y)
            {
                X = Math.Min(screen.X, this._resizeEndPoint.X),
                Width = Math.Abs(screen.X - this._resizeEndPoint.X),
                Y = Math.Min(screen.Y, this._resizeEndPoint.Y),
                Height = Math.Abs(screen.Y - this._resizeEndPoint.Y)
            }, Color.Black, FrameStyle.Dashed);
            this._resizeEndPoint.X = -1;
        }

        private void ResizeBoxControlHost_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;
            this.ResizeClean();
        }

        private void ResizeBoxControlHost_MouseMove(object sender, MouseEventArgs e)
        {
            if (!this.Visible || e.Button != MouseButtons.Left)
                return;
            int x1 = e.X;
            int y1 = e.Y;
            this.ResizeClean();
            int val1_1 = x1 + (this.Width - this.resizeBoxControlHost.Width);
            int val1_2 = y1 + (this.Height - this.resizeBoxControlHost.Height);
            int x2 = Math.Max(val1_1, this.MinimumSize.Width - 1);
            int y2 = Math.Max(val1_2, this.MinimumSize.Height - 1);
            Point screen1 = this.PointToScreen(MenuStrip._resizeStartPoint);
            Point screen2 = this.PointToScreen(new Point(x2, y2));
            ControlPaint.DrawReversibleFrame(new Rectangle()
            {
                X = Math.Min(screen1.X, screen2.X),
                Width = Math.Abs(screen1.X - screen2.X),
                Y = Math.Min(screen1.Y, screen2.Y),
                Height = Math.Abs(screen1.Y - screen2.Y)
            }, Color.Black, FrameStyle.Dashed);
            this._resizeEndPoint.X = screen2.X;
            this._resizeEndPoint.Y = screen2.Y;
        }

        private void ResizeBoxControlHost_MouseUp(object sender, MouseEventArgs e)
        {
            if (this._resizeEndPoint.X == -1)
                return;
            this.ResizeClean();
            if (!this.Visible || e.Button != MouseButtons.Left)
                return;
            this.ResizeBox(Math.Max(e.X + this.Width - this.resizeBoxControlHost.Width, this.MinimumSize.Width), Math.Max(e.Y + this.Height - this.resizeBoxControlHost.Height, this.MinimumSize.Height));
        }

        private void ResizeBoxControlHost_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage((Image)Resources.MenuStrip_ResizeGrip, 0, 0);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = (IContainer)new Container();
            this.sortASCMenuItem = new ToolStripMenuItem();
            this.sortDESCMenuItem = new ToolStripMenuItem();
            this.cancelSortMenuItem = new ToolStripMenuItem();
            this.toolStripSeparator1MenuItem = new ToolStripSeparator();
            this.cancelFilterMenuItem = new ToolStripMenuItem();
            this.customFilterLastFiltersListMenuItem = new ToolStripMenuItem();
            this.customFilterMenuItem = new ToolStripMenuItem();
            this.toolStripSeparator2MenuItem = new ToolStripSeparator();
            this.customFilterLastFilter1MenuItem = new ToolStripMenuItem();
            this.customFilterLastFilter2MenuItem = new ToolStripMenuItem();
            this.customFilterLastFilter3MenuItem = new ToolStripMenuItem();
            this.customFilterLastFilter4MenuItem = new ToolStripMenuItem();
            this.customFilterLastFilter5MenuItem = new ToolStripMenuItem();
            this.toolStripSeparator3MenuItem = new ToolStripSeparator();
            this.checkList = new System.Windows.Forms.TreeView();
            this.button_ok = new System.Windows.Forms.Button();
            this.button_cancel = new System.Windows.Forms.Button();
            this.checkFilterListPanel = new Panel();
            this.checkFilterListButtonsPanel = new Panel();
            this.checkFilterListButtonsControlHost = new ToolStripControlHost((Control)this.checkFilterListButtonsPanel);
            this.checkFilterListControlHost = new ToolStripControlHost((Control)this.checkFilterListPanel);
            this.checkTextFilter = new System.Windows.Forms.TextBox();
            this.checkTextFilterControlHost = new ToolStripControlHost((Control)this.checkTextFilter);
            this.resizeBoxControlHost = new ToolStripControlHost(new Control());
            this.SuspendLayout();
            this.BackColor = SystemColors.ControlLightLight;
            this.AutoSize = false;
            this.Padding = new Padding(0);
            this.Margin = new Padding(0);
            this.Size = new Size(287, 370);
            this.Closed += new ToolStripDropDownClosedEventHandler(this.MenuStrip_Closed);
            this.LostFocus += new EventHandler(this.MenuStrip_LostFocus);
            this.Items.AddRange(new ToolStripItem[11]
            {
        (ToolStripItem) this.sortASCMenuItem,
        (ToolStripItem) this.sortDESCMenuItem,
        (ToolStripItem) this.cancelSortMenuItem,
        (ToolStripItem) this.toolStripSeparator1MenuItem,
        (ToolStripItem) this.cancelFilterMenuItem,
        (ToolStripItem) this.customFilterLastFiltersListMenuItem,
        (ToolStripItem) this.toolStripSeparator3MenuItem,
        (ToolStripItem) this.checkTextFilterControlHost,
        (ToolStripItem) this.checkFilterListControlHost,
        (ToolStripItem) this.checkFilterListButtonsControlHost,
        (ToolStripItem) this.resizeBoxControlHost
            });
            this.sortASCMenuItem.Name = "sortASCMenuItem";
            this.sortASCMenuItem.AutoSize = false;
            this.sortASCMenuItem.Size = new Size(this.Width - 1, 22);
            this.sortASCMenuItem.Click += new EventHandler(this.SortASCMenuItem_Click);
            this.sortASCMenuItem.MouseEnter += new EventHandler(this.SortASCMenuItem_MouseEnter);
            this.sortASCMenuItem.ImageScaling = ToolStripItemImageScaling.None;
            this.sortDESCMenuItem.Name = "sortDESCMenuItem";
            this.sortDESCMenuItem.AutoSize = false;
            this.sortDESCMenuItem.Size = new Size(this.Width - 1, 22);
            this.sortDESCMenuItem.Click += new EventHandler(this.SortDESCMenuItem_Click);
            this.sortDESCMenuItem.MouseEnter += new EventHandler(this.SortDESCMenuItem_MouseEnter);
            this.sortDESCMenuItem.ImageScaling = ToolStripItemImageScaling.None;
            this.cancelSortMenuItem.Name = "cancelSortMenuItem";
            this.cancelSortMenuItem.Enabled = false;
            this.cancelSortMenuItem.AutoSize = false;
            this.cancelSortMenuItem.Size = new Size(this.Width - 1, 22);
            this.cancelSortMenuItem.Text = this._textStrings[(object)"CLEARSORT"].ToString();
            this.cancelSortMenuItem.Click += new EventHandler(this.CancelSortMenuItem_Click);
            this.cancelSortMenuItem.MouseEnter += new EventHandler(this.CancelSortMenuItem_MouseEnter);
            this.toolStripSeparator1MenuItem.Name = "toolStripSeparator1MenuItem";
            this.toolStripSeparator1MenuItem.Size = new Size(this.Width - 4, 6);
            this.cancelFilterMenuItem.Name = "cancelFilterMenuItem";
            this.cancelFilterMenuItem.Enabled = false;
            this.cancelFilterMenuItem.AutoSize = false;
            this.cancelFilterMenuItem.Size = new Size(this.Width - 1, 22);
            this.cancelFilterMenuItem.Text = this._textStrings[(object)"CLEARFILTER"].ToString();
            this.cancelFilterMenuItem.Click += new EventHandler(this.CancelFilterMenuItem_Click);
            this.cancelFilterMenuItem.MouseEnter += new EventHandler(this.CancelFilterMenuItem_MouseEnter);
            this.toolStripSeparator2MenuItem.Name = "toolStripSeparator2MenuItem";
            this.toolStripSeparator2MenuItem.Size = new Size(149, 6);
            this.toolStripSeparator2MenuItem.Visible = false;
            this.customFilterMenuItem.Name = "customFilterMenuItem";
            this.customFilterMenuItem.Size = new Size(152, 22);
            this.customFilterMenuItem.Text = this._textStrings[(object)"ADDCUSTOMFILTER"].ToString();
            this.customFilterMenuItem.Click += new EventHandler(this.CustomFilterMenuItem_Click);
            this.customFilterLastFilter1MenuItem.Name = "customFilterLastFilter1MenuItem";
            this.customFilterLastFilter1MenuItem.Size = new Size(152, 22);
            this.customFilterLastFilter1MenuItem.Tag = (object)"0";
            this.customFilterLastFilter1MenuItem.Text = (string)null;
            this.customFilterLastFilter1MenuItem.Visible = false;
            this.customFilterLastFilter1MenuItem.VisibleChanged += new EventHandler(this.CustomFilterLastFilter1MenuItem_VisibleChanged);
            this.customFilterLastFilter1MenuItem.Click += new EventHandler(this.CustomFilterLastFilterMenuItem_Click);
            this.customFilterLastFilter1MenuItem.TextChanged += new EventHandler(this.CustomFilterLastFilterMenuItem_TextChanged);
            this.customFilterLastFilter2MenuItem.Name = "customFilterLastFilter2MenuItem";
            this.customFilterLastFilter2MenuItem.Size = new Size(152, 22);
            this.customFilterLastFilter2MenuItem.Tag = (object)"1";
            this.customFilterLastFilter2MenuItem.Text = (string)null;
            this.customFilterLastFilter2MenuItem.Visible = false;
            this.customFilterLastFilter2MenuItem.Click += new EventHandler(this.CustomFilterLastFilterMenuItem_Click);
            this.customFilterLastFilter2MenuItem.TextChanged += new EventHandler(this.CustomFilterLastFilterMenuItem_TextChanged);
            this.customFilterLastFilter3MenuItem.Name = "customFilterLastFilter3MenuItem";
            this.customFilterLastFilter3MenuItem.Size = new Size(152, 22);
            this.customFilterLastFilter3MenuItem.Tag = (object)"2";
            this.customFilterLastFilter3MenuItem.Text = (string)null;
            this.customFilterLastFilter3MenuItem.Visible = false;
            this.customFilterLastFilter3MenuItem.Click += new EventHandler(this.CustomFilterLastFilterMenuItem_Click);
            this.customFilterLastFilter3MenuItem.TextChanged += new EventHandler(this.CustomFilterLastFilterMenuItem_TextChanged);
            this.customFilterLastFilter4MenuItem.Name = "lastfilter4MenuItem";
            this.customFilterLastFilter4MenuItem.Size = new Size(152, 22);
            this.customFilterLastFilter4MenuItem.Tag = (object)"3";
            this.customFilterLastFilter4MenuItem.Text = (string)null;
            this.customFilterLastFilter4MenuItem.Visible = false;
            this.customFilterLastFilter4MenuItem.Click += new EventHandler(this.CustomFilterLastFilterMenuItem_Click);
            this.customFilterLastFilter4MenuItem.TextChanged += new EventHandler(this.CustomFilterLastFilterMenuItem_TextChanged);
            this.customFilterLastFilter5MenuItem.Name = "customFilterLastFilter5MenuItem";
            this.customFilterLastFilter5MenuItem.Size = new Size(152, 22);
            this.customFilterLastFilter5MenuItem.Tag = (object)"4";
            this.customFilterLastFilter5MenuItem.Text = (string)null;
            this.customFilterLastFilter5MenuItem.Visible = false;
            this.customFilterLastFilter5MenuItem.Click += new EventHandler(this.CustomFilterLastFilterMenuItem_Click);
            this.customFilterLastFilter5MenuItem.TextChanged += new EventHandler(this.CustomFilterLastFilterMenuItem_TextChanged);
            this.customFilterLastFiltersListMenuItem.Name = "customFilterLastFiltersListMenuItem";
            this.customFilterLastFiltersListMenuItem.AutoSize = false;
            this.customFilterLastFiltersListMenuItem.Size = new Size(this.Width - 1, 22);
            this.customFilterLastFiltersListMenuItem.Image = (Image)Resources.ColumnHeader_Filtered;
            this.customFilterLastFiltersListMenuItem.ImageScaling = ToolStripItemImageScaling.None;
            this.customFilterLastFiltersListMenuItem.DropDownItems.AddRange(new ToolStripItem[7]
            {
        (ToolStripItem) this.customFilterMenuItem,
        (ToolStripItem) this.toolStripSeparator2MenuItem,
        (ToolStripItem) this.customFilterLastFilter1MenuItem,
        (ToolStripItem) this.customFilterLastFilter2MenuItem,
        (ToolStripItem) this.customFilterLastFilter3MenuItem,
        (ToolStripItem) this.customFilterLastFilter4MenuItem,
        (ToolStripItem) this.customFilterLastFilter5MenuItem
            });
            this.customFilterLastFiltersListMenuItem.MouseEnter += new EventHandler(this.CustomFilterLastFiltersListMenuItem_MouseEnter);
            this.customFilterLastFiltersListMenuItem.Paint += new PaintEventHandler(this.CustomFilterLastFiltersListMenuItem_Paint);
            this.toolStripSeparator3MenuItem.Name = "toolStripSeparator3MenuItem";
            this.toolStripSeparator3MenuItem.Size = new Size(this.Width - 4, 6);
            this.button_ok.Name = "button_ok";
            this.button_ok.BackColor = Control.DefaultBackColor;
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Margin = new Padding(0);
            this.button_ok.Size = new Size(75, 23);
            this.button_ok.Text = this._textStrings[(object)"BUTTONOK"].ToString();
            this.button_ok.Click += new EventHandler(this.Button_ok_Click);
            this.button_ok.Location = new Point(this.checkFilterListButtonsPanel.Width - 164, 0);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.BackColor = Control.DefaultBackColor;
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Margin = new Padding(0);
            this.button_cancel.Size = new Size(75, 23);
            this.button_cancel.Text = this._textStrings[(object)"BUTTONCANCEL"].ToString();
            this.button_cancel.Click += new EventHandler(this.Button_cancel_Click);
            this.button_cancel.Location = new Point(this.checkFilterListButtonsPanel.Width - 79, 0);
            this.resizeBoxControlHost.Name = "resizeBoxControlHost";
            this.resizeBoxControlHost.Control.Cursor = Cursors.SizeNWSE;
            this.resizeBoxControlHost.AutoSize = false;
            this.resizeBoxControlHost.Padding = new Padding(0);
            this.resizeBoxControlHost.Margin = new Padding(this.Width - 45, 0, 0, 0);
            this.resizeBoxControlHost.Size = new Size(10, 10);
            this.resizeBoxControlHost.Paint += new PaintEventHandler(this.ResizeBoxControlHost_Paint);
            this.resizeBoxControlHost.MouseDown += new MouseEventHandler(this.ResizeBoxControlHost_MouseDown);
            this.resizeBoxControlHost.MouseUp += new MouseEventHandler(this.ResizeBoxControlHost_MouseUp);
            this.resizeBoxControlHost.MouseMove += new MouseEventHandler(this.ResizeBoxControlHost_MouseMove);
            this.checkFilterListControlHost.Name = "checkFilterListControlHost";
            this.checkFilterListControlHost.AutoSize = false;
            this.checkFilterListControlHost.Size = new Size(this.Width - 35, 194);
            this.checkFilterListControlHost.Padding = new Padding(0);
            this.checkFilterListControlHost.Margin = new Padding(0);
            this.checkTextFilterControlHost.Name = "checkTextFilterControlHost";
            this.checkTextFilterControlHost.AutoSize = false;
            this.checkTextFilterControlHost.Size = new Size(this.Width - 35, 20);
            this.checkTextFilterControlHost.Padding = new Padding(4, 0, 4, 0);
            this.checkTextFilterControlHost.Margin = new Padding(0);
            this.checkFilterListButtonsControlHost.Name = "checkFilterListButtonsControlHost";
            this.checkFilterListButtonsControlHost.AutoSize = false;
            this.checkFilterListButtonsControlHost.Size = new Size(this.Width - 35, 24);
            this.checkFilterListButtonsControlHost.Padding = new Padding(0);
            this.checkFilterListButtonsControlHost.Margin = new Padding(0);
            this.checkFilterListPanel.Name = "checkFilterListPanel";
            this.checkFilterListPanel.AutoSize = false;
            this.checkFilterListPanel.Size = this.checkFilterListControlHost.Size;
            this.checkFilterListPanel.Padding = new Padding(0);
            this.checkFilterListPanel.Margin = new Padding(0);
            this.checkFilterListPanel.BackColor = this.BackColor;
            this.checkFilterListPanel.BorderStyle = BorderStyle.None;
            this.checkFilterListPanel.Controls.Add((Control)this.checkList);
            this.checkList.Name = "checkList";
            this.checkList.AutoSize = false;
            this.checkList.Padding = new Padding(0);
            this.checkList.Margin = new Padding(0);
            this.checkList.Bounds = new Rectangle(4, 4, this.checkFilterListPanel.Width - 8, this.checkFilterListPanel.Height - 8);
            this.checkList.StateImageList = this.GetCheckListStateImages();
            this.checkList.CheckBoxes = false;
            this.checkList.MouseLeave += new EventHandler(this.CheckList_MouseLeave);
            this.checkList.NodeMouseClick += new TreeNodeMouseClickEventHandler(this.CheckList_NodeMouseClick);
            this.checkList.KeyDown += new KeyEventHandler(this.CheckList_KeyDown);
            this.checkList.MouseEnter += new EventHandler(this.CheckList_MouseEnter);
            this.checkList.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(this.CheckList_NodeMouseDoubleClick);
            this.checkTextFilter.Name = "checkTextFilter";
            this.checkTextFilter.Padding = new Padding(0);
            this.checkTextFilter.Margin = new Padding(0);
            this.checkTextFilter.Size = this.checkTextFilterControlHost.Size;
            this.checkTextFilter.Dock = DockStyle.Fill;
            this.checkTextFilter.TextChanged += new EventHandler(this.CheckTextFilter_TextChanged);
            this.checkFilterListButtonsPanel.Name = "checkFilterListButtonsPanel";
            this.checkFilterListButtonsPanel.AutoSize = false;
            this.checkFilterListButtonsPanel.Size = this.checkFilterListButtonsControlHost.Size;
            this.checkFilterListButtonsPanel.Padding = new Padding(0);
            this.checkFilterListButtonsPanel.Margin = new Padding(0);
            this.checkFilterListButtonsPanel.BackColor = this.BackColor;
            this.checkFilterListButtonsPanel.BorderStyle = BorderStyle.None;
            this.checkFilterListButtonsPanel.Controls.AddRange(new Control[2]
            {
        (Control) this.button_ok,
        (Control) this.button_cancel
            });
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        public enum FilterType : byte
        {
            None,
            Custom,
            CheckList,
            Loaded,
        }

        public enum SortType : byte
        {
            None,
            ASC,
            DESC,
        }
    }
}