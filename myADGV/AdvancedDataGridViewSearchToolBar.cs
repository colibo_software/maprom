﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

namespace myADGV
{
    [DesignerCategory("")]
    public class AdvancedDataGridViewSearchToolBar : ToolStrip
    {
        private Hashtable _textStrings = new Hashtable();
        private const bool ButtonCloseEnabled = false;
        private DataGridViewColumnCollection _columnsList;
        private IContainer components;
        private ToolStripButton button_close;
        private ToolStripLabel label_search;
        private ToolStripComboBox comboBox_columns;
        private ToolStripTextBox textBox_search;
        private ToolStripButton button_frombegin;
        private ToolStripButton button_casesensitive;
        private ToolStripButton button_search;
        private ToolStripButton button_wholeword;
        private ToolStripSeparator separator_search;

        public event AdvancedDataGridViewSearchToolBarSearchEventHandler Search;

        public AdvancedDataGridViewSearchToolBar()
        {
            this._textStrings.Add((object)"LABELSEARCH", (object)"Search:");
            this._textStrings.Add((object)"BUTTONFROMBEGINTOOLTIP", (object)"From Begin");
            this._textStrings.Add((object)"BUTTONCASESENSITIVETOOLTIP", (object)"Case Sensitivity");
            this._textStrings.Add((object)"BUTTONSEARCHTOOLTIP", (object)"Find Next");
            this._textStrings.Add((object)"BUTTONCLOSETOOLTIP", (object)"Hide");
            this._textStrings.Add((object)"BUTTONWHOLEWORDTOOLTIP", (object)"Search only Whole Word");
            this._textStrings.Add((object)"COMBOBOXCOLUMNSALL", (object)"(All Columns)");
            this._textStrings.Add((object)"TEXTBOXSEARCHTOOLTIP", (object)"Value for Search");
            this.InitializeComponent();
            this.comboBox_columns.Items.AddRange(new object[1]
            {
        (object) this._textStrings[(object) "COMBOBOXCOLUMNSALL"].ToString()
            });
            this.button_close.ToolTipText = this._textStrings[(object)"BUTTONCLOSETOOLTIP"].ToString();
            this.label_search.Text = this._textStrings[(object)"LABELSEARCH"].ToString();
            this.textBox_search.ToolTipText = this._textStrings[(object)"TEXTBOXSEARCHTOOLTIP"].ToString();
            this.button_frombegin.ToolTipText = this._textStrings[(object)"BUTTONFROMBEGINTOOLTIP"].ToString();
            this.button_casesensitive.ToolTipText = this._textStrings[(object)"BUTTONCASESENSITIVETOOLTIP"].ToString();
            this.button_search.ToolTipText = this._textStrings[(object)"BUTTONSEARCHTOOLTIP"].ToString();
            this.button_wholeword.ToolTipText = this._textStrings[(object)"BUTTONWHOLEWORDTOOLTIP"].ToString();
            this.Items.RemoveAt(0);
            this.textBox_search.Text = this.textBox_search.ToolTipText;
            this.comboBox_columns.SelectedIndex = 0;
        }

        private void button_search_Click(object sender, EventArgs e)
        {
            if (this.textBox_search.TextLength <= 0 || !(this.textBox_search.Text != this.textBox_search.ToolTipText) || this.Search == null)
                return;
            DataGridViewColumn Column = (DataGridViewColumn)null;
            if (this.comboBox_columns.SelectedIndex > 0 && this._columnsList != null && this._columnsList.GetColumnCount(DataGridViewElementStates.Visible) > 0)
            {
                DataGridViewColumn[] array = this._columnsList.Cast<DataGridViewColumn>().Where<DataGridViewColumn>((Func<DataGridViewColumn, bool>)(col => col.Visible)).ToArray<DataGridViewColumn>();
                if (array.Length == this.comboBox_columns.Items.Count - 1 && array[this.comboBox_columns.SelectedIndex - 1].HeaderText == this.comboBox_columns.SelectedItem.ToString())
                    Column = array[this.comboBox_columns.SelectedIndex - 1];
            }
            this.Search((object)this, new AdvancedDataGridViewSearchToolBarSearchEventArgs(this.textBox_search.Text, Column, this.button_casesensitive.Checked, this.button_wholeword.Checked, this.button_frombegin.Checked));
        }

        private void button_close_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void textBox_search_TextChanged(object sender, EventArgs e)
        {
            this.button_search.Enabled = this.textBox_search.TextLength > 0 && this.textBox_search.Text != this.textBox_search.ToolTipText;
        }

        private void textBox_search_Enter(object sender, EventArgs e)
        {
            if (this.textBox_search.Text == this.textBox_search.ToolTipText && this.textBox_search.ForeColor == Color.LightGray)
                this.textBox_search.Text = "";
            else
                this.textBox_search.SelectAll();
            this.textBox_search.ForeColor = SystemColors.WindowText;
        }

        private void textBox_search_Leave(object sender, EventArgs e)
        {
            if (!(this.textBox_search.Text.Trim() == ""))
                return;
            this.textBox_search.Text = this.textBox_search.ToolTipText;
            this.textBox_search.ForeColor = Color.LightGray;
        }

        private void textBox_search_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.textBox_search.TextLength <= 0 || !(this.textBox_search.Text != this.textBox_search.ToolTipText) || e.KeyData != Keys.Return)
                return;
            this.button_search_Click((object)this.button_search, new EventArgs());
            e.SuppressKeyPress = true;
            e.Handled = true;
        }

        public void SetColumns(DataGridViewColumnCollection columns)
        {
            this._columnsList = columns;
            this.comboBox_columns.BeginUpdate();
            this.comboBox_columns.Items.Clear();
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(AdvancedDataGridViewSearchToolBar));
            this.comboBox_columns.Items.AddRange(new object[1]
            {
        (object) "(All columns)"
            });
            if (this._columnsList != null)
            {
                foreach (DataGridViewColumn columns1 in (BaseCollection)this._columnsList)
                {
                    if (columns1.Visible)
                        this.comboBox_columns.Items.Add((object)columns1.HeaderText);
                }
            }
            this.comboBox_columns.SelectedIndex = 0;
            this.comboBox_columns.EndUpdate();
        }

        private void ResizeMe(object sender, EventArgs e)
        {
            this.SuspendLayout();
            int w1 = 150;
            int w2 = 150;
            int num1 = this.comboBox_columns.Width + this.textBox_search.Width;
            foreach (ToolStripItem toolStripItem in (ArrangedElementCollection)this.Items)
            {
                toolStripItem.Overflow = ToolStripItemOverflow.Never;
                toolStripItem.Visible = true;
            }
            int width = this.PreferredSize.Width - num1 + w1 + w2;
            if (this.Width < width)
            {
                this.label_search.Visible = false;
                this.GetResizeBoxSize(this.PreferredSize.Width - num1 + w1 + w2, ref w1, ref w2);
                int num2 = this.PreferredSize.Width - num1 + w1 + w2;
                if (this.Width < num2)
                {
                    this.button_casesensitive.Overflow = ToolStripItemOverflow.Always;
                    this.GetResizeBoxSize(this.PreferredSize.Width - num1 + w1 + w2, ref w1, ref w2);
                    num2 = this.PreferredSize.Width - num1 + w1 + w2;
                }
                if (this.Width < num2)
                {
                    this.button_wholeword.Overflow = ToolStripItemOverflow.Always;
                    this.GetResizeBoxSize(this.PreferredSize.Width - num1 + w1 + w2, ref w1, ref w2);
                    num2 = this.PreferredSize.Width - num1 + w1 + w2;
                }
                if (this.Width < num2)
                {
                    this.button_frombegin.Overflow = ToolStripItemOverflow.Always;
                    this.separator_search.Visible = false;
                    this.GetResizeBoxSize(this.PreferredSize.Width - num1 + w1 + w2, ref w1, ref w2);
                    num2 = this.PreferredSize.Width - num1 + w1 + w2;
                }
                if (this.Width < num2)
                {
                    this.comboBox_columns.Overflow = ToolStripItemOverflow.Always;
                    this.textBox_search.Overflow = ToolStripItemOverflow.Always;
                    w1 = 150;
                    w2 = Math.Max(this.Width - this.PreferredSize.Width - this.textBox_search.Margin.Left - this.textBox_search.Margin.Right, 75);
                    this.textBox_search.Overflow = ToolStripItemOverflow.Never;
                    num2 = this.PreferredSize.Width - this.textBox_search.Width + w2;
                }
                if (this.Width < num2)
                {
                    this.button_search.Overflow = ToolStripItemOverflow.Always;
                    w2 = Math.Max(this.Width - this.PreferredSize.Width + this.textBox_search.Width, 75);
                    num2 = this.PreferredSize.Width - this.textBox_search.Width + w2;
                }
                if (this.Width < num2)
                {
                    this.button_close.Overflow = ToolStripItemOverflow.Always;
                    this.textBox_search.Margin = new Padding(8, 2, 8, 2);
                    w2 = Math.Max(this.Width - this.PreferredSize.Width + this.textBox_search.Width, 75);
                    num2 = this.PreferredSize.Width - this.textBox_search.Width + w2;
                }
                if (this.Width < num2)
                {
                    w2 = Math.Max(this.Width - this.PreferredSize.Width + this.textBox_search.Width, 20);
                    num2 = this.PreferredSize.Width - this.textBox_search.Width + w2;
                }
                if (num2 > this.Width)
                {
                    this.textBox_search.Overflow = ToolStripItemOverflow.Always;
                    this.textBox_search.Margin = new Padding(0, 2, 8, 2);
                    w2 = 150;
                }
            }
            else
                this.GetResizeBoxSize(width, ref w1, ref w2);
            if (this.comboBox_columns.Width != w1)
                this.comboBox_columns.Width = w1;
            if (this.textBox_search.Width != w2)
                this.textBox_search.Width = w2;
            this.ResumeLayout();
        }

        private void GetResizeBoxSize(int width, ref int w1, ref int w2)
        {
            int num1 = (int)Math.Round((double)(width - this.Width) / 2.0, 0, MidpointRounding.AwayFromZero);
            int num2 = w1;
            if (this.Width < width)
            {
                w1 = Math.Max(w1 - num1, 75);
                w2 = Math.Max(w2 - num1, 75);
            }
            else
            {
                w1 = Math.Min(w1 - num1, 150);
                w2 += this.Width - width + num2 - w1;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.button_close = new ToolStripButton();
            this.label_search = new ToolStripLabel();
            this.comboBox_columns = new ToolStripComboBox();
            this.textBox_search = new ToolStripTextBox();
            this.button_frombegin = new ToolStripButton();
            this.button_casesensitive = new ToolStripButton();
            this.button_search = new ToolStripButton();
            this.button_wholeword = new ToolStripButton();
            this.separator_search = new ToolStripSeparator();
            this.SuspendLayout();
            this.button_close.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.button_close.Image = (Image)Resources.SearchToolBar_ButtonCaseSensitive;
            this.button_close.ImageScaling = ToolStripItemImageScaling.None;
            this.button_close.ImageTransparentColor = Color.Magenta;
            this.button_close.Name = "button_close";
            this.button_close.Overflow = ToolStripItemOverflow.Never;
            this.button_close.Size = new Size(23, 24);
            this.button_close.Click += new EventHandler(this.button_close_Click);
            this.label_search.Name = "label_search";
            this.label_search.Size = new Size(45, 15);
            this.comboBox_columns.AutoSize = false;
            this.comboBox_columns.AutoToolTip = true;
            this.comboBox_columns.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBox_columns.FlatStyle = FlatStyle.Standard;
            this.comboBox_columns.IntegralHeight = false;
            this.comboBox_columns.Margin = new Padding(0, 2, 8, 2);
            this.comboBox_columns.MaxDropDownItems = 12;
            this.comboBox_columns.Name = "comboBox_columns";
            this.comboBox_columns.Size = new Size(150, 23);
            this.textBox_search.AutoSize = false;
            this.textBox_search.ForeColor = Color.LightGray;
            this.textBox_search.Margin = new Padding(0, 2, 8, 2);
            this.textBox_search.Name = "textBox_search";
            this.textBox_search.Overflow = ToolStripItemOverflow.Never;
            this.textBox_search.Size = new Size(100, 23);
            this.textBox_search.Enter += new EventHandler(this.textBox_search_Enter);
            this.textBox_search.Leave += new EventHandler(this.textBox_search_Leave);
            this.textBox_search.KeyDown += new KeyEventHandler(this.textBox_search_KeyDown);
            this.textBox_search.TextChanged += new EventHandler(this.textBox_search_TextChanged);
            this.button_frombegin.CheckOnClick = true;
            this.button_frombegin.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.button_frombegin.Image = (Image)Resources.SearchToolBar_ButtonFromBegin;
            this.button_frombegin.ImageScaling = ToolStripItemImageScaling.None;
            this.button_frombegin.ImageTransparentColor = Color.Magenta;
            this.button_frombegin.Name = "button_frombegin";
            this.button_frombegin.Size = new Size(23, 20);
            this.button_casesensitive.CheckOnClick = true;
            this.button_casesensitive.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.button_casesensitive.Image = (Image)Resources.SearchToolBar_ButtonCaseSensitive;
            this.button_casesensitive.ImageScaling = ToolStripItemImageScaling.None;
            this.button_casesensitive.ImageTransparentColor = Color.Magenta;
            this.button_casesensitive.Name = "button_casesensitive";
            this.button_casesensitive.Size = new Size(23, 20);
            this.button_search.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.button_search.Image = (Image)Resources.SearchToolBar_ButtonSearch;
            this.button_search.ImageScaling = ToolStripItemImageScaling.None;
            this.button_search.ImageTransparentColor = Color.Magenta;
            this.button_search.Name = "button_search";
            this.button_search.Overflow = ToolStripItemOverflow.Never;
            this.button_search.Size = new Size(23, 24);
            this.button_search.Click += new EventHandler(this.button_search_Click);
            this.button_wholeword.CheckOnClick = true;
            this.button_wholeword.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.button_wholeword.Image = (Image)Resources.SearchToolBar_ButtonWholeWord;
            this.button_wholeword.ImageScaling = ToolStripItemImageScaling.None;
            this.button_wholeword.ImageTransparentColor = Color.Magenta;
            this.button_wholeword.Margin = new Padding(1, 1, 1, 2);
            this.button_wholeword.Name = "button_wholeword";
            this.button_wholeword.Size = new Size(23, 20);
            this.separator_search.AutoSize = false;
            this.separator_search.Name = "separator_search";
            this.separator_search.Size = new Size(10, 25);
            this.AllowMerge = false;
            this.GripStyle = ToolStripGripStyle.Hidden;
            this.Items.AddRange(new ToolStripItem[9]
            {
        (ToolStripItem) this.button_close,
        (ToolStripItem) this.label_search,
        (ToolStripItem) this.comboBox_columns,
        (ToolStripItem) this.textBox_search,
        (ToolStripItem) this.button_frombegin,
        (ToolStripItem) this.button_wholeword,
        (ToolStripItem) this.button_casesensitive,
        (ToolStripItem) this.separator_search,
        (ToolStripItem) this.button_search
            });
            this.MaximumSize = new Size(0, 27);
            this.MinimumSize = new Size(0, 27);
            this.RenderMode = ToolStripRenderMode.Professional;
            this.Size = new Size(0, 27);
            this.Resize += new EventHandler(this.ResizeMe);
            this.ResumeLayout(false);
            this.PerformLayout();
        }
    }
}