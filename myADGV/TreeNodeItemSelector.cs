﻿using System.ComponentModel;
using System.Windows.Forms;

namespace myADGV
{
    [DesignerCategory("")]
    internal class TreeNodeItemSelector : TreeNode
    {
        private CheckState _checkState;
        private TreeNodeItemSelector _parent;

        private TreeNodeItemSelector(string text, object value, CheckState state, TreeNodeItemSelector.CustomNodeType nodeType)
          : base(text)
        {
            this.CheckState = state;
            this.NodeType = nodeType;
            this.Value = value;
        }

        public TreeNodeItemSelector Clone()
        {
            TreeNodeItemSelector nodeItemSelector1 = new TreeNodeItemSelector(this.Text, this.Value, this._checkState, this.NodeType);
            nodeItemSelector1.NodeFont = this.NodeFont;
            TreeNodeItemSelector nodeItemSelector2 = nodeItemSelector1;
            if (this.GetNodeCount(false) > 0)
            {
                foreach (TreeNodeItemSelector node in this.Nodes)
                    nodeItemSelector2.AddChild(node.Clone());
            }
            return nodeItemSelector2;
        }

        public TreeNodeItemSelector.CustomNodeType NodeType { get; private set; }

        public object Value { get; private set; }

        public TreeNodeItemSelector Parent {
            get {
                if (this._parent != null)
                    return this._parent;
                return (TreeNodeItemSelector)null;
            }
            set {
                this._parent = value;
            }
        }

        public new bool Checked {
            get {
                return this._checkState == CheckState.Checked;
            }
            set {
                this.CheckState = value ? CheckState.Checked : CheckState.Unchecked;
            }
        }

        public CheckState CheckState {
            get {
                return this._checkState;
            }
            set {
                this._checkState = value;
                switch (this._checkState)
                {
                    case CheckState.Checked:
                        this.StateImageIndex = 1;
                        break;
                    case CheckState.Indeterminate:
                        this.StateImageIndex = 2;
                        break;
                    default:
                        this.StateImageIndex = 0;
                        break;
                }
            }
        }

        public static TreeNodeItemSelector CreateNode(string text, object value, CheckState state, TreeNodeItemSelector.CustomNodeType type)
        {
            return new TreeNodeItemSelector(text, value, state, type);
        }

        public TreeNodeItemSelector CreateChildNode(string text, object value, CheckState state)
        {
            TreeNodeItemSelector child = (TreeNodeItemSelector)null;
            if (this.NodeType == TreeNodeItemSelector.CustomNodeType.DateTimeNode)
                child = new TreeNodeItemSelector(text, value, state, TreeNodeItemSelector.CustomNodeType.DateTimeNode);
            if (child != null)
                this.AddChild(child);
            return child;
        }

        public TreeNodeItemSelector CreateChildNode(string text, object value)
        {
            return this.CreateChildNode(text, value, this._checkState);
        }

        protected void AddChild(TreeNodeItemSelector child)
        {
            child.Parent = this;
            this.Nodes.Add((TreeNode)child);
        }

        public enum CustomNodeType : byte
        {
            Default,
            SelectAll,
            SelectEmpty,
            DateTimeNode,
        }
    }
}