﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace myADGV
{
    internal class FormCustomFilter : Form
    {
        private bool _filterDateAndTimeEnabled = true;
        private Hashtable _textStrings = new Hashtable();
        private FormCustomFilter.FilterType _filterType;
        private Control _valControl1;
        private Control _valControl2;
        private string _filterString;
        private string _filterStringDescription;
        private IContainer components;
        private Button button_ok;
        private Button button_cancel;
        private Label label_columnName;
        private ComboBox comboBox_filterType;
        private Label label_and;
        private ErrorProvider errorProvider;

        private FormCustomFilter()
        {
            this.InitializeComponent();
        }

        public FormCustomFilter(Type dataType, bool filterDateAndTimeEnabled)
          : this()
        {
            this._textStrings.Add((object)"EQUALS", (object)"equals");
            this._textStrings.Add((object)"DOES_NOT_EQUAL", (object)"does not equal");
            this._textStrings.Add((object)"EARLIER_THAN", (object)"earlier than");
            this._textStrings.Add((object)"EARLIER_THAN_OR_EQUAL_TO", (object)"earlier than or equal to");
            this._textStrings.Add((object)"LATER_THAN", (object)"later than");
            this._textStrings.Add((object)"LATER_THAN_OR_EQUAL_TO", (object)"later than or equal to");
            this._textStrings.Add((object)"BETWEEN", (object)"between");
            this._textStrings.Add((object)"GREATER_THAN", (object)"greater than");
            this._textStrings.Add((object)"GREATER_THAN_OR_EQUAL_TO", (object)"greater than or equal to");
            this._textStrings.Add((object)"LESS_THAN", (object)"less than");
            this._textStrings.Add((object)"LESS_THAN_OR_EQUAL_TO", (object)"less than or equal to");
            this._textStrings.Add((object)"BEGINS_WITH", (object)"begins with");
            this._textStrings.Add((object)"DOES_NOT_BEGIN_WITH", (object)"does not begin with");
            this._textStrings.Add((object)"ENDS_WITH", (object)"ends with");
            this._textStrings.Add((object)"DOES_NOT_END_WITH", (object)"does not end with");
            this._textStrings.Add((object)"CONTAINS", (object)"contains");
            this._textStrings.Add((object)"DOES_NOT_CONTAIN", (object)"does not contain");
            this._textStrings.Add((object)"INVALID_VALUE", (object)"Invalid Value");
            this._textStrings.Add((object)"FILTER_STRING_DESCRIPTION", (object)"Show rows where value {0} \"{1}\"");
            this._textStrings.Add((object)"FORM_TITLE", (object)"Custom Filter");
            this._textStrings.Add((object)"LABEL_COLUMNNAMETEXT", (object)"Show rows where value");
            this._textStrings.Add((object)"LABEL_AND", (object)"And");
            this._textStrings.Add((object)"BUTTON_OK", (object)"OK");
            this._textStrings.Add((object)"BUTTON_CANCEL", (object)"Cancel");
            this.Text = this._textStrings[(object)"FORM_TITLE"].ToString();
            this.label_columnName.Text = this._textStrings[(object)"LABEL_COLUMNNAMETEXT"].ToString();
            this.label_and.Text = this._textStrings[(object)"LABEL_AND"].ToString();
            this.button_ok.Text = this._textStrings[(object)"BUTTON_OK"].ToString();
            this.button_cancel.Text = this._textStrings[(object)"BUTTON_CANCEL"].ToString();
            this._filterType = !(dataType == typeof(DateTime)) ? (!(dataType == typeof(TimeSpan)) ? (dataType == typeof(int) || dataType == typeof(long) || (dataType == typeof(short) || dataType == typeof(uint)) || (dataType == typeof(ulong) || dataType == typeof(ushort) || (dataType == typeof(byte) || dataType == typeof(sbyte))) ? FormCustomFilter.FilterType.Integer : (dataType == typeof(float) || dataType == typeof(double) || dataType == typeof(Decimal) ? FormCustomFilter.FilterType.Float : (!(dataType == typeof(string)) ? FormCustomFilter.FilterType.Unknown : FormCustomFilter.FilterType.String))) : FormCustomFilter.FilterType.TimeSpan) : FormCustomFilter.FilterType.DateTime;
            this._filterDateAndTimeEnabled = filterDateAndTimeEnabled;
            switch (this._filterType)
            {
                case FormCustomFilter.FilterType.DateTime:
                    this._valControl1 = (Control)new DateTimePicker();
                    this._valControl2 = (Control)new DateTimePicker();
                    if (this._filterDateAndTimeEnabled)
                    {
                        DateTimeFormatInfo dateTimeFormat = Thread.CurrentThread.CurrentCulture.DateTimeFormat;
                        (this._valControl1 as DateTimePicker).CustomFormat = dateTimeFormat.ShortDatePattern + " HH:mm";
                        (this._valControl2 as DateTimePicker).CustomFormat = dateTimeFormat.ShortDatePattern + " HH:mm";
                        (this._valControl1 as DateTimePicker).Format = DateTimePickerFormat.Custom;
                        (this._valControl2 as DateTimePicker).Format = DateTimePickerFormat.Custom;
                    }
                    else
                    {
                        (this._valControl1 as DateTimePicker).Format = DateTimePickerFormat.Short;
                        (this._valControl2 as DateTimePicker).Format = DateTimePickerFormat.Short;
                    }
                    this.comboBox_filterType.Items.AddRange((object[])new string[7]
                    {
            this._textStrings[(object) "EQUALS"].ToString(),
            this._textStrings[(object) "DOES_NOT_EQUAL"].ToString(),
            this._textStrings[(object) "EARLIER_THAN"].ToString(),
            this._textStrings[(object) "EARLIER_THAN_OR_EQUAL_TO"].ToString(),
            this._textStrings[(object) "LATER_THAN"].ToString(),
            this._textStrings[(object) "LATER_THAN_OR_EQUAL_TO"].ToString(),
            this._textStrings[(object) "BETWEEN"].ToString()
                    });
                    break;
                case FormCustomFilter.FilterType.TimeSpan:
                    this._valControl1 = (Control)new TextBox();
                    this._valControl2 = (Control)new TextBox();
                    this.comboBox_filterType.Items.AddRange((object[])new string[2]
                    {
            this._textStrings[(object) "CONTAINS"].ToString(),
            this._textStrings[(object) "DOES_NOT_CONTAIN"].ToString()
                    });
                    break;
                case FormCustomFilter.FilterType.Float:
                case FormCustomFilter.FilterType.Integer:
                    this._valControl1 = (Control)new TextBox();
                    this._valControl2 = (Control)new TextBox();
                    this._valControl1.TextChanged += new EventHandler(this.valControl_TextChanged);
                    this._valControl2.TextChanged += new EventHandler(this.valControl_TextChanged);
                    this.comboBox_filterType.Items.AddRange((object[])new string[7]
                    {
            this._textStrings[(object) "EQUALS"].ToString(),
            this._textStrings[(object) "DOES_NOT_EQUAL"].ToString(),
            this._textStrings[(object) "GREATER_THAN"].ToString(),
            this._textStrings[(object) "GREATER_THAN_OR_EQUAL_TO"].ToString(),
            this._textStrings[(object) "LESS_THAN"].ToString(),
            this._textStrings[(object) "LESS_THAN_OR_EQUAL_TO"].ToString(),
            this._textStrings[(object) "BETWEEN"].ToString()
                    });
                    this._valControl1.Tag = (object)true;
                    this._valControl2.Tag = (object)true;
                    this.button_ok.Enabled = false;
                    break;
                default:
                    this._valControl1 = (Control)new TextBox();
                    this._valControl2 = (Control)new TextBox();
                    this.comboBox_filterType.Items.AddRange((object[])new string[8]
                    {
            this._textStrings[(object) "EQUALS"].ToString(),
            this._textStrings[(object) "DOES_NOT_EQUAL"].ToString(),
            this._textStrings[(object) "BEGINS_WITH"].ToString(),
            this._textStrings[(object) "DOES_NOT_BEGIN_WITH"].ToString(),
            this._textStrings[(object) "ENDS_WITH"].ToString(),
            this._textStrings[(object) "DOES_NOT_END_WITH"].ToString(),
            this._textStrings[(object) "CONTAINS"].ToString(),
            this._textStrings[(object) "DOES_NOT_CONTAIN"].ToString()
                    });
                    break;
            }
            this.comboBox_filterType.SelectedIndex = 0;
            this._valControl1.Name = "valControl1";
            this._valControl1.Location = new Point(30, 66);
            this._valControl1.Size = new Size(166, 20);
            this._valControl1.TabIndex = 4;
            this._valControl1.Visible = true;
            this._valControl1.KeyDown += new KeyEventHandler(this.valControl_KeyDown);
            this._valControl2.Name = "valControl2";
            this._valControl2.Location = new Point(30, 108);
            this._valControl2.Size = new Size(166, 20);
            this._valControl2.TabIndex = 5;
            this._valControl2.Visible = false;
            this._valControl2.VisibleChanged += new EventHandler(this.valControl2_VisibleChanged);
            this._valControl2.KeyDown += new KeyEventHandler(this.valControl_KeyDown);
            this.Controls.Add(this._valControl1);
            this.Controls.Add(this._valControl2);
            this.errorProvider.SetIconAlignment(this._valControl1, ErrorIconAlignment.MiddleRight);
            this.errorProvider.SetIconPadding(this._valControl1, -18);
            this.errorProvider.SetIconAlignment(this._valControl2, ErrorIconAlignment.MiddleRight);
            this.errorProvider.SetIconPadding(this._valControl2, -18);
        }

        private void FormCustomFilter_Load(object sender, EventArgs e)
        {
        }

        public string FilterString {
            get {
                return this._filterString;
            }
        }

        public string FilterStringDescription {
            get {
                return this._filterStringDescription;
            }
        }

        private string BuildCustomFilter(FormCustomFilter.FilterType filterType, bool filterDateAndTimeEnabled, string filterTypeConditionText, Control control1, Control control2)
        {
            string str1 = "[{0}] ";
            if (filterType == FormCustomFilter.FilterType.Unknown)
                str1 = "Convert([{0}], 'System.String') ";
            string str2 = str1;
            switch (filterType - 1)
            {
                case FormCustomFilter.FilterType.Unknown:
                    DateTime dateTime1 = ((DateTimePicker)control1).Value;
                    dateTime1 = new DateTime(dateTime1.Year, dateTime1.Month, dateTime1.Day, dateTime1.Hour, dateTime1.Minute, 0);
                    if (filterTypeConditionText == this._textStrings[(object)"EQUALS"].ToString())
                    {
                        str2 = "Convert([{0}], 'System.String') LIKE '%" + Convert.ToString(filterDateAndTimeEnabled ? dateTime1 : dateTime1.Date, (IFormatProvider)CultureInfo.CurrentCulture) + "%'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"EARLIER_THAN"].ToString())
                    {
                        str2 = str2 + "< '" + Convert.ToString(filterDateAndTimeEnabled ? dateTime1 : dateTime1.Date, (IFormatProvider)CultureInfo.CurrentCulture) + "'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"EARLIER_THAN_OR_EQUAL_TO"].ToString())
                    {
                        str2 = str2 + "<= '" + Convert.ToString(filterDateAndTimeEnabled ? dateTime1 : dateTime1.Date, (IFormatProvider)CultureInfo.CurrentCulture) + "'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"LATER_THAN"].ToString())
                    {
                        str2 = str2 + "> '" + Convert.ToString(filterDateAndTimeEnabled ? dateTime1 : dateTime1.Date, (IFormatProvider)CultureInfo.CurrentCulture) + "'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"LATER_THAN_OR_EQUAL_TO"].ToString())
                    {
                        str2 = str2 + ">= '" + Convert.ToString(filterDateAndTimeEnabled ? dateTime1 : dateTime1.Date, (IFormatProvider)CultureInfo.CurrentCulture) + "'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"BETWEEN"].ToString())
                    {
                        DateTime dateTime2 = ((DateTimePicker)control2).Value;
                        dateTime2 = new DateTime(dateTime2.Year, dateTime2.Month, dateTime2.Day, dateTime2.Hour, dateTime2.Minute, 0);
                        str2 = str2 + ">= '" + Convert.ToString(filterDateAndTimeEnabled ? dateTime1 : dateTime1.Date, (IFormatProvider)CultureInfo.CurrentCulture) + "'" + " AND " + str1 + "<= '" + Convert.ToString(filterDateAndTimeEnabled ? dateTime2 : dateTime2.Date, (IFormatProvider)CultureInfo.CurrentCulture) + "'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"DOES_NOT_EQUAL"].ToString())
                    {
                        str2 = "Convert([{0}], 'System.String') NOT LIKE '%" + Convert.ToString(filterDateAndTimeEnabled ? dateTime1 : dateTime1.Date, (IFormatProvider)CultureInfo.CurrentCulture) + "%'";
                        break;
                    }
                    break;
                case FormCustomFilter.FilterType.DateTime:
                    try
                    {
                        TimeSpan timeSpan = TimeSpan.Parse(control1.Text);
                        if (filterTypeConditionText == this._textStrings[(object)"CONTAINS"].ToString())
                        {
                            str2 = "(Convert([{0}], 'System.String') LIKE '%P" + (timeSpan.Days > 0 ? timeSpan.Days.ToString() + "D" : "") + (timeSpan.TotalHours > 0.0 ? "T" : "") + (timeSpan.Hours > 0 ? timeSpan.Hours.ToString() + "H" : "") + (timeSpan.Minutes > 0 ? timeSpan.Minutes.ToString() + "M" : "") + (timeSpan.Seconds > 0 ? timeSpan.Seconds.ToString() + "S" : "") + "%')";
                            break;
                        }
                        if (filterTypeConditionText == this._textStrings[(object)"DOES_NOT_CONTAIN"].ToString())
                        {
                            str2 = "(Convert([{0}], 'System.String') NOT LIKE '%P" + (timeSpan.Days > 0 ? timeSpan.Days.ToString() + "D" : "") + (timeSpan.TotalHours > 0.0 ? "T" : "") + (timeSpan.Hours > 0 ? timeSpan.Hours.ToString() + "H" : "") + (timeSpan.Minutes > 0 ? timeSpan.Minutes.ToString() + "M" : "") + (timeSpan.Seconds > 0 ? timeSpan.Seconds.ToString() + "S" : "") + "%')";
                            break;
                        }
                        break;
                    }
                    catch (FormatException ex)
                    {
                        str2 = (string)null;
                        break;
                    }
                case FormCustomFilter.FilterType.String:
                case FormCustomFilter.FilterType.Float:
                    string str3 = control1.Text;
                    if (filterType == FormCustomFilter.FilterType.Float)
                        str3 = str3.Replace(",", ".");
                    if (filterTypeConditionText == this._textStrings[(object)"EQUALS"].ToString())
                    {
                        str2 = str2 + "= " + str3;
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"BETWEEN"].ToString())
                    {
                        str2 = str2 + ">= " + str3 + " AND " + str1 + "<= " + (filterType == FormCustomFilter.FilterType.Float ? control2.Text.Replace(",", ".") : control2.Text);
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"DOES_NOT_EQUAL"].ToString())
                    {
                        str2 = str2 + "<> " + str3;
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"GREATER_THAN"].ToString())
                    {
                        str2 = str2 + "> " + str3;
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"GREATER_THAN_OR_EQUAL_TO"].ToString())
                    {
                        str2 = str2 + ">= " + str3;
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"LESS_THAN"].ToString())
                    {
                        str2 = str2 + "< " + str3;
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"LESS_THAN_OR_EQUAL_TO"].ToString())
                    {
                        str2 = str2 + "<= " + str3;
                        break;
                    }
                    break;
                default:
                    string str4 = this.FormatFilterString(control1.Text);
                    if (filterTypeConditionText == this._textStrings[(object)"EQUALS"].ToString())
                    {
                        str2 = str2 + "LIKE '" + str4 + "'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"DOES_NOT_EQUAL"].ToString())
                    {
                        str2 = str2 + "NOT LIKE '" + str4 + "'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"BEGINS_WITH"].ToString())
                    {
                        str2 = str2 + "LIKE '" + str4 + "%'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"ENDS_WITH"].ToString())
                    {
                        str2 = str2 + "LIKE '%" + str4 + "'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"DOES_NOT_BEGIN_WITH"].ToString())
                    {
                        str2 = str2 + "NOT LIKE '" + str4 + "%'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"DOES_NOT_END_WITH"].ToString())
                    {
                        str2 = str2 + "NOT LIKE '%" + str4 + "'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"CONTAINS"].ToString())
                    {
                        str2 = str2 + "LIKE '%" + str4 + "%'";
                        break;
                    }
                    if (filterTypeConditionText == this._textStrings[(object)"DOES_NOT_CONTAIN"].ToString())
                    {
                        str2 = str2 + "NOT LIKE '%" + str4 + "%'";
                        break;
                    }
                    break;
            }
            return str2;
        }

        private string FormatFilterString(string text)
        {
            string str1 = "";
            string[] strArray = new string[6]
            {
        "%",
        "[",
        "]",
        "*",
        "\"",
        "\\"
            };
            for (int index = 0; index < text.Length; ++index)
            {
                string str2 = text[index].ToString();
                str1 = !((IEnumerable<string>)strArray).Contains<string>(str2) ? str1 + str2 : str1 + "[" + str2 + "]";
            }
            return str1.Replace("'", "''");
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            this._filterStringDescription = (string)null;
            this._filterString = (string)null;
            this.Close();
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            if (this._valControl1.Visible && this._valControl1.Tag != null && (bool)this._valControl1.Tag || this._valControl2.Visible && this._valControl2.Tag != null && (bool)this._valControl2.Tag)
            {
                this.button_ok.Enabled = false;
            }
            else
            {
                string str1 = this.BuildCustomFilter(this._filterType, this._filterDateAndTimeEnabled, this.comboBox_filterType.Text, this._valControl1, this._valControl2);
                if (!string.IsNullOrEmpty(str1))
                {
                    this._filterString = str1;
                    this._filterStringDescription = string.Format(this._textStrings[(object)"FILTER_STRING_DESCRIPTION"].ToString(), (object)this.comboBox_filterType.Text, (object)this._valControl1.Text);
                    if (this._valControl2.Visible)
                    {
                        FormCustomFilter formCustomFilter = this;
                        string str2 = formCustomFilter._filterStringDescription + " " + this.label_and.Text + " \"" + this._valControl2.Text + "\"";
                        formCustomFilter._filterStringDescription = str2;
                    }
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    this._filterString = (string)null;
                    this._filterStringDescription = (string)null;
                    this.DialogResult = DialogResult.Cancel;
                }
                this.Close();
            }
        }

        private void comboBox_filterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._valControl2.Visible = this.comboBox_filterType.Text == this._textStrings[(object)"BETWEEN"].ToString();
            this.button_ok.Enabled = !this._valControl1.Visible || this._valControl1.Tag == null || !(bool)this._valControl1.Tag || this._valControl2.Visible && this._valControl2.Tag != null && (bool)this._valControl2.Tag;
        }

        private void valControl2_VisibleChanged(object sender, EventArgs e)
        {
            this.label_and.Visible = this._valControl2.Visible;
        }

        private void valControl_TextChanged(object sender, EventArgs e)
        {
            bool flag = false;
            switch (this._filterType)
            {
                case FormCustomFilter.FilterType.Float:
                    double result1;
                    flag = !double.TryParse((sender as TextBox).Text, out result1);
                    break;
                case FormCustomFilter.FilterType.Integer:
                    long result2;
                    flag = !long.TryParse((sender as TextBox).Text, out result2);
                    break;
            }
            var tmp = (flag ? 1 : ((sender as TextBox).Text.Length == 0 ? 1 : 0));
          (sender as Control).Tag = (object)(bool)(tmp==1);
            if (flag && (sender as TextBox).Text.Length > 0)
                this.errorProvider.SetError(sender as Control, this._textStrings[(object)"INVALID_VALUE"].ToString());
            else
                this.errorProvider.SetError(sender as Control, "");
            this.button_ok.Enabled = !this._valControl1.Visible || this._valControl1.Tag == null || !(bool)this._valControl1.Tag || this._valControl2.Visible && this._valControl2.Tag != null && (bool)this._valControl2.Tag;
        }

        private void valControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData != Keys.Return)
                return;
            if (sender == this._valControl1)
            {
                if (this._valControl2.Visible)
                    this._valControl2.Focus();
                else
                    this.button_ok_Click((object)this.button_ok, new EventArgs());
            }
            else
                this.button_ok_Click((object)this.button_ok, new EventArgs());
            e.SuppressKeyPress = false;
            e.Handled = true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = (IContainer)new Container();
            this.button_ok = new Button();
            this.button_cancel = new Button();
            this.label_columnName = new Label();
            this.comboBox_filterType = new ComboBox();
            this.label_and = new Label();
            this.errorProvider = new ErrorProvider(this.components);
            ((ISupportInitialize)this.errorProvider).BeginInit();
            this.SuspendLayout();
            this.button_ok.DialogResult = DialogResult.OK;
            this.button_ok.Location = new Point(40, 139);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new Size(75, 23);
            this.button_ok.TabIndex = 0;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new EventHandler(this.button_ok_Click);
            this.button_cancel.DialogResult = DialogResult.Cancel;
            this.button_cancel.Location = new Point(121, 139);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new Size(75, 23);
            this.button_cancel.TabIndex = 1;
            this.button_cancel.Text = "Cancel";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new EventHandler(this.button_cancel_Click);
            this.label_columnName.AutoSize = true;
            this.label_columnName.Location = new Point(4, 9);
            this.label_columnName.Name = "label_columnName";
            this.label_columnName.Size = new Size(120, 13);
            this.label_columnName.TabIndex = 2;
            this.label_columnName.Text = "Show rows where value";
            this.comboBox_filterType.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBox_filterType.FormattingEnabled = true;
            this.comboBox_filterType.Location = new Point(7, 25);
            this.comboBox_filterType.Name = "comboBox_filterType";
            this.comboBox_filterType.Size = new Size(189, 21);
            this.comboBox_filterType.TabIndex = 3;
            this.comboBox_filterType.SelectedIndexChanged += new EventHandler(this.comboBox_filterType_SelectedIndexChanged);
            this.label_and.AutoSize = true;
            this.label_and.Location = new Point(7, 89);
            this.label_and.Name = "label_and";
            this.label_and.Size = new Size(26, 13);
            this.label_and.TabIndex = 6;
            this.label_and.Text = "And";
            this.label_and.Visible = false;
            this.errorProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = (ContainerControl)this;
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.CancelButton = (IButtonControl)this.button_cancel;
            this.ClientSize = new Size(205, 169);
            this.Controls.Add((Control)this.label_and);
            this.Controls.Add((Control)this.label_columnName);
            this.Controls.Add((Control)this.comboBox_filterType);
            this.Controls.Add((Control)this.button_cancel);
            this.Controls.Add((Control)this.button_ok);
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = nameof(FormCustomFilter);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = FormStartPosition.CenterParent;
            this.Text = "Custom Filter";
            this.TopMost = true;
            this.Load += new EventHandler(this.FormCustomFilter_Load);
            ((ISupportInitialize)this.errorProvider).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private enum FilterType
        {
            Unknown,
            DateTime,
            TimeSpan,
            String,
            Float,
            Integer,
        }
    }
}