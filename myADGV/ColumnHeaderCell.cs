﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace myADGV
{
    [DesignerCategory("")]
    internal class ColumnHeaderCell : DataGridViewColumnHeaderCell
    {
        private Image _filterImage = (Image)Resources.ColumnHeader_UnFiltered;
        private Size _filterButtonImageSize = new Size(16, 16);
        private Rectangle _filterButtonOffsetBounds = Rectangle.Empty;
        private Rectangle _filterButtonImageBounds = Rectangle.Empty;
        private Padding _filterButtonMargin = new Padding(3, 4, 3, 4);
        private const bool FilterDateAndTimeDefaultEnabled = false;
        private bool _filterButtonPressed;
        private bool _filterButtonOver;
        private bool _filterEnabled;

        public event ColumnHeaderCellEventHandler FilterPopup;

        public event ColumnHeaderCellEventHandler SortChanged;

        public event ColumnHeaderCellEventHandler FilterChanged;

        public ColumnHeaderCell(DataGridViewColumnHeaderCell oldCell, bool filterEnabled)
        {
            this.Tag = oldCell.Tag;
            this.ErrorText = oldCell.ErrorText;
            this.ToolTipText = oldCell.ToolTipText;
            this.Value = oldCell.Value;
            this.ValueType = oldCell.ValueType;
            this.ContextMenuStrip = oldCell.ContextMenuStrip;
            this.Style = oldCell.Style;
            this._filterEnabled = filterEnabled;
            ColumnHeaderCell columnHeaderCell = oldCell as ColumnHeaderCell;
            if (columnHeaderCell != null && columnHeaderCell.MenuStrip != null)
            {
                this.MenuStrip = columnHeaderCell.MenuStrip;
                this._filterImage = columnHeaderCell._filterImage;
                this._filterButtonPressed = columnHeaderCell._filterButtonPressed;
                this._filterButtonOver = columnHeaderCell._filterButtonOver;
                this._filterButtonOffsetBounds = columnHeaderCell._filterButtonOffsetBounds;
                this._filterButtonImageBounds = columnHeaderCell._filterButtonImageBounds;
                this.MenuStrip.FilterChanged += new EventHandler(this.MenuStrip_FilterChanged);
                this.MenuStrip.SortChanged += new EventHandler(this.MenuStrip_SortChanged);
            }
            else
            {
                this.MenuStrip = new MenuStrip(oldCell.OwningColumn.ValueType);
                this.MenuStrip.FilterChanged += new EventHandler(this.MenuStrip_FilterChanged);
                this.MenuStrip.SortChanged += new EventHandler(this.MenuStrip_SortChanged);
            }
            this.IsFilterDateAndTimeEnabled = false;
            this.IsSortEnabled = true;
            this.IsFilterEnabled = true;
            this.IsFilterChecklistEnabled = true;
        }

        ~ColumnHeaderCell()
        {
            if (this.MenuStrip == null)
                return;
            this.MenuStrip.FilterChanged -= new EventHandler(this.MenuStrip_FilterChanged);
            this.MenuStrip.SortChanged -= new EventHandler(this.MenuStrip_SortChanged);
        }

        public override object Clone()
        {
            return (object)new ColumnHeaderCell((DataGridViewColumnHeaderCell)this, this.FilterAndSortEnabled);
        }

        public bool FilterAndSortEnabled {
            get {
                return this._filterEnabled;
            }
            set {
                if (!value)
                {
                    this._filterButtonPressed = false;
                    this._filterButtonOver = false;
                }
                if (value == this._filterEnabled)
                    return;
                this._filterEnabled = value;
                bool flag = false;
                if (this.MenuStrip.FilterString.Length > 0)
                {
                    this.MenuStrip_FilterChanged((object)this, new EventArgs());
                    flag = true;
                }
                if (this.MenuStrip.SortString.Length > 0)
                {
                    this.MenuStrip_SortChanged((object)this, new EventArgs());
                    flag = true;
                }
                if (flag)
                    return;
                this.RepaintCell();
            }
        }

        public void SetLoadedMode(bool enabled)
        {
            this.MenuStrip.SetLoadedMode(enabled);
            this.RefreshImage();
            this.RepaintCell();
        }

        public void CleanSort()
        {
            if (this.MenuStrip == null || !this.FilterAndSortEnabled)
                return;
            this.MenuStrip.CleanSort();
            this.RefreshImage();
            this.RepaintCell();
        }

        public void CleanFilter()
        {
            if (this.MenuStrip == null || !this.FilterAndSortEnabled)
                return;
            this.MenuStrip.CleanFilter();
            this.RefreshImage();
            this.RepaintCell();
        }

        public void SortASC()
        {
            if (this.MenuStrip == null || !this.FilterAndSortEnabled)
                return;
            this.MenuStrip.SortASC();
        }

        public void SortDESC()
        {
            if (this.MenuStrip == null || !this.FilterAndSortEnabled)
                return;
            this.MenuStrip.SortDESC();
        }

        public MenuStrip MenuStrip { get; private set; }

        public MenuStrip.SortType ActiveSortType {
            get {
                if (this.MenuStrip != null && this.FilterAndSortEnabled)
                    return this.MenuStrip.ActiveSortType;
                return MenuStrip.SortType.None;
            }
        }

        public MenuStrip.FilterType ActiveFilterType {
            get {
                if (this.MenuStrip != null && this.FilterAndSortEnabled)
                    return this.MenuStrip.ActiveFilterType;
                return MenuStrip.FilterType.None;
            }
        }

        public string SortString {
            get {
                if (this.MenuStrip != null && this.FilterAndSortEnabled)
                    return this.MenuStrip.SortString;
                return "";
            }
        }

        public string FilterString {
            get {
                if (this.MenuStrip != null && this.FilterAndSortEnabled)
                    return this.MenuStrip.FilterString;
                return "";
            }
        }

        public Size MinimumSize {
            get {
                return new Size(this._filterButtonImageSize.Width + this._filterButtonMargin.Left + this._filterButtonMargin.Right, this._filterButtonImageSize.Height + this._filterButtonMargin.Bottom + this._filterButtonMargin.Top);
            }
        }

        public bool IsSortEnabled {
            get {
                return this.MenuStrip.IsSortEnabled;
            }
            set {
                this.MenuStrip.IsSortEnabled = value;
            }
        }

        public bool IsFilterEnabled {
            get {
                return this.MenuStrip.IsFilterEnabled;
            }
            set {
                this.MenuStrip.IsFilterEnabled = value;
            }
        }

        public bool IsFilterChecklistEnabled {
            get {
                return this.MenuStrip.IsFilterChecklistEnabled;
            }
            set {
                this.MenuStrip.IsFilterChecklistEnabled = value;
            }
        }

        public bool IsFilterDateAndTimeEnabled {
            get {
                return this.MenuStrip.IsFilterDateAndTimeEnabled;
            }
            set {
                this.MenuStrip.IsFilterDateAndTimeEnabled = value;
            }
        }

        public bool IsMenuStripFilterNOTINLogicEnabled {
            get {
                return this.MenuStrip.IsFilterNOTINLogicEnabled;
            }
            set {
                this.MenuStrip.IsFilterNOTINLogicEnabled = value;
            }
        }

        public bool DoesTextFilterRemoveNodesOnSearch {
            get {
                return this.MenuStrip.DoesTextFilterRemoveNodesOnSearch;
            }
            set {
                this.MenuStrip.DoesTextFilterRemoveNodesOnSearch = value;
            }
        }

        public void SetSortEnabled(bool enabled)
        {
            if (this.MenuStrip == null)
                return;
            this.MenuStrip.IsSortEnabled = enabled;
            this.MenuStrip.SetSortEnabled(enabled);
        }

        public void SetFilterEnabled(bool enabled)
        {
            if (this.MenuStrip == null)
                return;
            this.MenuStrip.IsFilterEnabled = enabled;
            this.MenuStrip.SetFilterEnabled(enabled);
        }

        public void SetFilterChecklistEnabled(bool enabled)
        {
            if (this.MenuStrip == null)
                return;
            this.MenuStrip.IsFilterChecklistEnabled = enabled;
            this.MenuStrip.SetFilterChecklistEnabled(enabled);
        }

        public void SetChecklistTextFilterRemoveNodesOnSearchMode(bool enabled)
        {
            if (this.MenuStrip == null)
                return;
            this.MenuStrip.SetChecklistTextFilterRemoveNodesOnSearchMode(enabled);
        }

        private void MenuStrip_FilterChanged(object sender, EventArgs e)
        {
            this.RefreshImage();
            this.RepaintCell();
            if (!this.FilterAndSortEnabled || this.FilterChanged == null)
                return;
            this.FilterChanged((object)this, new ColumnHeaderCellEventArgs(this.MenuStrip, this.OwningColumn));
        }

        private void MenuStrip_SortChanged(object sender, EventArgs e)
        {
            this.RefreshImage();
            this.RepaintCell();
            if (!this.FilterAndSortEnabled || this.SortChanged == null)
                return;
            this.SortChanged((object)this, new ColumnHeaderCellEventArgs(this.MenuStrip, this.OwningColumn));
        }

        private void RepaintCell()
        {
            if (!this.Displayed || this.DataGridView == null)
                return;
            this.DataGridView.InvalidateCell((DataGridViewCell)this);
        }

        private void RefreshImage()
        {
            if (this.ActiveFilterType == MenuStrip.FilterType.Loaded)
                this._filterImage = (Image)Resources.ColumnHeader_SavedFilters;
            else if (this.ActiveFilterType == MenuStrip.FilterType.None)
            {
                if (this.ActiveSortType == MenuStrip.SortType.None)
                    this._filterImage = (Image)Resources.ColumnHeader_UnFiltered;
                else if (this.ActiveSortType == MenuStrip.SortType.ASC)
                    this._filterImage = (Image)Resources.ColumnHeader_OrderedASC;
                else
                    this._filterImage = (Image)Resources.ColumnHeader_OrderedDESC;
            }
            else if (this.ActiveSortType == MenuStrip.SortType.None)
                this._filterImage = (Image)Resources.ColumnHeader_Filtered;
            else if (this.ActiveSortType == MenuStrip.SortType.ASC)
                this._filterImage = (Image)Resources.ColumnHeader_FilteredAndOrderedASC;
            else
                this._filterImage = (Image)Resources.ColumnHeader_FilteredAndOrderedDESC;
        }

        protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {
            if (this.SortGlyphDirection != SortOrder.None)
                this.SortGlyphDirection = SortOrder.None;
            base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
            if (this.OwningColumn.ValueType == typeof(Bitmap) || !this.FilterAndSortEnabled || !paintParts.HasFlag((Enum)DataGridViewPaintParts.ContentBackground))
                return;
            this._filterButtonOffsetBounds = this.GetFilterBounds(true);
            this._filterButtonImageBounds = this.GetFilterBounds(false);
            Rectangle buttonOffsetBounds = this._filterButtonOffsetBounds;
            if (!clipBounds.IntersectsWith(buttonOffsetBounds))
                return;
            ControlPaint.DrawBorder(graphics, buttonOffsetBounds, Color.Gray, ButtonBorderStyle.Solid);
            buttonOffsetBounds.Inflate(-1, -1);
            using (Brush brush = (Brush)new SolidBrush(this._filterButtonOver ? Color.WhiteSmoke : Color.White))
                graphics.FillRectangle(brush, buttonOffsetBounds);
            graphics.DrawImage(this._filterImage, buttonOffsetBounds);
        }

        private Rectangle GetFilterBounds(bool withOffset = true)
        {
            Rectangle displayRectangle = this.DataGridView.GetCellDisplayRectangle(this.ColumnIndex, -1, false);
            return new Rectangle(new Point((withOffset ? displayRectangle.Right : displayRectangle.Width) - this._filterButtonImageSize.Width - this._filterButtonMargin.Right, (withOffset ? displayRectangle.Bottom : displayRectangle.Height) - this._filterButtonImageSize.Height - this._filterButtonMargin.Bottom), this._filterButtonImageSize);
        }

        protected override void OnMouseMove(DataGridViewCellMouseEventArgs e)
        {
            if (this.FilterAndSortEnabled)
            {
                if (this._filterButtonImageBounds.Contains(e.X, e.Y) && !this._filterButtonOver)
                {
                    this._filterButtonOver = true;
                    this.RepaintCell();
                }
                else if (!this._filterButtonImageBounds.Contains(e.X, e.Y) && this._filterButtonOver)
                {
                    this._filterButtonOver = false;
                    this.RepaintCell();
                }
            }
            base.OnMouseMove(e);
        }

        protected override void OnMouseDown(DataGridViewCellMouseEventArgs e)
        {
            if (this.FilterAndSortEnabled && this._filterButtonImageBounds.Contains(e.X, e.Y))
            {
                if (e.Button != MouseButtons.Left || this._filterButtonPressed)
                    return;
                this._filterButtonPressed = true;
                this._filterButtonOver = true;
                this.RepaintCell();
            }
            else
                base.OnMouseDown(e);
        }

        protected override void OnMouseUp(DataGridViewCellMouseEventArgs e)
        {
            if (this.FilterAndSortEnabled && e.Button == MouseButtons.Left && this._filterButtonPressed)
            {
                this._filterButtonPressed = false;
                this._filterButtonOver = false;
                this.RepaintCell();
                if (this._filterButtonImageBounds.Contains(e.X, e.Y) && this.FilterPopup != null)
                    this.FilterPopup((object)this, new ColumnHeaderCellEventArgs(this.MenuStrip, this.OwningColumn));
            }
            base.OnMouseUp(e);
        }

        protected override void OnMouseLeave(int rowIndex)
        {
            if (this.FilterAndSortEnabled && this._filterButtonOver)
            {
                this._filterButtonOver = false;
                this.RepaintCell();
            }
            base.OnMouseLeave(rowIndex);
        }
    }
}