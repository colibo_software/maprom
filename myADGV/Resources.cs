﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace myADGV
{
    [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [DebuggerNonUserCode]
    [CompilerGenerated]
    internal class Resources
    {
        private static ResourceManager resourceMan;
        private static CultureInfo resourceCulture;

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources()
        {
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals((object)Resource.resourceMan, (object)null))
                    Resource.resourceMan = new ResourceManager("Resource", typeof(Resource).Assembly);
                return Resource.resourceMan;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture {
            get {
                return Resource.resourceCulture;
            }
            set {
                Resource.resourceCulture = value;
            }
        }

        internal static Bitmap ColumnHeader_Filtered {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(ColumnHeader_Filtered), Resource.resourceCulture);
            }
        }

        internal static Bitmap ColumnHeader_FilteredAndOrderedASC {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(ColumnHeader_FilteredAndOrderedASC), Resource.resourceCulture);
            }
        }

        internal static Bitmap ColumnHeader_FilteredAndOrderedDESC {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(ColumnHeader_FilteredAndOrderedDESC), Resource.resourceCulture);
            }
        }

        internal static Bitmap ColumnHeader_OrderedASC {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(ColumnHeader_OrderedASC), Resource.resourceCulture);
            }
        }

        internal static Bitmap ColumnHeader_OrderedDESC {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(ColumnHeader_OrderedDESC), Resource.resourceCulture);
            }
        }

        internal static Bitmap ColumnHeader_SavedFilters {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(ColumnHeader_SavedFilters), Resource.resourceCulture);
            }
        }

        internal static Bitmap ColumnHeader_UnFiltered {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(ColumnHeader_UnFiltered), Resource.resourceCulture);
            }
        }

        internal static Bitmap MenuStrip_OrderASCbool {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(MenuStrip_OrderASCbool), Resource.resourceCulture);
            }
        }

        internal static Bitmap MenuStrip_OrderASCnum {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(MenuStrip_OrderASCnum), Resource.resourceCulture);
            }
        }

        internal static Bitmap MenuStrip_OrderASCtxt {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(MenuStrip_OrderASCtxt), Resource.resourceCulture);
            }
        }

        internal static Bitmap MenuStrip_OrderDESCbool {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(MenuStrip_OrderDESCbool), Resource.resourceCulture);
            }
        }

        internal static Bitmap MenuStrip_OrderDESCnum {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(MenuStrip_OrderDESCnum), Resource.resourceCulture);
            }
        }

        internal static Bitmap MenuStrip_OrderDESCtxt {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(MenuStrip_OrderDESCtxt), Resource.resourceCulture);
            }
        }

        internal static Bitmap MenuStrip_ResizeGrip {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(MenuStrip_ResizeGrip), Resource.resourceCulture);
            }
        }

        internal static Bitmap SearchToolBar_ButtonCaseSensitive {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(SearchToolBar_ButtonCaseSensitive), Resource.resourceCulture);
            }
        }

        internal static Bitmap SearchToolBar_ButtonClose {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(SearchToolBar_ButtonClose), Resource.resourceCulture);
            }
        }

        internal static Bitmap SearchToolBar_ButtonFromBegin {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(SearchToolBar_ButtonFromBegin), Resource.resourceCulture);
            }
        }

        internal static Bitmap SearchToolBar_ButtonSearch {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(SearchToolBar_ButtonSearch), Resource.resourceCulture);
            }
        }

        internal static Bitmap SearchToolBar_ButtonWholeWord {
            get {
                return (Bitmap)Resource.ResourceManager.GetObject(nameof(SearchToolBar_ButtonWholeWord), Resource.resourceCulture);
            }
        }
    }
}