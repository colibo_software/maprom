﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace myADGV
{
    [DesignerCategory("")]
    public class AdvancedDataGridView : DataGridView
    {
        private List<string> _sortOrderList = new List<string>();
        private List<string> _filterOrderList = new List<string>();
        private List<string> _filteredColumns = new List<string>();
        private bool _filterAndSortEnabled = true;
        private bool _loadedFilter;
        private string _sortString;
        private string _filterString;

        public event EventHandler<AdvancedDataGridView.SortEventArgs> SortStringChanged;

        public event EventHandler<AdvancedDataGridView.FilterEventArgs> FilterStringChanged;

        public void SetDoubleBuffered()
        {
            this.DoubleBuffered = true;
        }

        public void DisableFilterAndSort(DataGridViewColumn column)
        {
            if (!this.Columns.Contains(column))
                return;
            ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
            if (headerCell == null)
                return;
            if (headerCell.FilterAndSortEnabled && (headerCell.SortString.Length > 0 || headerCell.FilterString.Length > 0))
            {
                this.CleanFilter(true);
                headerCell.FilterAndSortEnabled = false;
            }
            else
                headerCell.FilterAndSortEnabled = false;
            this._filterOrderList.Remove(column.Name);
            this._sortOrderList.Remove(column.Name);
            this._filteredColumns.Remove(column.Name);
        }

        public void EnableFilterAndSort(DataGridViewColumn column)
        {
            if (!this.Columns.Contains(column))
                return;
            ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
            if (headerCell != null)
            {
                if (!headerCell.FilterAndSortEnabled && (headerCell.FilterString.Length > 0 || headerCell.SortString.Length > 0))
                    this.CleanFilter(true);
                headerCell.FilterAndSortEnabled = true;
                this._filteredColumns.Remove(column.Name);
                this.SetFilterDateAndTimeEnabled(column, headerCell.IsFilterDateAndTimeEnabled);
                this.SetSortEnabled(column, headerCell.IsSortEnabled);
                this.SetFilterEnabled(column, headerCell.IsFilterEnabled);
            }
            else
            {
                column.SortMode = DataGridViewColumnSortMode.Programmatic;
                ColumnHeaderCell columnHeaderCell = new ColumnHeaderCell(column.HeaderCell, true);
                columnHeaderCell.SortChanged += new ColumnHeaderCellEventHandler(this.Cell_SortChanged);
                columnHeaderCell.FilterChanged += new ColumnHeaderCellEventHandler(this.Cell_FilterChanged);
                columnHeaderCell.FilterPopup += new ColumnHeaderCellEventHandler(this.Cell_FilterPopup);
                column.MinimumWidth = columnHeaderCell.MinimumSize.Width;
                if (this.ColumnHeadersHeight < columnHeaderCell.MinimumSize.Height)
                    this.ColumnHeadersHeight = columnHeaderCell.MinimumSize.Height;
                column.HeaderCell = (DataGridViewColumnHeaderCell)columnHeaderCell;
            }
        }

        public void SetFilterAndSortEnabled(DataGridViewColumn column, bool enabled)
        {
            if (enabled)
                this.EnableFilterAndSort(column);
            else
                this.DisableFilterAndSort(column);
        }

        public void DisableFilterChecklist(DataGridViewColumn column)
        {
            if (!this.Columns.Contains(column))
                return;
            ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
            if (headerCell == null)
                return;
            headerCell.SetFilterChecklistEnabled(false);
        }

        public void EnableFilterChecklist(DataGridViewColumn column)
        {
            if (!this.Columns.Contains(column))
                return;
            ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
            if (headerCell == null)
                return;
            headerCell.SetFilterChecklistEnabled(true);
        }

        public void SetFilterChecklistEnabled(DataGridViewColumn column, bool enabled)
        {
            if (enabled)
                this.EnableFilterChecklist(column);
            else
                this.DisableFilterChecklist(column);
        }

        public void LoadFilterAndSort(string filter, string sorting)
        {
            foreach (ColumnHeaderCell filterableCell in this.FilterableCells)
                filterableCell.SetLoadedMode(true);
            this._filteredColumns.Clear();
            this._filterOrderList.Clear();
            this._sortOrderList.Clear();
            if (filter != null)
                this.FilterString = filter;
            if (sorting != null)
                this.SortString = sorting;
            this._loadedFilter = true;
        }

        public void CleanFilterAndSort()
        {
            foreach (ColumnHeaderCell filterableCell in this.FilterableCells)
                filterableCell.SetLoadedMode(false);
            this._filteredColumns.Clear();
            this._filterOrderList.Clear();
            this._sortOrderList.Clear();
            this._loadedFilter = false;
            this.CleanFilter();
            this.CleanSort();
        }

        public void SetMenuStripFilterNOTINLogic(bool enabled)
        {
            foreach (ColumnHeaderCell filterableCell in this.FilterableCells)
                filterableCell.IsMenuStripFilterNOTINLogicEnabled = enabled;
        }

        public bool FilterAndSortEnabled {
            get {
                return this._filterAndSortEnabled;
            }
            set {
                this._filterAndSortEnabled = value;
            }
        }

        public string SortString {
            get {
                if (string.IsNullOrEmpty(this._sortString))
                    return "";
                return this._sortString;
            }
            private set {
                if (!(value != this._sortString))
                    return;
                this._sortString = value;
                this.TriggerSortStringChanged();
            }
        }

        public void TriggerSortStringChanged()
        {
            AdvancedDataGridView.SortEventArgs e = new AdvancedDataGridView.SortEventArgs()
            {
                SortString = this._sortString,
                Cancel = false
            };
            if (this.SortStringChanged != null)
                this.SortStringChanged((object)this, e);
            if (e.Cancel)
                return;
            BindingSource dataSource = this.DataSource as BindingSource;
            if (dataSource == null)
                return;
            dataSource.Sort = e.SortString;
        }

        public void SetSortEnabled(DataGridViewColumn column, bool enabled)
        {
            if (!this.Columns.Contains(column))
                return;
            ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
            if (headerCell == null)
                return;
            headerCell.SetSortEnabled(enabled);
        }

        public void SortASC(DataGridViewColumn column)
        {
            if (!this.Columns.Contains(column))
                return;
            ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
            if (headerCell == null)
                return;
            headerCell.SortASC();
        }

        public void SortDESC(DataGridViewColumn column)
        {
            if (!this.Columns.Contains(column))
                return;
            ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
            if (headerCell == null)
                return;
            headerCell.SortDESC();
        }

        public void CleanSort(bool fireEvent)
        {
            foreach (ColumnHeaderCell filterableCell in this.FilterableCells)
                filterableCell.CleanSort();
            this._sortOrderList.Clear();
            if (fireEvent)
                this.SortString = (string)null;
            else
                this._sortString = (string)null;
        }

        public void CleanSort()
        {
            this.CleanSort(true);
        }

        public string FilterString {
            get {
                if (string.IsNullOrEmpty(this._filterString))
                    return "";
                return this._filterString;
            }
            private set {
                if (!(value != this._filterString))
                    return;
                this._filterString = value;
                this.TriggerFilterStringChanged();
            }
        }

        public void TriggerFilterStringChanged()
        {
            AdvancedDataGridView.FilterEventArgs e = new AdvancedDataGridView.FilterEventArgs()
            {
                FilterString = this._filterString,
                Cancel = false
            };
            if (this.FilterStringChanged != null)
                this.FilterStringChanged((object)this, e);
            if (e.Cancel)
                return;
            BindingSource dataSource = this.DataSource as BindingSource;
            if (dataSource == null)
                return;
            dataSource.Filter = e.FilterString;
        }

        public void SetFilterDateAndTimeEnabled(DataGridViewColumn column, bool enabled)
        {
            if (!this.Columns.Contains(column))
                return;
            ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
            if (headerCell == null)
                return;
            headerCell.IsFilterDateAndTimeEnabled = enabled;
        }

        public void SetFilterEnabled(DataGridViewColumn column, bool enabled)
        {
            if (!this.Columns.Contains(column))
                return;
            ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
            if (headerCell == null)
                return;
            headerCell.SetFilterEnabled(enabled);
        }

        public void SetChecklistTextFilterRemoveNodesOnSearchMode(DataGridViewColumn column, bool enabled)
        {
            if (!this.Columns.Contains(column))
                return;
            ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
            if (headerCell == null)
                return;
            headerCell.SetChecklistTextFilterRemoveNodesOnSearchMode(enabled);
        }

        public void CleanFilter(bool fireEvent)
        {
            foreach (ColumnHeaderCell filterableCell in this.FilterableCells)
                filterableCell.CleanFilter();
            this._filterOrderList.Clear();
            if (fireEvent)
                this.FilterString = (string)null;
            else
                this._filterString = (string)null;
        }

        public void CleanFilter()
        {
            this.CleanFilter(true);
        }

        public void SetTextFilterRemoveNodesOnSearch(DataGridViewColumn column, bool enabled)
        {
            if (!this.Columns.Contains(column))
                return;
            ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
            if (headerCell == null)
                return;
            headerCell.DoesTextFilterRemoveNodesOnSearch = enabled;
        }

        public bool? GetTextFilterRemoveNodesOnSearch(DataGridViewColumn column)
        {
            bool? nullable = new bool?();
            if (this.Columns.Contains(column))
            {
                ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
                if (headerCell != null)
                    nullable = new bool?(headerCell.DoesTextFilterRemoveNodesOnSearch);
            }
            return nullable;
        }

        public DataGridViewCell FindCell(string valueToFind, string columnName, int rowIndex, int columnIndex, bool isWholeWordSearch, bool isCaseSensitive)
        {
            if (valueToFind != null && this.RowCount > 0 && this.ColumnCount > 0 && (columnName == null || this.Columns.Contains(columnName) && this.Columns[columnName].Visible))
            {
                rowIndex = Math.Max(0, rowIndex);
                if (!isCaseSensitive)
                    valueToFind = valueToFind.ToLower();
                if (columnName != null)
                {
                    int index1 = this.Columns[columnName].Index;
                    if (columnIndex > index1)
                        ++rowIndex;
                    for (int index2 = rowIndex; index2 < this.RowCount; ++index2)
                    {
                        string lower = this.Rows[index2].Cells[index1].FormattedValue.ToString();
                        if (!isCaseSensitive)
                            lower = lower.ToLower();
                        if (!isWholeWordSearch && lower.Contains(valueToFind) || lower.Equals(valueToFind))
                            return this.Rows[index2].Cells[index1];
                    }
                }
                else
                {
                    columnIndex = Math.Max(0, columnIndex);
                    for (int index1 = rowIndex; index1 < this.RowCount; ++index1)
                    {
                        for (int index2 = columnIndex; index2 < this.ColumnCount; ++index2)
                        {
                            if (this.Rows[index1].Cells[index2].Visible)
                            {
                                string lower = this.Rows[index1].Cells[index2].FormattedValue.ToString();
                                if (!isCaseSensitive)
                                    lower = lower.ToLower();
                                if (!isWholeWordSearch && lower.Contains(valueToFind) || lower.Equals(valueToFind))
                                    return this.Rows[index1].Cells[index2];
                            }
                        }
                        columnIndex = 0;
                    }
                }
            }
            return (DataGridViewCell)null;
        }

        private IEnumerable<ColumnHeaderCell> FilterableCells {
            get {
                return this.Columns.Cast<DataGridViewColumn>().Where<DataGridViewColumn>((Func<DataGridViewColumn, bool>)(c =>
                {
                    if (c.HeaderCell != null)
                        return c.HeaderCell is ColumnHeaderCell;
                    return false;
                })).Select<DataGridViewColumn, ColumnHeaderCell>((Func<DataGridViewColumn, ColumnHeaderCell>)(c => c.HeaderCell as ColumnHeaderCell));
            }
        }

        protected override void OnColumnAdded(DataGridViewColumnEventArgs e)
        {
            
            e.Column.SortMode = DataGridViewColumnSortMode.Programmatic;
            ColumnHeaderCell columnHeaderCell = new ColumnHeaderCell(e.Column.HeaderCell, this.FilterAndSortEnabled);
            columnHeaderCell.SortChanged += new ColumnHeaderCellEventHandler(this.Cell_SortChanged);
            columnHeaderCell.FilterChanged += new ColumnHeaderCellEventHandler(this.Cell_FilterChanged);
            columnHeaderCell.FilterPopup += new ColumnHeaderCellEventHandler(this.Cell_FilterPopup);
            e.Column.MinimumWidth = columnHeaderCell.MinimumSize.Width;
            if (this.ColumnHeadersHeight < columnHeaderCell.MinimumSize.Height)
                this.ColumnHeadersHeight = columnHeaderCell.MinimumSize.Height;
            e.Column.HeaderCell = (DataGridViewColumnHeaderCell)columnHeaderCell;
            //if (e.Column.DataGridView == null) return;
            base.OnColumnAdded(e);
        }

        protected override void OnColumnRemoved(DataGridViewColumnEventArgs e)
        {
            this._filteredColumns.Remove(e.Column.Name);
            this._filterOrderList.Remove(e.Column.Name);
            this._sortOrderList.Remove(e.Column.Name);
            ColumnHeaderCell headerCell = e.Column.HeaderCell as ColumnHeaderCell;
            if (headerCell != null)
            {
                headerCell.SortChanged -= new ColumnHeaderCellEventHandler(this.Cell_SortChanged);
                headerCell.FilterChanged -= new ColumnHeaderCellEventHandler(this.Cell_FilterChanged);
                headerCell.FilterPopup -= new ColumnHeaderCellEventHandler(this.Cell_FilterPopup);
            }
            base.OnColumnRemoved(e);
        }

        protected override void OnRowsAdded(DataGridViewRowsAddedEventArgs e)
        {
            this._filteredColumns.Clear();
            base.OnRowsAdded(e);
        }

        protected override void OnRowsRemoved(DataGridViewRowsRemovedEventArgs e)
        {
            this._filteredColumns.Clear();
            base.OnRowsRemoved(e);
        }

        protected override void OnCellValueChanged(DataGridViewCellEventArgs e)
        {
            this._filteredColumns.Remove(this.Columns[e.ColumnIndex].Name);
            base.OnCellValueChanged(e);
        }

        private string BuildFilterString()
        {
            StringBuilder stringBuilder = new StringBuilder("");
            string str = "";
            foreach (string filterOrder in this._filterOrderList)
            {
                DataGridViewColumn column = this.Columns[filterOrder];
                if (column != null)
                {
                    ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
                    if (headerCell != null && headerCell.FilterAndSortEnabled && headerCell.ActiveFilterType != MenuStrip.FilterType.None)
                    {
                        stringBuilder.AppendFormat(str + "(" + headerCell.FilterString + ")", (object)column.DataPropertyName);
                        str = " AND ";
                    }
                }
            }
            return stringBuilder.ToString();
        }

        private void Cell_FilterPopup(object sender, ColumnHeaderCellEventArgs e)
        {
            if (!this.Columns.Contains(e.Column))
                return;
            MenuStrip filterMenu = e.FilterMenu;
            DataGridViewColumn column = e.Column;
            Rectangle displayRectangle = this.GetCellDisplayRectangle(column.Index, -1, true);
            if (this._filteredColumns.Contains(column.Name))
            {
                filterMenu.Show((Control)this, displayRectangle.Left, displayRectangle.Bottom, false);
            }
            else
            {
                this._filteredColumns.Add(column.Name);
                if (this._filterOrderList.Count<string>() > 0 && this._filterOrderList.Last<string>() == column.Name)
                    filterMenu.Show((Control)this, displayRectangle.Left, displayRectangle.Bottom, true);
                else
                    filterMenu.Show((Control)this, displayRectangle.Left, displayRectangle.Bottom, MenuStrip.GetValuesForFilter((DataGridView)this, column.Name));
            }
        }

        private void Cell_FilterChanged(object sender, ColumnHeaderCellEventArgs e)
        {
            if (!this.Columns.Contains(e.Column))
                return;
            MenuStrip filterMenu = e.FilterMenu;
            DataGridViewColumn column = e.Column;
            this._filterOrderList.Remove(column.Name);
            if (filterMenu.ActiveFilterType != MenuStrip.FilterType.None)
                this._filterOrderList.Add(column.Name);
            this.FilterString = this.BuildFilterString();
            _loadedFilter = true;//ToDO
            if (!this._loadedFilter)
                return;
            this._loadedFilter = false;
            foreach (ColumnHeaderCell columnHeaderCell in this.FilterableCells.Where<ColumnHeaderCell>((Func<ColumnHeaderCell, bool>)(f => f.MenuStrip != filterMenu)))
                columnHeaderCell.SetLoadedMode(false);
        }

        private string BuildSortString()
        {
            StringBuilder stringBuilder = new StringBuilder("");
            string str = "";
            foreach (string sortOrder in this._sortOrderList)
            {
                DataGridViewColumn column = this.Columns[sortOrder];
                if (column != null)
                {
                    ColumnHeaderCell headerCell = column.HeaderCell as ColumnHeaderCell;
                    if (headerCell != null && headerCell.FilterAndSortEnabled && headerCell.ActiveSortType != MenuStrip.SortType.None)
                    {
                        stringBuilder.AppendFormat(str + headerCell.SortString, (object)column.DataPropertyName);
                        str = ", ";
                    }
                }
            }
            return stringBuilder.ToString();
        }

        private void Cell_SortChanged(object sender, ColumnHeaderCellEventArgs e)
        {
            if (!this.Columns.Contains(e.Column))
                return;
            MenuStrip filterMenu = e.FilterMenu;
            DataGridViewColumn column = e.Column;
            this._sortOrderList.Remove(column.Name);
            if (filterMenu.ActiveSortType != MenuStrip.SortType.None)
                this._sortOrderList.Add(column.Name);
            this.SortString = this.BuildSortString();
        }

        public class SortEventArgs : EventArgs
        {
            public string SortString { get; set; }

            public bool Cancel { get; set; }

            public SortEventArgs()
            {
                this.SortString = (string)null;
                this.Cancel = false;
            }
        }

        public class FilterEventArgs : EventArgs
        {
            public string FilterString { get; set; }

            public bool Cancel { get; set; }

            public FilterEventArgs()
            {
                this.FilterString = (string)null;
                this.Cancel = false;
            }
        }
    }
}