﻿namespace Common
{
    public abstract class Logger
    {
        public Logger()
        {
            Log.OnLogHandler += new Log.LogEventHandler(LogMessage);
        }

        public abstract void LogMessage(string Message);
    }
}