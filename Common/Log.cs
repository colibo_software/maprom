﻿namespace Common
{
    public class Log
    {
        public delegate void LogEventHandler(string Message);
        public static event LogEventHandler OnLogHandler;
        private static object sync = new object();
        public static void WriteLine(string Message)
        {
            lock (sync)
            {
                if (OnLogHandler != null)
                {
                    OnLogHandler(Message);
                }
            }
        }
    }
}