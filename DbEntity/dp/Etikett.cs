﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Linq;
using System.Xml.Serialization;
using DbEntity;
using DbEntity.db;
using DbEntity.dp;
using DbEntity.json;

namespace DeutschePost
{
    public class Etikett : Entity
    {
        public string user { get; set; }
        public string delivery_name { get; set; }
        
        //public int delivery_id { get; set; }
        public string order_nr { get; set; }
        public int order_id { get; set; }
        public string tracking_id { get; set; }
        public decimal weight { get; set; }


        [Description("MyClass")]
        public Delivery delivery { get; set; }
        [Description("MyClass")]
        public Invoice invoice { get; set; }
        [Description("MyClass")]
        public Sender sender { get; set; }

        public State State { get; set; }

        public Analyzer Analyzer { get; set; }


        public DateTime Date { get; set; }

        public BankData BankData { get; set; }


        public Api api { get; set; }

        public SmtpServer smtp { get; set; }
        [NotMapped]
        public List<Product> products { get; set; }

        public Etikett()
        {
            user = String.Empty;

            tracking_id = String.Empty;

            State = State.unprocessed;
            Date = DateTime.Now;
            Analyzer = new Analyzer();
            products = new List<Product>();
            smtp = new SmtpServer();
            api = new ApiDP();
            BankData = new BankData();
        }

    }
}