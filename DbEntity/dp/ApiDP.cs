﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbEntity.dp;
using DeutschePost;

namespace DbEntity.json
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ApiDP :Api
    {
        public string PARTNER_ID { get; set; }
        public string SCHLUESSEL_DPWN_MARKTPLATZ { get; set; }
        public string KEY_PHASE { get; set; }
        public string Mail { get; set; }
        public string Password { get; set; }

        public ApiDP()
        {
            PARTNER_ID = String.Empty;
            SCHLUESSEL_DPWN_MARKTPLATZ = String.Empty;
            KEY_PHASE = String.Empty;
            Mail = String.Empty;
            Password = String.Empty;
        }
        public bool IsValid()
        {
            return !string.IsNullOrWhiteSpace(PARTNER_ID) &&
                   !string.IsNullOrWhiteSpace(SCHLUESSEL_DPWN_MARKTPLATZ) &&
                   !string.IsNullOrWhiteSpace(KEY_PHASE) &&
                   !string.IsNullOrWhiteSpace(Mail) &&
                   !string.IsNullOrWhiteSpace(Password);
        }
    }
}
