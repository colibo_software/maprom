﻿using System;
using System.ComponentModel;
using System.Text;
using DbEntity;

namespace DeutschePost
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Delivery : Entity
    {
        public string company { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string name { get; set; }
        public string country { get; set; }
        public string postal_code { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string street_nr { get; set; }
        public string additional { get; set; }

        public string email { get; set; }
        public string phone { get; set; }
        //public string customer_nr { get; set; }

        public Delivery()
        {
            company = String.Empty;
            firstname = String.Empty;
            lastname = String.Empty;
            name = String.Empty;
            country = String.Empty;
            postal_code = String.Empty;
            city = String.Empty;
            city = String.Empty;
            street = String.Empty;
            street_nr = String.Empty;
            additional = String.Empty;

            email = String.Empty;
            phone = String.Empty;
        }


        public string FormatString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"Company: {company}");
            stringBuilder.AppendLine($"Firstname: {firstname}");
            stringBuilder.AppendLine($"Lastname: {lastname}");
            stringBuilder.AppendLine($"Name: {name}");
            stringBuilder.AppendLine($"Country: {country}");
            stringBuilder.AppendLine($"Postal code: {postal_code}");
            stringBuilder.AppendLine($"City: {city}");
            stringBuilder.AppendLine($"Street: {street}");
            stringBuilder.AppendLine($"Street nr: {street_nr}");
            stringBuilder.AppendLine($"Additional: {additional}");

            return stringBuilder.ToString();
        }
    }
}