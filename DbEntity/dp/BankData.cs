﻿using System;

namespace DbEntity.dp
{
    public class BankData : Entity
    {
        public string accountReference { get; set; }
        public string cashOnDeliveryPrice { get; set; }
        public string iban { get; set; }
        public string bic { get; set; }
        public string accountOwner { get; set; }
        public string bankName { get; set; }

        public BankData()
        {
            accountOwner = string.Empty;
            accountReference = string.Empty;
            cashOnDeliveryPrice = String.Empty;
            iban = String.Empty;
            bic = String.Empty;
            bankName = String.Empty;
        }

        public bool isEmpty()
        {
            return true;
        }
    }
}