﻿using System;
using System.Xml.Serialization;

namespace DbEntity.db
{
    public class SmtpServer : Entity
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string From { get; set; }
        public string FromMail { get; set; }
        public SmtpServer()
        {
            Host = String.Empty;
            Port = -1;
            UserName = String.Empty;
            Password = String.Empty;
            From = String.Empty;
            FromMail = string.Empty;
        }

        public bool IsValid()
        {
            return !string.IsNullOrWhiteSpace(Host) &&
                   !string.IsNullOrWhiteSpace(UserName) &&
                   !string.IsNullOrWhiteSpace(Password) &&
                   !string.IsNullOrWhiteSpace(From) &&
                   !string.IsNullOrWhiteSpace(FromMail) &&
                   Port != -1;
        }
    }
}