﻿using System.ComponentModel;

namespace DbEntity.dp
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ApiDHL : Api
    {
        public string AppId { get; set; }
        public string Token { get; set; }
        public string User { get; set; }
        public string Signature { get; set; }
        public string AccNumber { get; set; }
        public string Code { get; set; }
        public string Product { get; set; }


        public ApiDHL()
        {
            
        }

        public bool IsValid()
        {
            return !string.IsNullOrWhiteSpace(AppId) &&
                   !string.IsNullOrWhiteSpace(Token) &&
                   !string.IsNullOrWhiteSpace(User) &&
                   !string.IsNullOrWhiteSpace(Signature) &&
                   !string.IsNullOrWhiteSpace(AccNumber) &&
                   !string.IsNullOrWhiteSpace(Product) &&
                   !string.IsNullOrWhiteSpace(Code);
        }
    }
}