﻿using System.Collections.Generic;

using DbEntity;

namespace DeutschePost
{
    public class Response : Entity
    {

        public bool status { get; set; }
        public List<Etikett> data { get; set; }

        public Response()
        {
            data = new List<Etikett>();
        }
    }
}