﻿using System.ComponentModel;
using DeutschePost;

namespace DbEntity.json
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Product:Entity
    {

        public string sku { get; set; }
        public string name { get; set; }
        public string tax { get; set; }
        public int price { get; set; }
        public int quantity { get; set; }
    }
}