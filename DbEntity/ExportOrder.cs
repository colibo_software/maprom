﻿using System;

namespace LabelPrint
{
    public class ExportOrder
    {
        public string tracking_id { get; set; }
        public DateTime date { get; set; }
        public int order_id { get; set; }


        public ExportOrder(string trackingId, DateTime date, int orderId)
        {
            tracking_id = trackingId;
            this.date = date;
            order_id = orderId;
        }

        public ExportOrder()
        {
            
        }
    }
}