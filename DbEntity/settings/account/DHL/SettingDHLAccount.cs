﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DbEntity
{
    public class SettingDHLAccount
    {
        public string ApplicationID { get; set; }
        public string Token { get; set; }
        public string User { get; set; }
        public string Signature { get; set; }
        public string AccountNumber { get; set; }

        public string Code { get; set; }
        public string Product { get; set; }

        public OutputSettings OutputSettings { get; set; }

        public SettingDHLAccount()
        {
#if false
            procuct = new List<KeyValuePair<string, string>>()
            {


                new KeyValuePair<string, string>("0101","V01PAK" ),
                new KeyValuePair<string, string>("0102","V01PAK" ),
                new KeyValuePair<string, string>("0103","V01PAK" ),
                new KeyValuePair<string, string>("0104","V01PAK" ),
                new KeyValuePair<string, string>("0105","V01PAK" ),
                new KeyValuePair<string, string>("0106","V01PRIO" ),
                new KeyValuePair<string, string>("0107","V01PRIO" ),
                new KeyValuePair<string, string>("0109","V01PRIO" ),
                new KeyValuePair<string, string>("0110","V01PRIO" ),
                new KeyValuePair<string, string>("0601","V06PAK" ),
                new KeyValuePair<string, string>("0602","V06PAK" ),
                new KeyValuePair<string, string>("0603","V06PAK" ),
                new KeyValuePair<string, string>("5301","V53WPAK" ),
                new KeyValuePair<string, string>("5302","V53WPAK" ),
                new KeyValuePair<string, string>("5401","V54EPAK" ),
                new KeyValuePair<string, string>("5402","V54EPAK" ),
                new KeyValuePair<string, string>("5403","V54EPAK" ),
                new KeyValuePair<string, string>("5501","V55PAK" ),
                new KeyValuePair<string, string>("5502","V55PAK" ),
                new KeyValuePair<string, string>("5503","V55PAK" ),
                new KeyValuePair<string, string>("0112","V06TG" ),
                new KeyValuePair<string, string>("0111","V06WZ" ),
                new KeyValuePair<string, string>("8602","V86PARCEL" ),
                new KeyValuePair<string, string>("8702","V87PARCEL" ),
                new KeyValuePair<string, string>("8202","V82PARCEL" ),
                new KeyValuePair<string, string>("0701","V01PAK" ),
                new KeyValuePair<string, string>("0701","V06PAK" ),
                new KeyValuePair<string, string>("8302","V86PARCEL" ),
                new KeyValuePair<string, string>("8502","V87PARCEL" ),

            };
#endif
            
            ApplicationID = String.Empty;
            Token = String.Empty;
            User = String.Empty;
            Signature = String.Empty;
            AccountNumber = string.Empty;
            Code = String.Empty;
            Product = String.Empty;

            OutputSettings = new OutputSettings();
        }
    }   
}