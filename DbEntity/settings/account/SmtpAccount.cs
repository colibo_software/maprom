﻿using System;

namespace DbEntity
{
    public class SmtpAccount
    {
        public string Host{ get; set; }
        public int Port{ get; set; }
        public string UserName{ get; set; }
        public string Password{ get; set; }
        public string From{ get; set; }
        public string FromMail{ get; set; }

        public SmtpAccount()
        {
            Host = String.Empty;
            Port = -1;
            UserName = String.Empty;
            Password = String.Empty;
            From = String.Empty;
            FromMail = String.Empty;
        }
    }
}