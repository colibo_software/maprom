﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace DbEntity
{
    public class SettingsAccount : ICloneable
    {
        public SettingDeutschePostAccount DeutschePostAccount { get; set; }
        public SettingDHLAccount DhlAccount { get; set; }
        public SmtpAccount SmtpAccount { get; set; }

        public SettingsAccount()
        {
            DeutschePostAccount = new SettingDeutschePostAccount();
            SmtpAccount = new SmtpAccount();

            DhlAccount = new SettingDHLAccount();

        }


        public object Clone()
        {
            return new SettingsAccount()
            {
                DeutschePostAccount = new SettingDeutschePostAccount()
                {
                    EMail = this.DeutschePostAccount.EMail,
                    Password = this.DeutschePostAccount.Password,
                    PARTNER_ID = this.DeutschePostAccount.PARTNER_ID,
                    KEY_PHASE = this.DeutschePostAccount.KEY_PHASE,
                    SCHLUESSEL_DPWN_MARKTPLATZ = this.DeutschePostAccount.SCHLUESSEL_DPWN_MARKTPLATZ,
                    PriceForCode = DeutschePostAccount.PriceForCode,
                    ProductCode = DeutschePostAccount.ProductCode,

                    SandboxDeutschePostAccount = new SandboxDeutschePostAccount()
                    {
                        Password = DeutschePostAccount.SandboxDeutschePostAccount.Password,
                        EMail = DeutschePostAccount.SandboxDeutschePostAccount.EMail,
                        PriceForCode = DeutschePostAccount.SandboxDeutschePostAccount.PriceForCode,
                        ProductCode = DeutschePostAccount.SandboxDeutschePostAccount.ProductCode
                    },

                    OutputSettings = (OutputSettings)DeutschePostAccount.OutputSettings.Clone()
                },
                DhlAccount = new SettingDHLAccount()
                {
                    ApplicationID = DhlAccount.ApplicationID,
                    Token = DhlAccount.Token,
                    User = DhlAccount.User,
                    Signature = DhlAccount.Signature,
                    AccountNumber = DhlAccount.AccountNumber,
                    Code = DhlAccount.Code,
                    Product = DhlAccount.Product,

                    OutputSettings = (OutputSettings)DhlAccount.OutputSettings.Clone()
                },
                SmtpAccount = new SmtpAccount()
                {
                    Host = this.SmtpAccount.Host,
                    Port = this.SmtpAccount.Port,
                    UserName = this.SmtpAccount.UserName,
                    Password = this.SmtpAccount.Password,
                    From = this.SmtpAccount.From,
                    FromMail = this.SmtpAccount.FromMail
                },

               
            };
        }
    }
}