﻿using System;
using System.ComponentModel;

namespace DbEntity
{
    public class WebHook : ICloneable
    {
        public string Url { get; set; }

        public BindingList<Dictionary> PairsGET { get; set; }
        public BindingList<Dictionary> PairsPOST { get; set; }

        public WebHook()
        {
            Url = string.Empty;

            PairsGET = new BindingList<Dictionary>();
            PairsPOST = new BindingList<Dictionary>();
        }

        public object Clone()
        {
            return new WebHook()
            {
                Url = this.Url,
                PairsGET = this.PairsGET,
                PairsPOST = this.PairsPOST
            };

        }
    }
}