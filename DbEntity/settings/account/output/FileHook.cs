﻿using System;

namespace DbEntity
{
    public class FileHook : ICloneable
    {
        public string ExportFolderDP { get; set; }
        public string ExportFolderDHL { get; set; }

        public FileHook()
        {
            ExportFolderDHL = String.Empty;
            ExportFolderDP = string.Empty;
        }

        public object Clone()
        {
            return new FileHook()
            {
                ExportFolderDHL = this.ExportFolderDHL,
                ExportFolderDP = this.ExportFolderDP
            };
        }
    }
}