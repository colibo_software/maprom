﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DbEntity
{
    public class OutputSettings : ICloneable
    {
        public bool IsWebHook { get; set; }
        public bool IsFile { get; set; }

        public WebHook WebHook { get; set; }
        public FileHook FileHook { get; set; }
        

        public OutputSettings()
        {
            WebHook = new WebHook();
            FileHook = new FileHook();
        }

        public object Clone()
        {
            return new OutputSettings()
            {
                IsWebHook = this.IsWebHook,
                IsFile = this.IsFile,
                WebHook = (DbEntity.WebHook)WebHook.Clone(),
                FileHook = (DbEntity.FileHook)FileHook.Clone()
            };

        }
    }
}