﻿namespace DbEntity
{
    public class SandboxDeutschePostAccount
    {
        public string EMail { get; set; }
        public string Password { get; set; }

        public int ProductCode { get; set; }
        public int PriceForCode { get; set; }

        public SandboxDeutschePostAccount()
        {
            EMail = string.Empty;
            Password = string.Empty;

            ProductCode = -1;
            PriceForCode = -1;
        }
    }
}