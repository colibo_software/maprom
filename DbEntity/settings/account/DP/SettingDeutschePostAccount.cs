﻿using System;

namespace DbEntity
{
    public class SettingDeutschePostAccount
    {
        public string PARTNER_ID { get; set; }
        public string SCHLUESSEL_DPWN_MARKTPLATZ { get; set; }
        public string KEY_PHASE { get; set; }
        public string EMail { get; set; }
        public string Password { get; set; }

        public int ProductCode { get; set; }
        public int PriceForCode { get; set; }

        public SandboxDeutschePostAccount SandboxDeutschePostAccount { get; set; }

        public OutputSettings OutputSettings { get; set; }

        public SettingDeutschePostAccount()
        {
            PARTNER_ID = string.Empty;
            SCHLUESSEL_DPWN_MARKTPLATZ = String.Empty;
            KEY_PHASE = string.Empty;
            EMail = string.Empty;
            Password = string.Empty;

            ProductCode = -1;
            PriceForCode = -1;

            SandboxDeutschePostAccount = new SandboxDeutschePostAccount();

            OutputSettings = new OutputSettings();
        }
    }
}