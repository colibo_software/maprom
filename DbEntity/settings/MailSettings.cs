﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DbEntity.json;
using DeutschePost;
using Newtonsoft.Json;

namespace DbEntity
{
    public class MailSettings
    {
        [JsonIgnore]
        public Etikett etikett { get; }
        public List<string> Collection { get; set; }

        public string PathTemplateMail { get; set; }

        public MailSettings()
        {
            Collection = new List<string>();

            etikett = new Etikett()
            {
                user = "K1",
                delivery_name = "deutsche_post_warensendung",
                order_id = 218210172,
                order_nr = "218210172",
                weight = 35,
                tracking_id = "none",
                delivery = new Delivery()
                {
                    company = "Kai Hake",
                    country = "DE",
                    postal_code = "37688",
                    city = "Beverungen",
                    street = "Sonnenbreite 3"
                },
                invoice = new Invoice(),
                sender = new Sender()
                {
                    company = "MAPROM GmbH",
                    name = "t.boeker@maprom.de",
                    country = "DE",
                    postal_code = "37671",
                    city = "Höxter",
                    street = "Rohrweg 33"
                },
                api = new ApiDP()
                {
                    PARTNER_ID = "AAAAA",
                    KEY_PHASE = "1",
                    SCHLUESSEL_DPWN_MARKTPLATZ = "F46FrqPamtjNuy1BOwp385i0FkY5FC6o",
                    Mail = "edv@maprom.de",
                    Password = "***********"
                }
            };

            PathTemplateMail = string.Empty;
        }
    }
}