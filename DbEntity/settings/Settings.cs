﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using DbEntity;
using Newtonsoft.Json;

namespace DbEntity
{
    public class Settings : Entity
    {
        [JsonIgnore]
        public string Json { get; set; }

        [NotMapped]
        public bool IsSandbox { get; set; }

        [NotMapped]
        public SettingsPrinter SettingsPrinter { get; set; }

        [NotMapped]
        public GeneralSettings GeneralSettings { get; set; }

        [NotMapped]
        public SettingsAccount SettingsAccount { get; set; }

        [NotMapped]
        public MailSettings MailSettings { get; set; }

        [NotMapped]
        [JsonIgnore]
        public MyContext _context { get; set; }


        public Settings()
        {
            this.SettingsPrinter = new SettingsPrinter();
            this.GeneralSettings = new GeneralSettings();
            this.SettingsAccount = new SettingsAccount();
            this.MailSettings = new MailSettings();

        }

        public Settings(MyContext context)
        {
            _context = context;

            this.SettingsPrinter = new SettingsPrinter();
            this.GeneralSettings = new GeneralSettings();
            this.SettingsAccount = new SettingsAccount();
            this.MailSettings = new MailSettings();

            Json = JsonConvert.SerializeObject(this);

            _context.Settingses.Add(this); //ToDo ? set
            _context.SaveChanges();
        }


public void Serialize()
        {
            //using (StreamWriter sw = new StreamWriter("settings.json", false, System.Text.Encoding.UTF8))
            //{
            //    sw.WriteLine(JsonConvert.SerializeObject(this));
            //}
            Json = JsonConvert.SerializeObject(this);

            _context.Set<Settings>().AddOrUpdate(); //ToDo ? set
            _context.SaveChanges();
        }

        public void Deserialize()
        {
            //string tmp = String.Empty;
            //try
            //{
            //    if (!File.Exists("settings.json"))
            //    {
            //        return null;
            //    }
            //    using (StreamReader sr = new StreamReader("settings.json", System.Text.Encoding.UTF8))//todo кодировки прочитать
            //    {
            //        tmp = sr.ReadToEnd();
            //    }
            //}
            //catch (Exception e)
            //{
            //    throw;
            //}
            var tmp = JsonConvert.DeserializeObject<Settings>(Json);

            IsSandbox = tmp.IsSandbox;
            this.SettingsPrinter = tmp.SettingsPrinter;
            this.GeneralSettings = tmp.GeneralSettings;
            this.SettingsAccount = (SettingsAccount)tmp.SettingsAccount.Clone();
            this.MailSettings = tmp.MailSettings;
        }
    }
}