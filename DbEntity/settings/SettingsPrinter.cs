﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace DbEntity
{
    public class SettingsPrinter
    {
        public string PrinterNameDP { get; set; }
        public string PrinterNameDHL { get; set; }

        public string PathTemplateLabelDeutschePost { get; set; }
        // Todo относитльные пути
        public string PathTemplateLabelDHL { get; set; }
        // Todo относитльные пути

        

        public SettingsPrinter()
        {
            PrinterNameDP = String.Empty;
            PrinterNameDHL = String.Empty;
            PathTemplateLabelDeutschePost = String.Empty;
            PathTemplateLabelDHL = String.Empty;
        }

    }

}