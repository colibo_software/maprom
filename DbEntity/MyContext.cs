﻿using System.Data.Entity;
using DbEntity.db;
using DbEntity.dp;
using DeutschePost;

namespace DbEntity
{
    public class MyContext : DbContext
    {
        public MyContext()
            : base("DBConnection")
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<MyContext, Migrations.Configuration>());
        }

        public DbSet<Delivery> Deliveries { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Sender> Senders { get; set; }
        public DbSet<Etikett> Etiketten { get; set; }
        public DbSet<Analyzer> Analyzers { get; set; }

        public DbSet<Log> Logs { get; set; }
        public DbSet<SmtpServer> SmtpServers { get; set; }
        public DbSet<BankData> BankDatas { get; set; }
        public DbSet<Api> Apis { get; set; }

        public DbSet<Settings> Settingses { get; set; }


    }
}