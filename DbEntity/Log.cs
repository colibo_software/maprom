﻿using System;

namespace DbEntity
{
    public class Log
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string Level { get; set; }
        public string StackTrace { get; set; }
        public string Callsite { get; set; }
        public DateTime Date { get; set; } 

        public Log()
        {

        }
    }
}