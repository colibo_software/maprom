namespace DbEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        Level = c.String(),
                        Callsite = c.String(),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Analyzers", "SetTrackingNr", c => c.Int(nullable: false));
            AddColumn("dbo.Etiketten", "tracking_id", c => c.Int(nullable: false));
            AddColumn("dbo.Etiketten", "weight", c => c.Double(nullable: false));
            AddColumn("dbo.Etiketten", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Etiketten", "Date");
            DropColumn("dbo.Etiketten", "weight");
            DropColumn("dbo.Etiketten", "tracking_id");
            DropColumn("dbo.Analyzers", "SetTrackingNr");
            DropTable("dbo.Logs");
        }
    }
}
