namespace DbEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addShipmentNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Analyzers", "ShipmentNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Analyzers", "ShipmentNumber");
        }
    }
}
