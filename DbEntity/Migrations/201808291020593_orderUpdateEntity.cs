namespace DbEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderUpdateEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Analyzers", "PathFile", c => c.String());
            AddColumn("dbo.Analyzers", "StateFile", c => c.Int(nullable: false));
            AlterColumn("dbo.Etiketten", "weight", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Analyzers", "PathArchive");
            DropColumn("dbo.Analyzers", "StateArchive");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Analyzers", "StateArchive", c => c.Int(nullable: false));
            AddColumn("dbo.Analyzers", "PathArchive", c => c.String());
            AlterColumn("dbo.Etiketten", "weight", c => c.Double(nullable: false));
            DropColumn("dbo.Analyzers", "StateFile");
            DropColumn("dbo.Analyzers", "PathFile");
        }
    }
}
