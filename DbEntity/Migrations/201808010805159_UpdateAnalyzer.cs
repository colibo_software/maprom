namespace DbEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAnalyzer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Analyzers", "StampLink", c => c.String());
            DropColumn("dbo.Analyzers", "PathArchqqive");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Analyzers", "PathArchqqive", c => c.String());
            DropColumn("dbo.Analyzers", "StampLink");
        }
    }
}
