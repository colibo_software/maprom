namespace DbEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Analyzers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PathArchive = c.String(),
                        PathArchqqive = c.String(),
                        StateArchive = c.Int(nullable: false),
                        StateDocument = c.Int(nullable: false),
                        StateStamp = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Deliveries",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        company = c.String(),
                        firstname = c.String(),
                        lastname = c.String(),
                        name = c.String(),
                        country = c.String(),
                        postal_code = c.String(),
                        city = c.String(),
                        street = c.String(),
                        street_nr = c.String(),
                        additional = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        company = c.String(),
                        firstname = c.String(),
                        lastname = c.String(),
                        name = c.String(),
                        country = c.String(),
                        postal_code = c.String(),
                        city = c.String(),
                        street = c.String(),
                        street_nr = c.String(),
                        additional = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Etiketten",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        user = c.String(),
                        delivery_name = c.String(),
                        delivery_identifier = c.Int(nullable: false),
                        voucher_nr = c.String(),
                        voucher_identifier = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                        Analyzer_Id = c.Guid(),
                        Delivery_Id = c.Guid(),
                        Invoice_Id = c.Guid(),
                        Sender_Id = c.Guid(),
                        Response_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Analyzers", t => t.Analyzer_Id)
                .ForeignKey("dbo.Deliveries", t => t.Delivery_Id)
                .ForeignKey("dbo.Invoices", t => t.Invoice_Id)
                .ForeignKey("dbo.Senders", t => t.Sender_Id)
                .ForeignKey("dbo.Responses", t => t.Response_Id)
                .Index(t => t.Analyzer_Id)
                .Index(t => t.Delivery_Id)
                .Index(t => t.Invoice_Id)
                .Index(t => t.Sender_Id)
                .Index(t => t.Response_Id);
            
            CreateTable(
                "dbo.Senders",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        company = c.String(),
                        firstname = c.String(),
                        lastname = c.String(),
                        name = c.String(),
                        country = c.String(),
                        postal_code = c.String(),
                        city = c.String(),
                        street = c.String(),
                        street_nr = c.String(),
                        additional = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Responses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Etiketten", "Response_Id", "dbo.Responses");
            DropForeignKey("dbo.Etiketten", "Sender_Id", "dbo.Senders");
            DropForeignKey("dbo.Etiketten", "Invoice_Id", "dbo.Invoices");
            DropForeignKey("dbo.Etiketten", "Delivery_Id", "dbo.Deliveries");
            DropForeignKey("dbo.Etiketten", "Analyzer_Id", "dbo.Analyzers");
            DropIndex("dbo.Etiketten", new[] { "Response_Id" });
            DropIndex("dbo.Etiketten", new[] { "Sender_Id" });
            DropIndex("dbo.Etiketten", new[] { "Invoice_Id" });
            DropIndex("dbo.Etiketten", new[] { "Delivery_Id" });
            DropIndex("dbo.Etiketten", new[] { "Analyzer_Id" });
            DropTable("dbo.Responses");
            DropTable("dbo.Senders");
            DropTable("dbo.Etiketten");
            DropTable("dbo.Invoices");
            DropTable("dbo.Deliveries");
            DropTable("dbo.Analyzers");
        }
    }
}
