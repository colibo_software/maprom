﻿using System;
using DeutschePost;

namespace DbEntity
{
    public class Analyzer : Entity
    {
        public string PathFile { get; set; }
        public string StampLink { get; set; }
        public string ShipmentNumber { get; set; }
        public State StateFile { get; set; }
        public State StateDocument { get; set; }
        public State StateStamp { get; set; }
        public State SetTrackingNr { get; set; }

        public State SendMail { get; set; }

        public Analyzer()
        {
            StampLink = String.Empty;
            PathFile = string.Empty;
            StateFile = State.unprocessed;
            StateDocument = State.unprocessed;
            StateStamp = State.unprocessed;
            SetTrackingNr = State.unprocessed;
            SendMail = State.unprocessed;
        }
    }
}