﻿using System;

namespace LabelPrint
{
    public class SmtpData
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string From { get; set; }

        public SmtpData()
        {
            Host = String.Empty;
            Port = String.Empty;
            UserName = String.Empty;
            Password = String.Empty;
            From = String.Empty;
        }
    }
}