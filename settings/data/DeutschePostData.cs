﻿using System;

namespace LabelPrint
{
    public class DeutschePostData
    {
        public int ProductCode { get; set; }
        public int PriceForCode { get; set; }
        public string TrakingNrUrl { get; set; }

        public DeutschePostData()
        {
            TrakingNrUrl = String.Empty;
            ProductCode = -1;
            PriceForCode = -1;
        }
    }
}