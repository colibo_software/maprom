﻿using System;
using System.Windows.Forms;


namespace LabelPrint
{
    public class SettingsData : ICloneable
    {
        public DbEntity.DeutschePostData DeutschePostData { get; set; }
        public SmtpData SmtpData { get; set; }

        public SettingsData()
        {
            DeutschePostData = new DbEntity.DeutschePostData();
            SmtpData = new SmtpData();
        }

        public object Clone()
        {
            return new DbEntity.SettingsData()
            {
                DeutschePostData = new DbEntity.DeutschePostData()
                {
                    PriceForCode = this.DeutschePostData.PriceForCode,
                    ProductCode = this.DeutschePostData.ProductCode
                },
                SmtpData = new DbEntity.SmtpData()
                {
                    Host = this.SmtpData.Host,
                    Port = this.SmtpData.Port,
                    UserName = this.SmtpData.UserName,
                    Password = this.SmtpData.Password,
                    From = this.SmtpData.From,
                }
            };
        }
    }
}
