﻿using System;

namespace LabelPrint
{
    public class SettingDeutschePostAccount
    {
        public string PARTNER_ID { get; set; }
        public string SCHLUESSEL_DPWN_MARKTPLATZ { get; set; }
        public string KEY_PHASE { get; set; }
        public string EMail { get; set; }
        public string Password { get; set; }

        public SettingDeutschePostAccount()
        {
            PARTNER_ID = string.Empty;
            SCHLUESSEL_DPWN_MARKTPLATZ = String.Empty;
            KEY_PHASE = string.Empty;
            EMail = string.Empty;
            Password = string.Empty;
        }
    }
}