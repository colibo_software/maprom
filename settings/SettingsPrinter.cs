﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using LabelPrint.Annotations;
using Newtonsoft.Json;

namespace LabelPrint
{
    public class SettingsPrinter
    {
        public string PrinterName { get; set; }

        public string PathTemplateLabelDeutschePost { get; set; }
        // Todo относитльные пути
        


        public SettingsPrinter()
        {
            PrinterName = String.Empty;
            PathTemplateLabelDeutschePost = String.Empty;
        }

    }

}