﻿namespace LabelPrint
{
    public class SettingsAccount
    {
        public SettingDeutschePostAccount DeutschePostAccount { get; set; }

        public SettingsAccount()
        {
            DeutschePostAccount = new SettingDeutschePostAccount();
        }
    }
}