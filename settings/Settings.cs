﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace LabelPrint
{
    public class Settings
    {
        public bool IsSandbox { get; set; }
        public SettingsPrinter SettingsPrinter { get; set; }
        public GeneralSettings GeneralSettings { get; set; }
        public SettingsAccount SettingsAccount { get; set; }
        public SettingsData SettingsData { get; set; }

        public Settings()
        {
            SettingsPrinter = new SettingsPrinter();
            GeneralSettings = new GeneralSettings();
            SettingsAccount =new SettingsAccount();
            SettingsData = new SettingsData();
        }

        public void Serialize()
        {
            using (StreamWriter sw = new StreamWriter("settings.json", false, System.Text.Encoding.UTF8))
            {
                sw.WriteLine(JsonConvert.SerializeObject(this));
            }
        }

        public static Settings Deserialize()
        {
            string tmp = String.Empty;
            try
            {
                if (!File.Exists("settings.json"))
                {
                    return null;
                }
                using (StreamReader sr = new StreamReader("settings.json", System.Text.Encoding.UTF8))//todo кодировки прочитать
                {
                    tmp = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return JsonConvert.DeserializeObject<Settings>(tmp);
        }
    }
}