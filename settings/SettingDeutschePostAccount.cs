﻿namespace LabelPrint
{
    public class SettingDeutschePostAccount
    {
        public string PARTNER_ID { get; set; }
        public string SCHLUESSEL_DPWN_MARKTPLATZ { get; set; }
        public string KEY_PHASE { get; set; }
        public string EMail { get; set; }
        public string Password { get; set; }

        public SettingDeutschePostAccount()
        {
            
        }
    }
}